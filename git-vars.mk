ifeq (${CI}, true)
  GIT_COMMIT = ${CI_COMMIT_SHA}
  GIT_CLEAN = 1
  BUILD_DATE = $(shell date --date='${CI_COMMIT_TIMESTAMP}' +%s)
else
  GIT_COMMIT = $(shell git rev-parse HEAD)
  GIT_CLEAN = $(shell [[ -z `git status -s` ]] && echo 1 || echo 0)
  BUILD_DATE = $(shell git log -1 --pretty=%ct)
endif
GIT_COMMIT_ARR = $(shell python -c "print(r'{' + ', '.join('0x' + '${GIT_COMMIT}'[i:i+2] for i in range(0, 40, 2)) + r'}', end='')")
export SOURCE_DATE_EPOCH = $(BUILD_DATE)

GIT_DESCRIBE    = $(shell git describe --tags)
VERSION_MAJOR   = $(shell python -c "import re; m = re.match(r'v?(?P<ma>\d+)\.(?P<mi>\d+).(?P<pa>\d+)(-(?P<co>\d+))?', '${GIT_DESCRIBE}', re.I); print(m.group('ma') or 0, end='')")
VERSION_MINOR   = $(shell python -c "import re; m = re.match(r'v?(?P<ma>\d+)\.(?P<mi>\d+).(?P<pa>\d+)(-(?P<co>\d+))?', '${GIT_DESCRIBE}', re.I); print(m.group('mi') or 0, end='')")
VERSION_PATCH   = $(shell python -c "import re; m = re.match(r'v?(?P<ma>\d+)\.(?P<mi>\d+).(?P<pa>\d+)(-(?P<co>\d+))?', '${GIT_DESCRIBE}', re.I); print(m.group('pa') or 0, end='')")
VERSION_COMMITS = $(shell python -c "import re; m = re.match(r'v?(?P<ma>\d+)\.(?P<mi>\d+).(?P<pa>\d+)(-(?P<co>\d+))?', '${GIT_DESCRIBE}', re.I); print(m.group('co') or 0, end='')")
