# Shared Utilities

This repository holds utilities shared by multiple projects.
Currently this only includes C library functions, though.

See the [starcopter shared library](sclib/) documentation for details.
