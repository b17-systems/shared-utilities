/**
 * @file fdcan.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Libcanard Media IO Driver for STM32 FDCAN Peripheral
 * @version 0.2
 *
 * @copyright Copyright (c) 2022 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 * TODO: add module-level integration and usage documentation
 *
 */

#include "fdcan.h"

#include <math.h>
#include <sclib/assert.h>
#include <sclib/timekeeper.h>

// don't log to UAVCAN from inside this module
#define configLOG_MASK_UAVCAN
#define LOG_LEVEL configFDCAN_LOG_LEVEL
#include <sclib/log.h>

// ============================================ TYPES AND INTERNAL STATE ===============================================

// Task handle and redundant transport index for each available FDCAN interface, required to notify the transport task
// of interface/bus events. See transport.h and FDCANx_IT1_IRQHandler() for details.
static struct InterfaceConfiguration {
    TaskHandle_t notification_target;
    uint_fast8_t interface_index;
} interface_configuration[FDCAN_INTERFACE_COUNT];

// Result of a bit timings calculation to reach a certain bitrate.
typedef struct BitTimings {
    uint_fast16_t bit_rate_prescaler;
    uint_fast16_t max_resync_jump_width;
    uint_fast16_t bit_segment_1;
    uint_fast16_t bit_segment_2;
} BitTimings;

uint32_t fdcan_clk;

// =============================================== INTERNAL FUNCTIONS ==================================================

static inline FDCAN_GlobalTypeDef* get_iface(const uint_fast8_t hw_index) __attribute__((const));
static inline FDCAN_GlobalTypeDef* get_iface(const uint_fast8_t hw_index) {
    switch (hw_index) {
        case 0:
            return FDCAN1;
#ifdef FDCAN2
        case 1:
            return FDCAN2;
#endif
#ifdef FDCAN3
        case 2:
            return FDCAN3;
#endif
        default:
            WTF();
    }
}

static inline FDCANMessageRAM* get_message_ram(const uint_fast8_t hw_index) __attribute__((const));
static inline FDCANMessageRAM* get_message_ram(const uint_fast8_t hw_index) {
    ASSERT(hw_index < FDCAN_INTERFACE_COUNT);
    return (FDCANMessageRAM*) (SRAMCAN_BASE + hw_index * SRAMCAN_SIZE);
}

/**
 * @brief Get CAN Identifier of Highest Priority Frame Queued in an Interface's TX Queue
 *
 * @param hw_index zero-based hardware interface index, e.g. 0 for FDCAN1
 * @return uint32_t priority (lower value means higher priority), defaults to UINT32_MAX for empty queue
 */
static uint32_t get_highest_priority_queued(const uint_fast8_t hw_index) {
    FDCAN_GlobalTypeDef* const           iface                   = get_iface(hw_index);
    volatile FDCANTxBufferElement* const tfq                     = get_message_ram(hw_index)->TFQ;
    uint32_t                             highest_priority_queued = UINT32_MAX;

    for (uint_fast8_t index = 0u; index < SRAMCAN_TFQ_NBR; ++index) {
        // TXBRP (Tx buffer request pending register)
        //   TRP[2:0]:  Transmission request pending, one bit per TFQ slot
        if (!(iface->TXBRP & (1u << index))) {
            // no transmission request pending for current index
            continue;
        }
        if (tfq[index].identifier < highest_priority_queued) {
            highest_priority_queued = tfq[index].identifier;
        }
    }

    return highest_priority_queued;
}

/**
 * @brief Calculate Prescaler, Bit Time Segments and SJW to reach a Target Bitrate
 *
 * This function iterates over possible prescaler and time segment combinations to reach a solution for clock prescaler,
 * bit time segments 1 and 2 and synchronization jump width, respecting value bounds for both nominal (FDCAN_NBTP) and
 * data (FDCAN_DBTP) bit timing configuration values.
 *
 * This function samples the configured FDCAN subsystem kernel clock from the global fdcan_clk variable, and the common
 * clock divider configuration from the configFDCAN_CKDIV_PDIV_VALUE macro. When the fdcan_clk rate changes, all
 * previously calculated bit timing solutions would need to be recalculated, obviously.
 *
 * @param target_bitrate bitrate to (try to) match, given a globally configured fdcan_clk and PDIV value
 * @param is_nominal use value bounds for nominal timing (FDCAN_NBTP), not data timing (FDCAN_DBTP)
 * @return BitTimings best solution found, even if it should be crappy; watch log output for feedback
 */
static BitTimings calculate_bit_timings(const uint32_t target_bitrate, const bool is_nominal) {
    const uint_fast16_t MIN_TQ_PER_BIT = 4;
    // sample point location in parts-per-ten-thousand
    const uint_fast16_t SAMPLE_POINT_MIN    = 7500u;   // 75.0 %
    const uint_fast16_t SAMPLE_POINT_TARGET = 8750u;   // 87.5 %
    const uint_fast16_t SAMPLE_POINT_MAX    = 9000u;   // 90.0 %
    const uint_fast16_t SYNC_JUMP_WIDTH     = 1000u;   // 10.0 %
    const uint_fast16_t HUNDRED_PERCENT     = 10000u;  // 100.0 %
    const uint_fast8_t  ckdiv = configFDCAN_CKDIV_PDIV_VALUE == 0 ? 1u : (configFDCAN_CKDIV_PDIV_VALUE & 0x0fu) << 1;

    LOG_TRACE("Calculate bit timing for %u kbit/s in %s phase", (uint16_t) (target_bitrate / 1000U),
              is_nominal ? "arbitration" : "data");

    const uint32_t fdcan_tq_ck          = fdcan_clk / ckdiv;
    const uint32_t clock_cycles_per_bit = fdcan_tq_ck / target_bitrate;

    uint_fast16_t PRESCALER_MAX;
    uint_fast16_t BIT_SEGMENT_1_MAX;
    uint_fast16_t BIT_SEGMENT_2_MAX;

    if (is_nominal) {
        PRESCALER_MAX     = 512u;
        BIT_SEGMENT_1_MAX = 256u;
        BIT_SEGMENT_2_MAX = 128u;
    } else {
        PRESCALER_MAX     = 32u;
        BIT_SEGMENT_1_MAX = 32u;
        BIT_SEGMENT_2_MAX = 16u;
    }

    uint_fast16_t time_quanta_before_sample_point = 1 + BIT_SEGMENT_1_MAX;
    uint_fast16_t time_quanta_after_sample_point =
        time_quanta_before_sample_point * (HUNDRED_PERCENT - SAMPLE_POINT_MIN) / SAMPLE_POINT_MIN;

    uint_fast16_t prescaler = 1;
    uint_fast16_t time_quanta_per_bit =
        time_quanta_before_sample_point +
        (BIT_SEGMENT_2_MAX < time_quanta_after_sample_point ? BIT_SEGMENT_2_MAX : time_quanta_after_sample_point);

    uint_fast16_t best_bit_rate_prescaler = prescaler;
    uint_fast16_t best_bit_segment_1      = BIT_SEGMENT_1_MAX;
    uint_fast16_t best_bit_segment_2      = time_quanta_per_bit - time_quanta_before_sample_point;
    uint_fast16_t best_sample_point_error = HUNDRED_PERCENT;

    enum { eNONE = 0, eTIME_QUANTUM_ITERATION_END, eGOOD_SOLUTION_FOUND } break_reason = eNONE;

    while (break_reason == eNONE) {
        while (1) {
            const uint_fast16_t clock_cycles = prescaler * time_quanta_per_bit;
            if (clock_cycles > clock_cycles_per_bit) {
                time_quanta_per_bit--;
            } else if (clock_cycles < clock_cycles_per_bit) {
                prescaler++;
            } else {
                // we've found a possible solution
                break;
            }

            if (prescaler > PRESCALER_MAX || time_quanta_per_bit < MIN_TQ_PER_BIT) {
                // we've reached the end of our iteration, let's hope it's enough
                break_reason = eTIME_QUANTUM_ITERATION_END;
                break;
            }
        }

        time_quanta_after_sample_point = time_quanta_per_bit * (HUNDRED_PERCENT - SAMPLE_POINT_MAX) / HUNDRED_PERCENT;
        if (time_quanta_after_sample_point > BIT_SEGMENT_2_MAX) time_quanta_after_sample_point = BIT_SEGMENT_2_MAX;
        time_quanta_before_sample_point = time_quanta_per_bit - time_quanta_after_sample_point;
        if (time_quanta_before_sample_point > BIT_SEGMENT_1_MAX + 1) {
            time_quanta_before_sample_point = BIT_SEGMENT_1_MAX + 1;
            time_quanta_after_sample_point  = time_quanta_per_bit - time_quanta_before_sample_point;
        }
        uint_fast16_t last_sample_point_error = HUNDRED_PERCENT;

        while (1) {
            const uint_fast16_t sample_point = HUNDRED_PERCENT * time_quanta_before_sample_point / time_quanta_per_bit;
            if (sample_point < SAMPLE_POINT_MIN) {
                // try again with next prescaler value
                prescaler += 1;
                break;
            }

            const uint_fast16_t sample_point_error = abs((int) sample_point - (int) SAMPLE_POINT_TARGET);
            if (sample_point_error < best_sample_point_error) {
                // this is the best overall solution so far
                best_bit_rate_prescaler = prescaler;
                best_bit_segment_1      = time_quanta_before_sample_point - 1;
                best_bit_segment_2      = time_quanta_after_sample_point;
                best_sample_point_error = sample_point_error;
            } else if (sample_point_error > last_sample_point_error) {
                // we'll degrade from here, try again
                prescaler += 1;
                break;
            }

            if (sample_point_error < 100u /* 1 % */) {
                // this is enough, don't look any further
                break_reason = eGOOD_SOLUTION_FOUND;
                break;
            }

            last_sample_point_error = sample_point_error;
            time_quanta_after_sample_point++;
            time_quanta_before_sample_point--;
        }
    }

    time_quanta_per_bit                 = best_bit_segment_1 + best_bit_segment_2 + 1;
    uint_fast16_t max_resync_jump_width = SYNC_JUMP_WIDTH * time_quanta_per_bit / HUNDRED_PERCENT;
    if (max_resync_jump_width > best_bit_segment_2)
        max_resync_jump_width = best_bit_segment_2;
    else if (max_resync_jump_width < 1)
        max_resync_jump_width = 1;

    const BitTimings result = {.bit_rate_prescaler    = best_bit_rate_prescaler,
                               .bit_segment_1         = best_bit_segment_1,
                               .bit_segment_2         = best_bit_segment_2,
                               .max_resync_jump_width = max_resync_jump_width};

    if (break_reason != eGOOD_SOLUTION_FOUND) {
        LOG_WARNING("No good bit timing solution found for %u kbit/s in %s phase", (uint16_t) (target_bitrate / 1000),
                    is_nominal ? "arbitration" : "data");
    }

#if LOG_LEVEL <= LOG_LEVEL_TRACE
    const uint_fast32_t tq_ps        = 1000000000000ULL * result.bit_rate_prescaler / fdcan_tq_ck;
    const uint_fast16_t sample_point = HUNDRED_PERCENT * (result.bit_segment_1 + 1) / time_quanta_per_bit;
    LOG_TRACE("Timing solution found: prescaler = %u, tq = %2u.%3u ns, tbit = %4u ns (1 + %3u + %2u tq), "
              "SP at %2u.%1u %% after %3u ns, SJW = %3u ns (%2u tq)",
              result.bit_rate_prescaler,                                              // prescaler = %u,
              (uint_fast16_t) (tq_ps / 1000), (uint_fast16_t) (tq_ps % 1000),         // tq = %2u.%3u ns,
              (uint_fast16_t) ((tq_ps * time_quanta_per_bit + 500) / 1000),           // tbit = %4u ns
              result.bit_segment_1, result.bit_segment_2,                             // (1 + %3u + %2u tq),
              sample_point / 100, sample_point % 100 / 10,                            // SP at %2u.%1u %%
              (uint_fast16_t) ((tq_ps * (result.bit_segment_1 + 1) + 500) / 1000),    // after %3u ns,
              (uint_fast16_t) ((tq_ps * result.max_resync_jump_width + 500) / 1000),  // SJW = %3u ns
              result.max_resync_jump_width                                            // (%2u tq)
    );
#endif
    return result;
}

/**
 * @brief Enter Configuration Mode
 *
 * Enter configuration mode by first ensuring the interface is started, then setting the INIT bit in FDCAN_CCCR to enter
 * initialization mode and finally setting the CCE bit in FDCAN_CCCR.
 *
 * Software initialization is started by setting INIT bit in FDCAN_CCCR register. While INIT bit in FDCAN_CCCR register
 * is set, message transfer from and to the CAN bus is stopped, the status of the CAN bus output FDCAN_TX is recessive
 * (high). The EML (error management logic) counters are unchanged. Setting INIT bit in FDCAN_CCCR does not change any
 * configuration register. Access to the FDCAN configuration registers is only enabled when both INIT bit in FDCAN_CCCR
 * register and CCE bit in FDCAN_CCCR register are set.
 *
 * The following registers are reset when CCE bit in FDCAN_CCCR register is set:
 *   • FDCAN_HPMS - High priority message status
 *   • FDCAN_RXF0S - Rx FIFO 0 status
 *   • FDCAN_RXF1S - Rx FIFO 1 status
 *   • FDCAN_TXFQS - Tx FIFO/Queue status
 *   • FDCAN_TXBRP - Tx buffer request pending
 *   • FDCAN_TXBTO - Tx buffer transmission occurred
 *   • FDCAN_TXBCF - Tx buffer cancellation finished
 *   • FDCAN_TXEFS - Tx event FIFO status
 *
 * @param hw_index zero-based hardware interface index, e.g. 0 for FDCAN1
 */
static void enter_configuration_mode(const uint_fast8_t hw_index) {
    FDCAN_GlobalTypeDef* const iface = get_iface(hw_index);

    // start the interface prior to initialization
    PERIPH_BIT(iface->CCCR, FDCAN_CCCR_CSR_Pos) = 0;
    while (PERIPH_BIT(iface->CCCR, FDCAN_CCCR_CSA_Pos) != 0) {
        // wait for the interface to wake up
    }

    // enter initialization mode by setting CCCR.INIT (Initialization)
    PERIPH_BIT(iface->CCCR, FDCAN_CCCR_INIT_Pos) = 1;
    while (PERIPH_BIT(iface->CCCR, FDCAN_CCCR_INIT_Pos) == 0) {
        // wait for interface
    }

    // enable configuration change by setting CCCR.CCE (Configuration change enable)
    PERIPH_BIT(iface->CCCR, FDCAN_CCCR_CCE_Pos) = 1;
}

/**
 * @brief Exit Configuration Mode
 *
 * Exit configuration mode and (possibly) start interface operation.
 *
 * Clearing INIT bit in FDCAN_CCCR finishes the software initialization. Afterwards the bit stream processor (BSP)
 * synchronizes itself to the data transfer on the CAN bus by waiting for the occurrence of a sequence of 11 consecutive
 * recessive bits (Bus_Idle) before it can take part in bus activities and start the message transfer.
 *
 * When the controller is in Bus Off mode, this function may be called to start the reactivation sequence.
 * After clearing CCCR[INIT], the device waits for 129 occurrences of Bus Idle (129 × 11 consecutive recessive bits)
 * before resuming normal operation. No further action or re-initialization will be needed.
 *
 * @param hw_index zero-based hardware interface index, e.g. 0 for FDCAN1
 */
static void exit_configuration_mode(const uint_fast8_t hw_index) {
    FDCAN_GlobalTypeDef* const iface = get_iface(hw_index);

    // exit initialization mode by clearing CCCR.INIT (Initialization)
    PERIPH_BIT(iface->CCCR, FDCAN_CCCR_INIT_Pos) = 0;
    while (PERIPH_BIT(iface->CCCR, FDCAN_CCCR_INIT_Pos) != 0) {
        // wait for bit to be cleared
    }
}

// =================================================== PUBLIC API ======================================================

void fdcan_configure(const uint_fast8_t         hw_index,
                     const int32_t              nominal_bitrate,
                     const int32_t              data_bitrate,
                     const FDCANOperationalMode mode,
                     const bool                 use_fd) {
    FDCAN_GlobalTypeDef* const iface = get_iface(hw_index);

    enter_configuration_mode(hw_index);

    FDCAN_CONFIG->CKDIV = configFDCAN_CKDIV_PDIV_VALUE & FDCAN_CKDIV_PDIV_Msk;

    if (nominal_bitrate > 0) {
        BitTimings timings = calculate_bit_timings(nominal_bitrate, true);

        // set arbitration phase bitrate in NBTP (nominal bit timing and prescaler register)
        iface->NBTP = ((((uint32_t) timings.max_resync_jump_width - 1U) << FDCAN_NBTP_NSJW_Pos) & FDCAN_NBTP_NSJW_Msk) |
                      ((((uint32_t) timings.bit_rate_prescaler - 1U) << FDCAN_NBTP_NBRP_Pos) & FDCAN_NBTP_NBRP_Msk) |
                      ((((uint32_t) timings.bit_segment_1 - 1U) << FDCAN_NBTP_NTSEG1_Pos) & FDCAN_NBTP_NTSEG1_Msk) |
                      ((((uint32_t) timings.bit_segment_2 - 1U) << FDCAN_NBTP_NTSEG2_Pos) & FDCAN_NBTP_NTSEG2_Msk);
    }
    if (use_fd) {
        if (data_bitrate > 0) {
            BitTimings timings = calculate_bit_timings(data_bitrate, false);

            // set data phase bitrate in DBTP (data bit timing and prescaler register)
            iface->DBTP =
                (configFDCAN_ENABLE_TRANSCEIVER_DELAY_COMPENSATION ? FDCAN_DBTP_TDC : 0) |
                ((((uint32_t) timings.max_resync_jump_width - 1U) << FDCAN_DBTP_DSJW_Pos) & FDCAN_DBTP_DSJW_Msk) |
                ((((uint32_t) timings.bit_rate_prescaler - 1U) << FDCAN_DBTP_DBRP_Pos) & FDCAN_DBTP_DBRP_Msk) |
                ((((uint32_t) timings.bit_segment_1 - 1U) << FDCAN_DBTP_DTSEG1_Pos) & FDCAN_DBTP_DTSEG1_Msk) |
                ((((uint32_t) timings.bit_segment_2 - 1U) << FDCAN_DBTP_DTSEG2_Pos) & FDCAN_DBTP_DTSEG2_Msk);

            // configure SSP
            const uint32_t ssp_offset_mtq = timings.bit_segment_1 * timings.bit_rate_prescaler;
            if (ssp_offset_mtq > 64) LOG_INFO("TDCO = %u, consider lowering fdcan_tq_ck", (uint16_t) ssp_offset_mtq);
            MODIFY_REG(iface->TDCR, FDCAN_TDCR_TDCO, (ssp_offset_mtq << FDCAN_TDCR_TDCO_Pos) & FDCAN_TDCR_TDCO_Msk);
        } else {
            // don't touch bit timing, but make sure the transceiver delay compensation configuration is up to date
            PERIPH_BIT(iface->DBTP, FDCAN_DBTP_TDC_Pos) = configFDCAN_ENABLE_TRANSCEIVER_DELAY_COMPENSATION ? 1 : 0;
        }
        // enable CAN FD by setting CCCR.FDOE (FD operation enable) and CCCR.BRSE (FDCAN bit rate switching)
        SET_BIT(iface->CCCR, FDCAN_CCCR_FDOE | FDCAN_CCCR_BRSE);
    } else {
        // disable CAN FD by clearing CCCR.FDOE (FD operation enable) and CCCR.BRSE (FDCAN bit rate switching)
        CLEAR_BIT(iface->CCCR, FDCAN_CCCR_FDOE | FDCAN_CCCR_BRSE);
    }

    PERIPH_BIT(iface->CCCR, FDCAN_CCCR_TXP_Pos)  = configFDCAN_ENABLE_TRANSMIT_PAUSE;
    PERIPH_BIT(iface->CCCR, FDCAN_CCCR_EFBI_Pos) = configFDCAN_ENABLE_EDGE_FILTERING;
    PERIPH_BIT(iface->CCCR, FDCAN_CCCR_DAR_Pos)  = configFDCAN_DISABLE_AUTOMATIC_RETRANSMISSION;
    PERIPH_BIT(iface->CCCR, FDCAN_CCCR_ASM_Pos)  = mode == eFDCAN_MODE_RESTRICTED;
    PERIPH_BIT(iface->CCCR, FDCAN_CCCR_MON_Pos) =
        mode == eFDCAN_MODE_MONITORING || mode == eFDCAN_MODE_LOOPBACK_INTERNAL;
    PERIPH_BIT(iface->CCCR, FDCAN_CCCR_TEST_Pos) =
        mode == eFDCAN_MODE_LOOPBACK_INTERNAL || mode == eFDCAN_MODE_LOOPBACK_EXTERNAL;
    PERIPH_BIT(iface->TEST, FDCAN_TEST_LBCK_Pos) =
        mode == eFDCAN_MODE_LOOPBACK_INTERNAL || mode == eFDCAN_MODE_LOOPBACK_EXTERNAL;

    // TXBC (Tx buffer configuration register)
    //   TFQM:      Tx FIFO/queue mode; 0: FIFO operation, 1: queue operation
    PERIPH_BIT(iface->TXBC, FDCAN_TXBC_TFQM_Pos) = 1;  // queue operation

    // TSCC (timestamp counter configuration register)
    //   TSS[1:0]:  Timestamp select; 2: external timestamp from TIM3
    iface->TSCC = 0b10 << FDCAN_TSCC_TSS_Pos;

    // RXGFC (global filter configuration register)
    //   LSE[3:0]:  List size extended, number of extended message ID filter elements
    //   FnOM:      FIFO n operation mode (overwrite or blocking); 0: blocking
    //   ANFS[1:0]: Accept non-matching frames standard; 1: Rx FIFO 1, 3: reject
    //   ANFE[1:0]: Accept non-matching frames extended; 1: Rx FIFO 1, 3: reject
    //   RRFS:      Reject remote frames standard; 1: reject
    //   RRFE:      Reject remote frames extended; 1: reject
    iface->RXGFC = (((SRAMCAN_FLE_NBR << FDCAN_RXGFC_LSE_Pos) & FDCAN_RXGFC_LSE_Msk) |
                    ((configFDCAN_ACCEPT_NON_MATCHING_STANDARD_FRAMES ? 1 : 3) << FDCAN_RXGFC_ANFS_Pos) |
                    ((configFDCAN_ACCEPT_NON_MATCHING_EXTENDED_FRAMES ? 1 : 3) << FDCAN_RXGFC_ANFE_Pos) |
                    ((configFDCAN_REJECT_REMOTE_STANDARD_FRAMES ? 1 : 0) << FDCAN_RXGFC_RRFS_Pos) |
                    ((configFDCAN_REJECT_REMOTE_EXTENDED_FRAMES ? 1 : 0) << FDCAN_RXGFC_RRFE_Pos));

    exit_configuration_mode(hw_index);
}

// =========================================== PUBLIC FUNCTIONS (KIND OF) ==============================================

/**
 * @brief Configure Interface Interrupts for Transport Layer Integration
 *
 * This function enables a selection of FDCAN interrupts, routes them to the FDCANx_IT1_IRQ line, sets the interrupt's
 * priority to configFDCAN_IT1_PRIORITY and enables the interrupt.
 * If this function is called with notification_target == NULL, the exact opposite is performed to disable the interrupt
 * and unregister the interface from the transport worker task.
 *
 * In any case the notification target handle and (arbitrary) interface index are stored statically for the ISR to find.
 *
 * This function is not intended to be called directly. Instead, it should be proxied through a function hard-coding
 * hw_index to a fixed value, providing a function matching the signature of MediaIOInterface::install_hooks().
 *
 * @param notification_target handle of transport task to notify of events, NULL to disable/unregister
 * @param interface_index interface index to send notifications for, may differ from hardware index
 * @param hw_index zero-based hardware interface index, e.g. 0 for FDCAN1
 *
 * @see MediaIOInterface::install_hooks
 */
static void install_isr_hooks(const TaskHandle_t notification_target,
                              const uint_fast8_t interface_index,
                              const uint_fast8_t hw_index) {
    ASSERT(hw_index < FDCAN_INTERFACE_COUNT);
    interface_configuration[hw_index].notification_target = notification_target;
    interface_configuration[hw_index].interface_index     = interface_index;

    FDCAN_GlobalTypeDef* const iface = get_iface(hw_index);

    // Available interrupts
    //   ARA:   Access to reserved address
    //   PED:   Protocol error in data phase (data bit time is used)
    //   PEA:   Protocol error in arbitration phase (nominal bit time is used)
    //   WDI:   Watchdog interrupt
    //   BO:    Bus_Off status
    //   EW:    Warning status
    //   EP:    Error passive
    //   ELO:   Error logging overflow
    //   TOO:   Timeout occurred
    //   MRAF:  Message RAM access failure
    //   TSW:   Timestamp wraparound
    //   TEFL:  Tx event FIFO element lost
    //   TEFF:  Tx event FIFO full
    //   TEFN:  Tx event FIFO New Entry
    //   TFE:   Tx FIFO empty
    //   TCF:   Transmission cancellation finished
    //   TC:    Transmission completed
    //   HPM:   High-priority message
    //   RF1L:  Rx FIFO 1 message lost
    //   RF1F:  Rx FIFO 1 full
    //   RF1N:  Rx FIFO 1 new message
    //   RF0L:  Rx FIFO 0 message lost
    //   RF0F:  Rx FIFO 0 full
    //   RF0N:  Rx FIFO 0 new message
    const uint32_t IE_MASK = FDCAN_IE_RF0NE | FDCAN_IE_RF1NE | FDCAN_IE_TCE | FDCAN_IE_TEFNE | FDCAN_IE_EWE |
                             FDCAN_IE_EPE | FDCAN_IE_BOE | FDCAN_IE_ELOE | FDCAN_IE_PEAE | FDCAN_IE_PEDE;

    // Each Tx buffer has its own TIE bit, triggering the Transmission Completed (TC) interrupt. Enable all.
    const uint32_t TXBTIE_MASK = FDCAN_TXBTIE_TIE_Msk;

    // ILS (interrupt line select register)
    //   PERR: Protocol error
    //       ARAL: Access to reserved address line
    //       PEDL: Protocol error in data phase line
    //       PEAL: Protocol error in arbitration phase line
    //       WDIL: Watchdog interrupt line
    //       BOL: Bus_Off status
    //       EWL: Warning status interrupt line
    //   BERR: Bit and line error
    //       EPL Error passive interrupt line
    //       ELOL: Error logging overflow interrupt line
    //   MISC:
    //       TOOL: Timeout occurred interrupt line
    //       MRAFL: Message RAM access failure interrupt line
    //       TSWL: Timestamp wraparound interrupt line
    //   TFERR: Tx FIFO ERROR
    //       TEFLL: Tx event FIFO element lost interrupt line
    //       TEFFL: Tx event FIFO full interrupt line
    //       TEFNL: Tx event FIFO new entry interrupt line
    //       TFEL: Tx FIFO empty interrupt line
    //   SMSG: Status message
    //       TCFL: Transmission cancellation finished interrupt line
    //       TCL: Transmission completed interrupt line
    //       HPML: High-priority message interrupt line
    //   RxFIFO1: RX FIFO 1
    //       RF1LL: Rx FIFO 1 message lost interrupt line
    //       RF1FL: Rx FIFO 1 full Interrupt line
    //       RF1NL: Rx FIFO 1 new message interrupt line
    //   RxFIFO0: RX FIFO 0
    //       RF0LL: Rx FIFO 0 message lost interrupt line
    //       RF0FL: Rx FIFO 0 full interrupt line
    //       RF0NL: Rx FIFO 0 new message interrupt line
    const uint32_t ILS_MASK =
        FDCAN_ILS_RXFIFO0 | FDCAN_ILS_RXFIFO1 | FDCAN_ILS_SMSG | FDCAN_ILS_TFERR | FDCAN_ILS_BERR | FDCAN_ILS_PERR;

    // CMSIS
    IRQn_Type FDCANx_IT1_IRQn;

    switch (hw_index) {
        case 0:
            FDCANx_IT1_IRQn = FDCAN1_IT1_IRQn;
            break;
#ifdef FDCAN2
        case 1:
            FDCANx_IT1_IRQn = FDCAN2_IT1_IRQn;
            break;
#endif
#ifdef FDCAN3
        case 2:
            FDCANx_IT1_IRQn = FDCAN3_IT1_IRQn;
            break;
#endif
        default:
            // we should never reach here
            WTF();
    }

    if (notification_target != NULL) {
        NVIC_SetPriority(FDCANx_IT1_IRQn, configFDCAN_IT1_PRIORITY);

        iface->IE |= IE_MASK;
        iface->TXBTIE |= TXBTIE_MASK;
        iface->ILS |= ILS_MASK;
        iface->ILE |= FDCAN_ILE_EINT1;

        NVIC_EnableIRQ(FDCANx_IT1_IRQn);
    } else {
        iface->IE &= ~IE_MASK;
        iface->TXBTIE &= ~TXBTIE_MASK;
        iface->ILS &= ~ILS_MASK;
        iface->ILE &= ~FDCAN_ILE_EINT1;

        NVIC_DisableIRQ(FDCANx_IT1_IRQn);
    }
}

/**
 * @brief Push a CanardTxQueueItem to the Media IO Layer for Transmission
 *
 * This is the only API function to get a frame onto the wire. The interface is simple - offer a frame and the current
 * timestamp to an interface: when the function returns true the frame has been handled and may be freed by the caller,
 * when this function returns false the frame should be re-queued at a later time.
 *
 * push_frame manages the interface's Tx queue to minimize the likelyhood of an inner priority inversion happening (see
 * UAVCAN specification section 4.2.4.3) by only queuing frames with a higher priority (smaller numeric identifier) than
 * those already present in the queue.
 *
 * If the frame's transmission deadline has expired upon calling this function, push_frame returns true without actually
 * adding the frame to the Tx queue, indicating the frame may be discarded by the caller. A return value of true thus
 * does *not* guarantee a successful transmission.
 *
 * This function is not intended to be called directly. Instead, it should be proxied through a function hard-coding
 * hw_index to a fixed value, providing a function matching the signature of MediaIOInterface::push_frame().
 *
 * @param current_time timestamp the interface driver shall compare tx_item->tx_deadline_usec to
 * @param tx_item reference to canard queue item containing the frame and a tx deadline
 * @param hw_index zero-based hardware interface index, e.g. 0 for FDCAN1
 * @return true if the frame has been handled and may be discarded,
 *         false if it shall be pushed again at a later time
 *
 * @see MediaIOInterface::push_frame
 */
static bool push_frame(const CanardMicrosecond        current_time,
                       const CanardTxQueueItem* const tx_item,
                       const uint_fast8_t             hw_index) {
    static_assert(sizeof(FDCANMessageRAM) == SRAMCAN_SIZE, "Internal Constraint Violation");

    if (current_time > tx_item->tx_deadline_usec) {
        LOG_INFO("TX deadline exceeded for ID %lx", tx_item->frame.extended_can_id);
        // we report success, the frame may be discarded since it has expired
        return true;
    }

    FDCAN_GlobalTypeDef* const iface = get_iface(hw_index);

    if (iface->TXFQS & FDCAN_TXFQS_TFQF) {
        // TX FIFO/Queue full, no transmission just yet
        LOG_TRACE("TX FIFO/Queue full");
        // the frame might get transmitted at a later point, don't discard it just yet
        return false;
    }

    const uint32_t highest_priority_queued = get_highest_priority_queued(hw_index);
    if (!(tx_item->frame.extended_can_id < highest_priority_queued)) {
        // only queue higher priority items
        LOG_TRACE("TFQ blocked by ID %lx", highest_priority_queued);
        return false;
    }

    // read the Tx FIFO/queue write index from TXFQS (Tx FIFO/queue status register)
    //   TFQPI[1:0]: Tx FIFO/queue put index
    const uint_fast8_t tfq_put_index = (iface->TXFQS & FDCAN_TXFQS_TFQPI_Msk) >> FDCAN_TXFQS_TFQPI_Pos;
    // place message in corresponding RAM section
    volatile FDCANTxBufferElement* const txbuf = &(get_message_ram(hw_index)->TFQ[tfq_put_index]);

    txbuf->word1 =
        tx_item->frame.extended_can_id | FDCAN_ELEMENT_XTD | (iface->PSR & FDCAN_PSR_EP ? FDCAN_ELEMENT_ESI : 0);
    // The BRS and FDF bits are only evaluated when the corresponding bits are set in CCCR.
    // It is therefore safe to set them, since both bits will be ignored in classic CAN mode.
    // If we're using CAN FD (CCCR.FDOE) we always want to use bit rate switching (CCCR.BRSE) as well.
    txbuf->word2 =
        ((uint32_t) CanardCANLengthToDLC[tx_item->frame.payload_size] << 16) | FDCAN_ELEMENT_BRS | FDCAN_ELEMENT_FDF;

    // STM32G4 Series Reference Manual (RM0440), 44.3.3 Message RAM, page 1958:
    //     When the FDCAN addresses the Message RAM, it addresses 32-bit words (aligned), not a
    //     single byte. The RAM addresses are 32-bit words, i.e. only bits 15 to 2 are evaluated, the
    //     two least significant bits are ignored.
    //
    // Both the CanardFrame payload buffer and the message RAM are word-aligned, therefore both memory regions can be
    // addressed by word. The payload buffer was allocated by the CanardInstance's memory_allocate, which only allocates
    // memory word-aligned. Thus, even for a payload of 13 bytes the full four words would be allocated in the buffer.
    uint32_t* src = (uint32_t*) tx_item->frame.payload;
    uint32_t* dst = (uint32_t*) txbuf->data_bytes;

    for (uint32_t index = 0; index < tx_item->frame.payload_size; index += 4) {
        *dst++ = *src++;
    }

    // request transmission by setting the corresponding bit in TXBAR (Tx buffer add request register)
    iface->TXBAR = (uint32_t) 1 << tfq_put_index;

    LOG_TRACE("frame queued at index %u", tfq_put_index);

    return true;
}

/**
 * @brief Query Interface for Received Frames
 *
 * This is the only API function to read frames from the Rx FIFOs. FIFO0 is checked first, FIFO1 second. This introduces
 * a chance for priority inversion on a busy bus/busy µC, but the effects are assumed to be small in normal operation.
 *
 * A received frame and its corresponding payload are copied into out_canard_frame.
 * ATTENTION: out_canard_frame's payload buffer needs to be allocated and big enough for the transport's MTU!
 *
 * This function is not intended to be called directly. Instead, it should be proxied through a function hard-coding
 * hw_index to a fixed value, providing a function matching the signature of MediaIOInterface::pop_frame().
 *
 * @param out_canard_frame output buffer to place the received frame in
 * @param out_rx_timestamp output buffer for reception timestamp, shall be set to 0 if not implemented
 * @param hw_index zero-based hardware interface index, e.g. 0 for FDCAN1
 * @return true if a frame was copied from the RX queue to *out_canard_frame,
 *         false if the queue is exhausted and no further frames are available
 *
 * @see MediaIOInterface::pop_frame
 */
static bool pop_frame(CanardFrame* const out_canard_frame,
                      uint64_t* const    out_rx_timestamp,
                      const uint_fast8_t hw_index) {
    LOG_TRACE("poll RX frame from CAN%u", hw_index + 1);

    FDCAN_GlobalTypeDef* const iface = get_iface(hw_index);

    volatile FDCANRxFIFOElement* rx_fifo_element;
    uint_fast8_t                 fifo_number;
    uint_fast8_t                 fifo_index;
    volatile uint32_t*           ack_register;

    if ((iface->RXF0S & FDCAN_RXF0S_F0FL) > 0) {
        fifo_number     = 0;
        fifo_index      = (iface->RXF0S & FDCAN_RXF0S_F0GI_Msk) >> FDCAN_RXF0S_F0GI_Pos;
        ack_register    = &iface->RXF0A;
        rx_fifo_element = &(get_message_ram(hw_index)->RF0[fifo_index]);
    } else if ((iface->RXF1S & FDCAN_RXF1S_F1FL) > 0) {
        fifo_number     = 1;
        fifo_index      = (iface->RXF1S & FDCAN_RXF1S_F1GI_Msk) >> FDCAN_RXF1S_F1GI_Pos;
        ack_register    = &iface->RXF1A;
        rx_fifo_element = &(get_message_ram(hw_index)->RF1[fifo_index]);
    } else {
        LOG_TRACE("Rx FIFOs empty");
        return false;
    }
    (void) fifo_number;

    LOG_TRACE("Found element in Rx FIFO %u at index %u", fifo_number, fifo_index);

    if (out_rx_timestamp) *out_rx_timestamp = unwrap_partial_past_timestamp(rx_fifo_element->RXTS);
    out_canard_frame->extended_can_id = rx_fifo_element->identifier;
    out_canard_frame->payload_size    = CanardCANDLCToLength[rx_fifo_element->DLC];

    // word-aligned write, this is intentional
    uint32_t* src = (uint32_t*) rx_fifo_element->data_bytes;
    uint32_t* dst = (uint32_t*) out_canard_frame->payload;

    for (uint32_t index = 0; index < out_canard_frame->payload_size; index += 4) {
        *dst++ = *src++;
    }

    // STM32G4 Series Reference Manual (RM0440), 44.4.23 CAN Rx FIFO 0 acknowledge register (FDCAN_RXF0A), page 1995:
    //     After the Host has read a message or a sequence of messages from Rx FIFO n it has to
    //     write the buffer index of the last element read from Rx FIFO n to FnAI. This sets the Rx
    //     FIFO n get index RXFnS[FnGI] to FnAI + 1 and update the FIFO n fill level RXFnS[FnFL].
    *ack_register = fifo_index;

    return true;
}

/**
 * @brief Configure Hardware Acceptance Filter for Interface
 *
 * Set acceptance filter <index> to (extended) ID and mask specified in CanardFilter instance. To make use of the FDCAN
 * core's two independent Rx FIFOs, incoming frames matched by filters at even positions are routed to FIFO0, while
 * frames matched by filters at odd positions are placed in FIFO1.
 *
 * An all-zero filter.extended_mask disables the acceptance filter at <index>.
 *
 * This function is not intended to be called directly. Instead, it should be proxied through a function hard-coding
 * hw_index to a fixed value, providing a function matching the signature of MediaIOInterface::set_filter().
 *
 * @param filter CanardFilter instance with ID and Mask
 * @param index position in the interface's filter bank index to write this filter to
 * @param hw_index zero-based hardware interface index, e.g. 0 for FDCAN1
 *
 * @see MediaIOInterface::set_filter
 */
static void set_acceptance_filter(const CanardFilter filter, const uint_fast8_t index, const uint_fast8_t hw_index) {
    ASSERT(index < SRAMCAN_FLE_NBR);
    volatile FDCANExtIDFilterElement* const filter_element = &(get_message_ram(hw_index)->FLE[index]);
    LOG_TRACE("Set CAN%u filter %u to ID 0x%08lx, mask 0x%08lx", hw_index + 1, index, filter.extended_can_id,
              filter.extended_mask);

    if (filter.extended_mask == 0) {
        // disable filter
        *filter_element = (FDCANExtIDFilterElement){{0UL}, {0UL}};
    } else {
        *filter_element = (FDCANExtIDFilterElement){
            .EFEC  = (index & 1) ? eEFEC_TO_RXFIFO1 : eEFEC_TO_RXFIFO0,  // distribute odd/even filters to FIFO 1/0
            .EFT   = eEFT_MASK,
            .EFID1 = filter.extended_can_id,
            .EFID2 = filter.extended_mask,
        };
    }
}

/**
 * @brief Collect State Metrics from Interface
 *
 * This function collects detailled error metrics from the interface's FDCAN_ECR and FDCAN_PSR registers.
 * Any significant changes in error state or count will get logged via the logging framework. Note, however, that the
 * UAVCAN logging handler is disabled for this module, therefore logs will only be emitted via other handlers.
 *
 * If out is not NULL, a detailled bus error stats report will be written to *out.
 *
 * This function is not intended to be called directly. Instead, it should be proxied through a function hard-coding
 * hw_index to a fixed value, providing a function matching the signature of MediaIOInterface::get_stats().
 *
 * @param out BusErrorStats struct to write the information to, or NULL if no detailled report is required
 * @param hw_index zero-based hardware interface index, e.g. 0 for FDCAN1
 * @return number of new bus errors since the last call for this interface
 *
 * @see MediaIOInterface::get_stats
 */
static uint_fast8_t get_error_stats(BusErrorStats* const out, const uint_fast8_t hw_index) {
    static BusErrorStats last_error_stats[FDCAN_INTERFACE_COUNT]     = {{0}};
    static uint32_t      cumulated_bus_errors[FDCAN_INTERFACE_COUNT] = {0};
    static TickType_t    reactivate_bus_at[FDCAN_INTERFACE_COUNT]    = {0};

    FDCAN_GlobalTypeDef* const iface = get_iface(hw_index);
    uint32_t const             ECR   = iface->ECR;
    uint32_t const             PSR   = iface->PSR;

    uint_fast8_t const new_logged_errors = (ECR & FDCAN_ECR_CEL_Msk) >> FDCAN_ECR_CEL_Pos;
    cumulated_bus_errors[hw_index] += new_logged_errors;

    BusErrorStats stats = {
        .receive_error_counter  = (ECR & FDCAN_ECR_REC_Msk) >> FDCAN_ECR_REC_Pos,
        .transmit_error_counter = (ECR & FDCAN_ECR_TEC_Msk) >> FDCAN_ECR_TEC_Pos,
        .error_warning_flag     = (PSR & FDCAN_PSR_EW_Msk) ? 1 : 0,
        .error_passive_flag     = (PSR & FDCAN_PSR_EP_Msk) ? 1 : 0,
        .bus_off_flag           = (PSR & FDCAN_PSR_BO_Msk) ? 1 : 0,
        .last_error_code        = (PSR & FDCAN_PSR_LEC_Msk) >> FDCAN_PSR_LEC_Pos,
        .data_last_error_code   = (PSR & FDCAN_PSR_DLEC_Msk) >> FDCAN_PSR_DLEC_Pos,
    };

    stats.state = (stats.bus_off_flag
                       ? eBusErrorBusOff
                       : (stats.error_passive_flag ? eBusErrorPassive
                                                   : (stats.error_warning_flag ? eBusErrorWarning : eBusErrorActive)));

    static_assert(sizeof(stats) == sizeof(uint32_t), "the below comparison casts stats to an uint32_t for comparison");
    if (((*(uint32_t*) &stats) & 0x00ffffffu) != ((*(uint32_t*) &last_error_stats[hw_index]) & 0x00ffffffu) ||
        new_logged_errors > 0)
    {
        LOG_DEBUG("CAN%u: LEC = %u, DLEC = %u, REC = %3u, TEC = %3u, CEL = %lu", hw_index + 1, stats.last_error_code,
                  stats.data_last_error_code, stats.receive_error_counter, stats.transmit_error_counter,
                  cumulated_bus_errors[hw_index]);
    }

    if (stats.state != last_error_stats[hw_index].state) {
        char               preamble[48];
        const bool         improvement = stats.state < last_error_stats[hw_index].state;
        const char* const  direction   = improvement ? "down" : "up";
        const unsigned int len = sprintf(preamble, "CAN%u Error State went %s to Error ", hw_index + 1, direction);
        ASSERT(len < sizeof(preamble));

        switch (stats.state) {
            case eBusErrorActive: {
                LOG_NOTICE("%sActive.", preamble);
                break;
            }

            case eBusErrorWarning: {
                LOG_WARNING("%sActive (Warning).", preamble);
                break;
            }

            case eBusErrorPassive: {
                LOG_ERROR("%sPassive.", preamble);
                break;
            }

            case eBusErrorBusOff: {
                LOG_CRITICAL("%sBus Off.", preamble);
                // this is an arbitrary back-off duration to limit dampen the state oscillation
                reactivate_bus_at[hw_index] = xTaskGetTickCount() + pdMS_TO_TICKS(250);
                break;
            }

            default:
                WTF();
        }
    }

    if (stats.state == eBusErrorBusOff && xTaskGetTickCount() >= reactivate_bus_at[hw_index]) {
        // STM32G4 Series Reference Manual (RM0440), 44.4.13 FDCAN protocol status register (FDCAN_PSR), page 1948:
        //
        //     The Bus_Off recovery sequence (see CAN Specification Rev. 2.0 or ISO11898-1) cannot be shortened by
        //     setting or resetting CCCR[INIT]. If the device goes Bus_Off, it sets CCCR.INIT of its own, stopping all
        //     bus activities. Once CCCR[INIT] has been cleared by the CPU, the device then waits for 129 occurrences of
        //     Bus Idle (129 × 11 consecutive recessive bits) before resuming normal operation. At the end of the
        //     Bus_Off recovery sequence, the Error Management Counters are reset. During the waiting time after the
        //     reset of CCCR[INIT], each time a sequence of 11 recessive bits has been monitored, a Bit0 Error code is
        //     written to PSR[LEC], enabling the CPU to readily check up whether the CAN bus is stuck at dominant or
        //     continuously disturbed and to monitor the Bus_Off recovery sequence. ECR[REC] is used to count these
        //     sequences.

        exit_configuration_mode(hw_index);
    }

    // Monitor the determined Transceiver Delay Compensation Value – if it is too high,
    // something may be wrong with the transceiver or the bus.
    const uint_fast8_t TDCV = ((PSR & FDCAN_PSR_TDCV_Msk) >> FDCAN_PSR_TDCV_Pos);
    if (TDCV > 112u) LOG_WARNING("TDCV = %u", TDCV);

    last_error_stats[hw_index] = stats;

    const uint32_t IR = iface->IR & ~FDCAN_IR_TSW;  // ignore timestamp wraparound, this is handled by the timekeeper
    if (IR) {
        LOG_WARNING("FDCAN%u->IR = 0x%08lx", hw_index + 1, IR);
        iface->IR = IR;
    }

    if (out) *out = stats;
    return new_logged_errors;
}

// ========================================= HARDWARE PLATFORM ABSTRACTION =============================================

#ifdef FDCAN1
void fdcan1_install_isr_hooks(const TaskHandle_t notification_target, const uint_fast8_t interface_index) {
    install_isr_hooks(notification_target, interface_index, 0u);
}
bool fdcan1_push_frame(const CanardMicrosecond current_time, const CanardTxQueueItem* const tx_item) {
    return push_frame(current_time, tx_item, 0u);
}
bool fdcan1_pop_frame(CanardFrame* const out_canard_frame, uint64_t* const out_rx_timestamp) {
    return pop_frame(out_canard_frame, out_rx_timestamp, 0u);
}
void fdcan1_set_acceptance_filter(const CanardFilter filter, const uint_fast8_t index) {
    set_acceptance_filter(filter, index, 0u);
}
uint_fast8_t fdcan1_get_error_stats(BusErrorStats* const out_bus_error_stats) {
    return get_error_stats(out_bus_error_stats, 0u);
}

const MediaIOInterface FDCAN1MediaIO = {.install_hooks = fdcan1_install_isr_hooks,
                                        .push_frame    = fdcan1_push_frame,
                                        .pop_frame     = fdcan1_pop_frame,
                                        .set_filter    = fdcan1_set_acceptance_filter,
                                        .num_filters   = SRAMCAN_FLE_NBR,
                                        .get_stats     = fdcan1_get_error_stats};
#endif
#ifdef FDCAN2
void fdcan2_install_isr_hooks(const TaskHandle_t notification_target, const uint_fast8_t interface_index) {
    install_isr_hooks(notification_target, interface_index, 1u);
}
bool fdcan2_push_frame(const CanardMicrosecond current_time, const CanardTxQueueItem* const tx_item) {
    return push_frame(current_time, tx_item, 1u);
}
bool fdcan2_pop_frame(CanardFrame* const out_canard_frame, uint64_t* const out_rx_timestamp) {
    return pop_frame(out_canard_frame, out_rx_timestamp, 1u);
}
void fdcan2_set_acceptance_filter(const CanardFilter filter, const uint_fast8_t index) {
    set_acceptance_filter(filter, index, 1u);
}
uint_fast8_t fdcan2_get_error_stats(BusErrorStats* const out_bus_error_stats) {
    return get_error_stats(out_bus_error_stats, 1u);
}

const MediaIOInterface FDCAN2MediaIO = {.install_hooks = fdcan2_install_isr_hooks,
                                        .push_frame    = fdcan2_push_frame,
                                        .pop_frame     = fdcan2_pop_frame,
                                        .set_filter    = fdcan2_set_acceptance_filter,
                                        .num_filters   = SRAMCAN_FLE_NBR,
                                        .get_stats     = fdcan2_get_error_stats};
#endif
#ifdef FDCAN3
void fdcan3_install_isr_hooks(const TaskHandle_t notification_target, const uint_fast8_t interface_index) {
    install_isr_hooks(notification_target, interface_index, 2u);
}
bool fdcan3_push_frame(const CanardMicrosecond current_time, const CanardTxQueueItem* const tx_item) {
    return push_frame(current_time, tx_item, 2u);
}
bool fdcan3_pop_frame(CanardFrame* const out_canard_frame, uint64_t* const out_rx_timestamp) {
    return pop_frame(out_canard_frame, out_rx_timestamp, 2u);
}
void fdcan3_set_acceptance_filter(const CanardFilter filter, const uint_fast8_t index) {
    set_acceptance_filter(filter, index, 2u);
}
uint_fast8_t fdcan3_get_error_stats(BusErrorStats* const out_bus_error_stats) {
    return get_error_stats(out_bus_error_stats, 2u);
}

const MediaIOInterface FDCAN3MediaIO = {.install_hooks = fdcan3_install_isr_hooks,
                                        .push_frame    = fdcan3_push_frame,
                                        .pop_frame     = fdcan3_pop_frame,
                                        .set_filter    = fdcan3_set_acceptance_filter,
                                        .num_filters   = SRAMCAN_FLE_NBR,
                                        .get_stats     = fdcan3_get_error_stats};
#endif

// =============================================== INTERRUPT HANDLING ==================================================

void FDCANx_IT1_IRQHandler(const uint_fast8_t hw_index) {
    const uint32_t FRAME_RECEIVED    = FDCAN_IR_RF0N | FDCAN_IR_RF1N;
    const uint32_t FRAME_TRANSMITTED = FDCAN_IR_TC | FDCAN_IR_TEFN;
    const uint32_t BUS_STATE_CHANGED =
        FDCAN_IR_PEA | FDCAN_IR_PED | FDCAN_IR_EW | FDCAN_IR_EP | FDCAN_IR_BO | FDCAN_IR_ELO;

    const TaskHandle_t target   = interface_configuration[hw_index].notification_target;
    const uint_fast8_t tr_index = interface_configuration[hw_index].interface_index;

    if (target == NULL) return;

    FDCAN_GlobalTypeDef* const iface        = get_iface(hw_index);
    BaseType_t                 should_yield = pdFALSE;

    if (iface->IR & FRAME_RECEIVED) {
        xTaskNotifyFromISR(target, TRANSPORT_FRAME_RECEIVED(tr_index), eSetBits, &should_yield);
        iface->IR = FRAME_RECEIVED;  // clear bits
    }

    if (iface->IR & FRAME_TRANSMITTED) {
        xTaskNotifyFromISR(target, TRANSPORT_FRAME_TRANSMITTED(tr_index), eSetBits, &should_yield);
        iface->IR = FRAME_TRANSMITTED;  // clear bits
    }

    if (iface->IR & BUS_STATE_CHANGED) {
        xTaskNotifyFromISR(target, TRANSPORT_BUS_STATE_CHANGED(tr_index), eSetBits, &should_yield);
        iface->IR = BUS_STATE_CHANGED;  // clear bits
    }

    portYIELD_FROM_ISR(should_yield);
}

#if configFDCAN_INSTALL_INTERRUPT_SERVICE_ROUTINES

#    ifdef FDCAN1
void FDCAN1_IT1_IRQHandler(void) { FDCANx_IT1_IRQHandler(0u); }
#    endif
#    ifdef FDCAN2
void FDCAN2_IT1_IRQHandler(void) { FDCANx_IT1_IRQHandler(1u); }
#    endif
#    ifdef FDCAN3
void FDCAN3_IT1_IRQHandler(void) { FDCANx_IT1_IRQHandler(2u); }
#    endif

#endif
