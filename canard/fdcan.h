/**
 * @file fdcan.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Libcanard Media IO Driver for STM32 FDCAN Peripheral
 * @version 0.2
 *
 * @copyright Copyright (c) 2022 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#pragma once

#include <main.h>

#include "transport.h"
#include "canard.h"

// =========================================== MODULE LEVEL CONFIGURATION ==============================================

#ifndef configFDCAN_INSTALL_INTERRUPT_SERVICE_ROUTINES
#    define configFDCAN_INSTALL_INTERRUPT_SERVICE_ROUTINES 1
#endif

#ifndef configFDCAN_IT1_PRIORITY
// Set to highest possible priority which can still interface with FreeRTOS
#    define configFDCAN_IT1_PRIORITY (configMAX_SYSCALL_INTERRUPT_PRIORITY >> (8 - configPRIO_BITS))
#endif

#ifndef configFDCAN_LOG_LEVEL
#    define configFDCAN_LOG_LEVEL LOG_LEVEL_TRACE
#endif

#ifndef configFDCAN_DISABLE_AUTOMATIC_RETRANSMISSION
#    define configFDCAN_DISABLE_AUTOMATIC_RETRANSMISSION 0
#endif

#ifndef configFDCAN_ENABLE_TRANSCEIVER_DELAY_COMPENSATION
#    define configFDCAN_ENABLE_TRANSCEIVER_DELAY_COMPENSATION 1
#endif

#ifndef configFDCAN_ENABLE_TRANSMIT_PAUSE
#    define configFDCAN_ENABLE_TRANSMIT_PAUSE 0
#endif

#ifndef configFDCAN_ENABLE_EDGE_FILTERING
#    define configFDCAN_ENABLE_EDGE_FILTERING 0
#endif

#ifndef configFDCAN_ACCEPT_NON_MATCHING_STANDARD_FRAMES
#    define configFDCAN_ACCEPT_NON_MATCHING_STANDARD_FRAMES 0
#endif

#ifndef configFDCAN_ACCEPT_NON_MATCHING_EXTENDED_FRAMES
#    define configFDCAN_ACCEPT_NON_MATCHING_EXTENDED_FRAMES 0
#endif

#ifndef configFDCAN_REJECT_REMOTE_STANDARD_FRAMES
#    define configFDCAN_REJECT_REMOTE_STANDARD_FRAMES 1
#endif

#ifndef configFDCAN_REJECT_REMOTE_EXTENDED_FRAMES
#    define configFDCAN_REJECT_REMOTE_EXTENDED_FRAMES 1
#endif

#ifndef configFDCAN_CKDIV_PDIV_VALUE
#    define configFDCAN_CKDIV_PDIV_VALUE 0
#endif

// ========================================= HARDWARE PLATFORM ABSTRACTION =============================================

#if (defined FDCAN1) && (!defined FDCAN2) && (!defined FDCAN3)
#    define FDCAN_INTERFACE_COUNT 1
#elif (defined FDCAN1) && (defined FDCAN2) && (!defined FDCAN3)
#    define FDCAN_INTERFACE_COUNT 2
#elif (defined FDCAN1) && (defined FDCAN2) && (defined FDCAN3)
#    define FDCAN_INTERFACE_COUNT 3
#else
#    error FDCAN Interface count not understood
#endif

#ifdef FDCAN1
// Media IO Abstraction for STM32G4 FDCAN1 interface
extern const MediaIOInterface FDCAN1MediaIO;
#endif
#ifdef FDCAN2
// Media IO Abstraction for STM32G4 FDCAN2 interface
extern const MediaIOInterface FDCAN2MediaIO;
#endif
#ifdef FDCAN3
// Media IO Abstraction for STM32G4 FDCAN3 interface
extern const MediaIOInterface FDCAN3MediaIO;
#endif

// ===================================================== TYPES =========================================================

typedef enum FDCANOperationalMode {
    // The FDCAN default operating mode after hardware reset is event-driven CAN communication. TT Operation Mode is not
    // supported. Once the FDCAN is initialized and INIT bit in FDCAN_CCCR register is cleared, the FDCAN synchronizes
    // itself to the CAN bus and is ready for communication.
    eFDCAN_MODE_NORMAL = 0,

    // In Restricted operation mode the node is able to receive data and remote frames and to give acknowledge to valid
    // frames, but it does not send data frames, remote frames, active error frames, or overload frames. In case of an
    // error condition or overload condition, it does not send dominant bits, instead it waits for the occurrence of bus
    // idle condition to resynchronize itself to the CAN communication. The error counters (ECR.REC, ECR.TEC) are frozen
    // while error logging (ECR.CEL) is active.
    //
    // The Restricted operation mode can be used in applications that adapt themselves to different CAN bit rates. In
    // this case the application tests different bit rates and leaves the Restricted operation mode after it has
    // received a valid frame.
    eFDCAN_MODE_RESTRICTED,

    // In Bus monitoring mode (for more details refer to ISO11898-1, 10.12 Bus monitoring), the FDCAN is able to receive
    // valid data frames, but cannot start a transmission. In this mode, it sends only recessive bits on the CAN bus. If
    // the FDCAN is required to send a dominant bit (ACK bit, overload flag, active error flag), the bit is rerouted
    // internally so that the FDCAN can monitor it, even if the CAN bus remains in recessive state.
    //
    // The Bus monitoring mode can be used to analyze the traffic on a CAN bus without affecting it by the transmission
    // of dominant bits.
    eFDCAN_MODE_MONITORING,

    // Internal Loop Back mode can be used for a “Hot Selftest”, meaning the FDCAN can be tested without affecting a
    // running CAN system connected to the FDCAN_TX and FDCAN_RX pins. In this mode, FDCAN_RX pin is disconnected from
    // the FDCAN and FDCAN_TX pin is held recessive.
    eFDCAN_MODE_LOOPBACK_INTERNAL,

    // In External Loop Back mode, the FDCAN treats its own transmitted messages as received messages and stores them
    // (if they pass acceptance filtering) into Rx FIFOs. This mode is provided for hardware self-test. To be
    // independent from external stimulation, the FDCAN ignores acknowledge errors (recessive bit sampled in the
    // acknowledge slot of a data / remote frame) in External Loop Back mode. In this mode the FDCAN performs an
    // internal feedback from its transmit output to its receive input. The actual value of the FDCAN_RX input pin is
    // disregarded by the FDCAN. The transmitted messages can be monitored at the FDCAN_TX transmit pin.
    eFDCAN_MODE_LOOPBACK_EXTERNAL
} FDCANOperationalMode;

// =================================================== PUBLIC API ======================================================

/**
 * @brief FDCAN subsystem kernel clock
 *
 * The STM32G4 CAN subsystem uses a common configurable clock for all (up to) three FDCAN cores. It is configured by
 * the FDCANSEL[1:0] bits in the Peripherals independent clock configuration register (RCC_CCIPR), and may be
 * reconfigured at runtime.
 *
 * To be able to calculate timings and prescaler factors for a given bitrate, the fdcan driver module needs to be aware
 * of the configured fdcan core clock rate. Upon enabling and configuring this clock, the platform initialization code
 * is expected to update the fdcan_clk variable with the (new) clock rate.
 *
 * After modifying this clock rate, fdcan_configure() needs to be re-run to update bit timing configuration.
 */
extern uint32_t fdcan_clk;

/**
 * @brief Configure FDCAN Interface
 *
 * This function takes care of
 *   - starting the interface
 *   - applying module-level configurations to the interface
 *   - calculating and setting bit timing configurations
 *   - setting the interface into the specified operating mode
 *   - configuring the Tx FIFO/queue and Rx queues for transport worker integration
 *   - configuring the interface's acceptance filter layout
 *   - configuring the interface to use TIM3 for RX timestamping
 *
 * This function DOES NOT COVER
 *   - starting the interface clock
 *   - starting the GPIO clock for TX/RX signals
 *   - configuring the GPIOs (alternate function mapping, mode, speed)
 *   - configuring (enable, priority) FDCAN interrupts
 *   - configuring TIM3 for timestamping
 *
 * This function may be called repeatedly to reconfigure the interface on the fly, e.g. to perform tests or guess the
 * bitrate a bus is operating at (see "Restricted Mode").
 *
 * @param hw_index zero-based hardware enumeration index, e.g. 0 for FDCAN1
 * @param nominal_bitrate target bitrate in arbitration phase, set to zero for "leave-as-is"
 * @param data_bitrate target bitrate in data phase (only relevant in FD mode), set to zero for "leave-as-is"
 * @param mode interface operational mode, set to eFDCAN_MODE_NORMAL if unsure
 * @param use_fd enable interface for CAN FD instead of Classic CAN
 */
void fdcan_configure(const uint_fast8_t         hw_index,
                     const int32_t              nominal_bitrate,
                     const int32_t              data_bitrate,
                     const FDCANOperationalMode mode,
                     const bool                 use_fd);
