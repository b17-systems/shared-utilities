/**
 * @file node.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Experimental Node Interface
 * @version 0.1
 * @date 2022-01-08
 *
 * @copyright Copyright (c) 2022 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#pragma once

#include <stdint.h>

#include <FreeRTOS.h>
#include <task.h>

#include "canard.h"

// =========================================== MODULE LEVEL CONFIGURATION ==============================================

#ifndef configNODE_NUM_INTERFACES
#    define configNODE_NUM_INTERFACES 1
#endif

#ifndef configNODE_LOG_LEVEL
#    define configNODE_LOG_LEVEL LOG_LEVEL_TRACE
#endif

#ifndef configNODE_TX_QUEUE_CAPACITY
#    define configNODE_TX_QUEUE_CAPACITY 50
#endif

#ifdef configNODE_DEFAULT_BITRATE
#    define configNODE_DEFAULT_ARBITRATION_BITRATE configNODE_DEFAULT_BITRATE
#    define configNODE_DEFAULT_DATA_BITRATE        configNODE_DEFAULT_BITRATE
#endif

#ifndef configNODE_DEFAULT_ARBITRATION_BITRATE
#    define configNODE_DEFAULT_ARBITRATION_BITRATE 1000000UL
#endif

#ifndef configNODE_DEFAULT_DATA_BITRATE
#    define configNODE_DEFAULT_DATA_BITRATE 5000000UL
#endif

#ifndef configNODE_GET_INFO_TASK_PRIORITY
#    define configNODE_GET_INFO_TASK_PRIORITY 1
#endif

#ifndef configNODE_EXECUTE_COMMAND_TASK_PRIORITY
#    define configNODE_EXECUTE_COMMAND_TASK_PRIORITY 1
#endif

#ifndef configNODE_INCLUDE_AUTOMATIC_NODE_ID_ALLOCATION
#    define configNODE_INCLUDE_AUTOMATIC_NODE_ID_ALLOCATION 1
#endif

#define JOIN(A, B)       JOIN_AGAIN(A, B)
#define JOIN_AGAIN(A, B) A##B

// =============================================== TYPE DECLARATIONS ===================================================

static const char* const CANARD_TRANSFER_KIND_NAMES[CANARD_NUM_TRANSFER_KINDS] = {"message", "reponse", "request"};
static_assert(CanardTransferKindMessage == 0 && CanardTransferKindResponse == 1 && CanardTransferKindRequest == 2,
              "something's wrong with libcanard's transfer kind enumeration order");

/**
 * @brief Node Subscription Object
 *
 * Libcanard requires an instance of CanardRxSubscription to store state, and the transport worker task needs an
 * RxTransferHandler along with some metadata to route an incoming message or service response to the correct task.
 * This state is saved in the NodeSubscription struct.
 *
 * A NodeSubscription has to be created before a call to node_subscribe() or node_register_service(), and has to stay
 * valid until calling node_unsubscribe() or node_unregister_service(), respectively.
 */
typedef struct NodeSubscription NodeSubscription;

/**
 * @brief RX Transfer Handler Function Signature
 *
 * The RxTransferHandler type specifies the signature of the handler/hook function, which will be called by the
 * transport worker after having received a UAVCAN transfer.
 *
 * Under the hood, every completed RX transfer is handled by a RxTransferHandler, although for incoming messages and
 * service responses the handler function is provided by the node framework. Thus, for the external API, the
 * RxTransferHandler is mainly of relevance for registering services with the node module.
 *
 * The received transfer and the transfer's payload buffer (both allocated on the heap) have to be free'd after use.
 * If the handler hands the transfer off to a consuming task, that consuming task is responsible for freeing the memory.
 * In case the handler should have a transport ready to be free'd (either the current transfer, already processed, or a
 * previous transfer, superseded by the current one before it has been picked up by a consuming task), the handler may
 * return a (non-const*) pointer to the worker task, in which case the transport worker task will take over the memory
 * responsibility.
 *
 * ATTENTION: The handler function is called from within the transport worker task, it therefore shall neither
 *            block (excessively) nor require big amounts of stack space. If more in-depth computation is required,
 *            the handler function shall hand this off to a separate task (existing or newly created).
 *
 * @param transfer completed and assembled CanardRxTransfer to be handled by the function
 * @param sub node subscription the received transfer belongs to
 * @return CanardRxTransfer* pointer to transfer to be freed from memory, or NULL if no cleanup is required
 *
 * @see NodeSubscription, node_register_service
 */
typedef CanardRxTransfer* (*RxTransferHandler)(const CanardRxTransfer*, const NodeSubscription* const);

struct NodeSubscription {
    // libcanard subscription object, don't touch this
    CanardRxSubscription canard_sub;

    // Handle of subscribing task, internally used for message subscriptions and service responses.
    // This is only used in the place_transfer_into_task_mailbox() transfer handler, and may be repurposed
    // if a different transfer handler is used.
    TaskHandle_t task;

    // RX transfer handler to execute upon completing a received transfer. For message subscriptions and service
    // responses this is internally set to place_transfer_into_task_mailbox() and does not need to be set manually.
    // This is not enforced, however, and an implementation may substitute this _after_ the call to node_subscribe()
    // if deemed necessary.
    RxTransferHandler handler;

    union {
        // freely usable parameter, as an alternative to the .mailbox attribute
        const void* param;

        // index to notify .task at, used by place_transfer_into_task_mailbox() for message subscriptions
        uint32_t mailbox;
    };
};

/**
 * @brief Result of a uavcan.node.ExecuteCommand.
 *
 * The members of this enum mirror the values defined in uavcan.node.ExecuteCommand.1.1.Response.status.
 */
typedef enum eCommandResult {
    eCOMMAND_RESULT_SUCCESS        = 0,  // Started or executed successfully
    eCOMMAND_RESULT_FAILURE        = 1,  // Could not start or the desired outcome could not be reached
    eCOMMAND_RESULT_NOT_AUTHORIZED = 2,  // Denied due to lack of authorization
    eCOMMAND_RESULT_BAD_COMMAND    = 3,  // The requested command is not known or not supported
    eCOMMAND_RESULT_BAD_PARAMETER  = 4,  // The supplied parameter cannot be used with the selected command
    eCOMMAND_RESULT_BAD_STATE      = 5,  // The current state of the node does not permit execution of this command
    eCOMMAND_RESULT_INTERNAL_ERROR = 6,  // The operation should have succeeded but an unexpected failure occurred
} CommandResult_t;

/**
 * @brief uavcan.node.ExecuteCommand Handler Function Signature.
 *
 * This function will be called from the ExecuteCommand task, which is spawned for every request received. Delays in
 * this handler function are permitted and will not cause any adverse effects, but keep in mind that the request may
 * time out on the client's side if the response takes too long.
 *
 * @param number of bytes in the parameter array (max. 255)
 * @param argument bytes, implementation-specific
 * @return status of the executed command
 *
 * @see ExternalCommand_t
 */
typedef CommandResult_t (*ExecuteCommandHandler)(const size_t, const uint8_t[]);

/**
 * @brief External command declaration.
 *
 * A function matching the ExecuteCommandHandler signature can be registered as an external command, which then will be
 * executable by a uavcan.node.ExecuteCommand service call.
 *
 * This struct has to be placed in the .cyphal_xcmd_commands memory section for the ExecuteCommand handler to find.
 * The following block needs to be present in the linker file:
 *
 *    .cyphal_xcmd_commands : {
 *         . = ALIGN(4);
 *         __cyphal_xcmd_commands_start__ = .;
 *         KEEP(*(.cyphal_xcmd_commands))
 *         __cyphal_xcmd_commands_end__ = .;
 *     } > app
 *
 * Usually, this struct will not be created manually, but through the DECLARE_EXTERNAL_COMMAND macro.
 *
 * @see DECLARE_EXTERNAL_COMMAND
 */
typedef const struct sExternalCommand {
    // Identifier of vendor-specific command. Note that vendor-specific commands shall not use identifiers above 32767.
    const uint16_t command;
    // Function that will handle the command.
    const ExecuteCommandHandler handler;
} ExternalCommand_t;

/**
 * @brief Declare a function to be executable by uavcan.node.ExecuteCommand.1.1.
 *
 * @param uint16 identifier for vendor-specific command
 * @param ExecuteCommandHandler function to handle the command
 */
#define DECLARE_EXTERNAL_COMMAND(u16_command, handler_func)                                                            \
    const ExternalCommand_t __attribute__((section(".cyphal_xcmd_commands"), used))                                    \
    JOIN(JOIN(_xcmd_, u16_command), JOIN(_, handler_func)) = {.command = u16_command, .handler = handler_func}

// =============================================== GETTERS & SETTERS ===================================================

TaskHandle_t node_get_transport_task(void) __attribute__((pure));
CanardNodeID node_get_id(void) __attribute__((pure));
void         node_set_id(CanardNodeID id);
bool         node_is_anonymous(void) __attribute__((pure));
bool         node_initialized(void) __attribute__((pure));
size_t       node_get_qsize(const uint_fast8_t rti);
uint_fast8_t node_get_mode(void);
uint_fast8_t node_get_vssc(void);
uint32_t     node_get_uptime(void);
uint_fast8_t node_modify_vssc(const uint_fast8_t clearmask, const uint_fast8_t setmask);

extern uint_fast8_t get_system_health(void);
extern uint_fast8_t get_system_mode(void);

// =================================================== PUBLIC API ======================================================

/**
 * @brief Initialize Global Node Instance
 *
 * This function is the initialization entrypoint for the entire UAVCAN stack, comprising of the node module, the
 * attached canard instance (including its Tx queues), the transport worker task and corresponding media IO interface
 * configurations.
 *
 * Additionally, it starts and manages a few application level functions:
 *
 * - a FreeRTOS timer is started to send uavcan.node.Heartbeat messages
 * - services uavcan.node.GetInfo and uavcan.node.ExecuteCommand are registered
 * - if the registry services are enabled, services uavcan.register.List and uavcan.register.Access are registered
 * - network time synchronization via a uavcan.time.Synchronization client is started
 *
 * This function has to be executed before using any node module functions, but must not be executed more than once.
 */
void node_init(void);

/**
 * @brief Publish Serialized Transfer
 *
 * Send a (serialized) libcanard transfer over UAVCAN. This is the primary function to get a transport on the bus,
 * be it a message broadcast or a service request or response.
 *
 * This function internally acquires and releases the node lock, and might have to wait for it.
 *
 * @param valid_for_us duration (in µs) the transfer is considered valid; after that the transfer is cancelled
 * @param metadata transfer metadata object, required by libcanard
 * @param payload_size number of (used) bytes in the payload buffer
 * @param payload serialized payload buffer
 */
void node_publish(const uint32_t                      valid_for_us,
                  const CanardTransferMetadata* const metadata,
                  const size_t                        payload_size,
                  const uint8_t* const                payload);

/**
 * @brief Try to Publish Serialized Transfer
 *
 * Send a (serialized) libcanard transfer over UAVCAN.
 * Abort if the node lock does not become available within block_time.
 *
 * @param valid_for_us duration (in µs) the transfer is considered valid; after that the transfer is cancelled
 * @param metadata transfer metadata object, required by libcanard
 * @param payload_size number of (used) bytes in the payload buffer
 * @param payload serialized payload buffer
 * @param block_time FreeRTOS ticks to wait for the node lock to become available
 *
 * @see node_publish
 */
void node_try_publish(const uint32_t                      valid_for_us,
                      const CanardTransferMetadata* const metadata,
                      const size_t                        payload_size,
                      const uint8_t* const                payload,
                      const TickType_t                    block_time);

/**
 * @brief Subscribe to UAVCAN Message
 *
 * Register a message subscription with the underlying libcanard instance. The NodeSubscription has to be created before
 * calling this function, and it has to stay valid (in scope on stack, or allocated on the heap) until it is cancelled
 * with node_unsubscribe().
 *
 * The subscription's .mailbox has to be configured BEFORE calling this function. The application may use the same
 * mailbox index for multiple subscriptions, however necessary precautions against input buffer (mailbox) overruns
 * need to be considered.
 *
 * This function automatically configures the interface's hardware acceptance filters to accept messages of this
 * subject.
 *
 * Transfers can be polled from the mailbox by node_poll().
 *
 * @param port_id Subject ID to subscribe to
 * @param extent message extent as specified in the message data type, see UAVCAN specification section 3.4.5.5
 * @param out_sub reference to existing subscription object with configured mailbox index
 *
 * @see node_poll, node_unsubscribe
 */
void node_subscribe(const CanardPortID port_id, const size_t extent, NodeSubscription* const out_sub);

/**
 * @brief Unsubscribe from Previously Subscribed to UAVCAN Message
 *
 * This function automatically removes this subject from the interface's hardware acceptance filters.
 *
 * @param port_id Subject ID to unsubscribe from
 *
 * @see node_subscribe
 */
void node_unsubscribe(const CanardPortID port_id);

/**
 * @brief Poll Task Mailbox for New Transfers (Messages)
 *
 * This function polls a single FreeRTOS task notification index for new messages. The mailbox to poll is encoded in the
 * NodeSubscription object.
 *
 * ATTENTION: The application is responsible of freeing both the returned transfer and its payload buffer.
 *
 * @param sub subscription object originally handed to node_subscribe
 * @param block_time FreeRTOS ticks to wait for new messages before returning NULL
 * @return CanardRxTransfer* received transfer (still serialized), or NULL if the operation timed out; FREE AFTER USE
 *
 * @see node_subscribe
 */
CanardRxTransfer* node_poll(const NodeSubscription* const sub, const TickType_t block_time);

/**
 * @brief Register UAVCAN Service Handler
 *
 * This function registers a UAVCAN service with libcanard/the transport worker. Before calling this function the node
 * subscription object has to be created, and the service handler function has to be written to *out_sub.handler.
 *
 * Upon receiving a service request, the transport worker will execute out_sub->handler(), which shall handle the
 * request and send a response. See the RxTransferHandler type documentation for more information.
 *
 * @param port_id Service ID to provide a service at
 * @param extent service request extent as specified in the data type, see UAVCAN specification section 3.4.5.5
 * @param out_sub reference to existing subscription object with configured handler function
 *
 * @see RxTransferHandler, node_unregister_service
 */
void node_register_service(const CanardPortID port_id, const size_t extent, NodeSubscription* const out_sub);

/**
 * @brief Unregister a Previously Registered UAVCAN Service
 *
 * @param port_id Serice ID of service to unregister
 *
 * @see RxTransferHandler, node_register_service
 */
void node_unregister_service(const CanardPortID port_id);

/**
 * @brief Call a UAVCAN Service from a Remote Server
 *
 * TODO: write about limitations
 *
 * ATTENTION: The application is responsible of freeing both the returned transfer and its payload buffer.
 *
 * @param metadata metadata object containing service ID, remote node ID and transfer ID
 * @param request_size number of bytes in request_buffer forming the request transfer
 * @param response_extent extent of (expected) response as specified in the service data type,
 *                        see UAVCAN specification section 3.4.5.5
 * @param request_buffer buffer holding the (serialized) service request
 * @param free_after_tx (optional) pointer to memory to free after sending the request, can be used to free the
 *                      request buffer before receiving a response
 * @param mailbox FreeRTOS task notification index to use for polling the response
 * @param block_time_ms duration (in milliseconds) to wait for a response before returning NULL
 * @return CanardRxTransfer* (serialized) service response, or NULL if the request timed out; FREE AFTER USE
 */
CanardRxTransfer* node_call(const CanardTransferMetadata* const metadata,
                            const size_t                        request_size,
                            const size_t                        response_extent,
                            const uint8_t* const                request_buffer,
                            void*                               free_after_tx,
                            const uint32_t                      mailbox,
                            const TickType_t                    block_time_ms);

CanardRxTransfer* dispatch_request_handler_task(const TaskFunction_t         task,
                                                const char* const            short_name,
                                                const char* const            full_name,
                                                const configSTACK_DEPTH_TYPE stack_size,
                                                const UBaseType_t            priority,
                                                const CanardRxTransfer*      transfer);
