/**
 * @file transport.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief UAVCAN Transport Layer Abstraction and Worker Task
 * @version 0.1
 * @date 2022-01-09
 *
 * @copyright Copyright (c) 2022 starcopter GmbH
 *
 */

#include "transport.h"
#include "fdcan.h"  // temporary, for static configuration to FDCAN1MediaIO

#include "../sclib/registry.h"
#include "../sclib/timekeeper.h"
#include "../sclib/assert.h"

#include <uavcan/node/Health_1_0.h>  // for enumeration values
#include <uavcan/node/Mode_1_0.h>    // for enumeration values

// don't log to UAVCAN from inside this module
#define configLOG_MASK_UAVCAN
#define LOG_LEVEL configTRANSPORT_LOG_LEVEL
#include <sclib/log.h>

#define TRANSPORT_ANY_FRAME_RECEIVED                                                                                   \
    (TRANSPORT_FRAME_RECEIVED(0) | TRANSPORT_FRAME_RECEIVED(1) | TRANSPORT_FRAME_RECEIVED(2))
#define TRANSPORT_ANY_FRAME_TRANSMITTED                                                                                \
    (TRANSPORT_FRAME_TRANSMITTED(0) | TRANSPORT_FRAME_TRANSMITTED(1) | TRANSPORT_FRAME_TRANSMITTED(2))
#define TRANSPORT_ANY_BUS_STATE_CHANGED                                                                                \
    (TRANSPORT_BUS_STATE_CHANGED(0) | TRANSPORT_BUS_STATE_CHANGED(1) | TRANSPORT_BUS_STATE_CHANGED(2))

extern int_fast8_t node_rx_accept(const CanardMicrosecond  timestamp_usec,
                                  const CanardFrame* const frame,
                                  const uint_fast8_t       rti,
                                  CanardRxTransfer* const  out_transfer,
                                  NodeSubscription** const out_subscription);

extern const CanardTxQueueItem* node_tx_peek(const uint_fast8_t rti);

extern void node_tx_free(const uint_fast8_t rti, const CanardTxQueueItem* const item);

// ============================================= TASK-LOCAL STATE & API ================================================

static uint_fast8_t health;                                      // saturated uint2 uavcan.node.Health.1.0.value
static uint_fast8_t mode = uavcan_node_Mode_1_0_INITIALIZATION;  // saturated uint3 uavcan.node.Mode.1.0.value
inline uint_fast8_t transport_get_health(void) { return health; }
inline uint_fast8_t transport_get_mode(void) { return mode; }

// ============================================= TRANSPORT WORKER TASK =================================================

static void TransportWorker(void* param __unused) {
    const TickType_t block_time = pdMS_TO_TICKS(configTRANSPORT_BLOCK_TIME_MS);
    const size_t     N          = configNODE_NUM_INTERFACES;

    for (;;) {
        // ============================================= INITIALIZATION ================================================

        // To read a received frame from one of the Rx FIFOs, we will need a CanardFrame to write to. This single frame
        // will be reused constantly for each new received frame and thus does not need to be freed after use.
        CanardFrame* rx_frame = pvPortMalloc(sizeof(CanardFrame) + CANARD_MTU_CAN_FD);
        ASSERT(rx_frame);
        rx_frame->payload     = (void*) ((uint32_t) rx_frame + sizeof(CanardFrame));
        uint64_t rx_timestamp = 0;

        /// To handle a received frame, node_rx_accept() needs a pointer to an output CanardRxTransfer object, which
        /// will get populated in case the received frame completed a transfer. Upfront it will be unknown if a call to
        /// node_rx_accept() will complete a transfer, therefore a suitable output object has to be specified for each
        /// call. To avoid allocating and freeing memory for a CanardRxTransfer each time we handle a received frame, we
        /// will keep an output object at all times. In case a transfer is completed, the object is handed off to the
        /// consuming task and rx_transfer will be appointed new memory.
        CanardRxTransfer* rx_transfer = pvPortMalloc(sizeof(CanardRxTransfer));
        ASSERT(rx_transfer);

        // TODO: make this configuration dynamic
        const MediaIOInterface* ifaces[N];
        uint32_t                cumulated_bus_errors[N];
        BusErrorStats           iface_stats[N];

        for (uint_fast8_t rti = 0u; rti < N; ++rti) {
            ifaces[rti] = &FDCAN1MediaIO;
            ifaces[rti]->install_hooks(xTaskGetCurrentTaskHandle(), rti);
            cumulated_bus_errors[rti] = ifaces[rti]->get_stats(&iface_stats[rti]);
        }

        // A non-zero error value will cause the loop below to stop and the task to start over and re-initialize.
        int error = 0;

        // periodically check bus state anyway
        TickType_t next_proactive_state_check = xTaskGetTickCount();

        mode = uavcan_node_Mode_1_0_OPERATIONAL;

        // ================================================== LOOP =====================================================
        do {
            uint32_t notification;
            xTaskNotifyWait(0, UINT32_MAX, &notification, block_time);

            if (next_proactive_state_check <= xTaskGetTickCount()) {
                notification |= TRANSPORT_ANY_BUS_STATE_CHANGED;  // set all bits
                next_proactive_state_check = xTaskGetTickCount() + pdMS_TO_TICKS(100);
            }

            if (notification & TRANSPORT_ANY_BUS_STATE_CHANGED) {
                for (uint_fast8_t rti = 0u; rti < N; ++rti) {
                    if (!(notification & TRANSPORT_BUS_STATE_CHANGED(rti))) continue;

                    const uint_fast8_t new_bus_error_count = ifaces[rti]->get_stats(&iface_stats[rti]);
                    cumulated_bus_errors[rti] += new_bus_error_count;
                }

                enum CANErrorState worst = eBusErrorActive;
                for (uint_fast8_t rti = 0u; rti < N; ++rti)
                    if (iface_stats[rti].state > worst) worst = iface_stats[rti].state;

                switch (worst) {
                    case eBusErrorActive:
                    case eBusErrorWarning:
                        // all interfaces are Error Active
                        health = uavcan_node_Health_1_0_NOMINAL;
                        break;
                    case eBusErrorPassive:
                        // at least one interface is Error Passive, but none is off
                        health = uavcan_node_Health_1_0_ADVISORY;
                        break;
                    case eBusErrorBusOff:
                        // at least one interface has shut off entirely
                        health = uavcan_node_Health_1_0_CAUTION;
                        break;
                    default:
                        // We should never reach here. If we do, something is terribly wrong.
                        health = uavcan_node_Health_1_0_WARNING;
                        HALT_IF_DEBUGGING();
                }
            }

            if (notification & TRANSPORT_ANY_FRAME_RECEIVED) {
                for (uint_fast8_t rti = 0u; rti < N; ++rti) {
                    if (!(notification & TRANSPORT_FRAME_RECEIVED(rti))) continue;

                    while (ifaces[rti]->pop_frame(rx_frame, &rx_timestamp)) {
                        LOG_TRACE("frame received from iface %u", rti);

                        NodeSubscription* sub    = NULL;
                        const int_fast8_t result = node_rx_accept(rx_timestamp, rx_frame, rti, rx_transfer, &sub);
                        LOG_TRACE("RX Frame with identifier %lx processed", rx_frame->extended_can_id);

                        // don't free the RX frame memory, it will get used for the next reception

                        if (result == 0u) {
                            // node_rx_accept() ran successfully, but no transfer has been completed yet.
                            // Nothing has been written to *rx_transfer, so no memory needs freeing just yet.
                        } else if (result == 1u) {
                            // the frame completed a subscribed to transfer, available for pickup at *rx_transfer
                            LOG_DEBUG("%s %u.%02u from %u received",
                                      CANARD_TRANSFER_KIND_NAMES[rx_transfer->metadata.transfer_kind],
                                      rx_transfer->metadata.port_id, rx_transfer->metadata.transfer_id,
                                      rx_transfer->metadata.remote_node_id);

                            ASSERT(sub);
                            ASSERT(sub->handler);

                            CanardRxTransfer* returned_rx_transfer = sub->handler(rx_transfer, sub);
                            if (returned_rx_transfer) {
                                // The handler function returned a transfer that has not yet been cleaned up. This may
                                // either be the exact transfer it has been handed, or a previous transfer which has not
                                // been picked up by a consuming task. In any case, the responsibility to manage the
                                // returned transfer's memory lies with us now.
                                // The transfer's payload shall be free'd, the transfer object itself can be reused.
                                vPortFree(returned_rx_transfer->payload);
                                rx_transfer = returned_rx_transfer;
                            } else {
                                // The handler function did not return any transfer that would need cleaning up, so we
                                // have to create a new buffer for the next call to node_rx_accept().
                                rx_transfer = pvPortMalloc(sizeof(CanardRxTransfer));
                                ASSERT(rx_transfer);
                            }
                        } else {
                            // No changes have been made to *rx_transfer, so no memory needs to be allocated or freed.
                            LOG_ERROR("node_rx_accept() returned with error %i", (int8_t) result);
                        }
                    }
                }
            }

            // const bool any_frame_transmitted = notification & TRANSPORT_ANY_FRAME_TRANSMITTED;

            const CanardTxQueueItem* txqi;
            for (uint_fast8_t rti = 0u; rti < N; ++rti) {
                if (iface_stats[rti].state == eBusErrorBusOff) {
                    // drain the queue, the bus is inaccessible
                    while ((txqi = node_tx_peek(rti)) != NULL)
                        node_tx_free(rti, txqi);
                } else {
                    while ((txqi = node_tx_peek(rti)) != NULL) {
                        if (ifaces[rti]->push_frame(get_monotonic_timestamp_us(), txqi)) {
                            // push successful (or frame timed out), frame memory can be freed
                            LOG_TRACE("TX Frame at %p with ID %lx handled", txqi, txqi->frame.extended_can_id);
                            node_tx_free(rti, txqi);
                        } else {
                            // frame rejected, we're done here
                            LOG_TRACE("frame rejected by interface");
                            break;
                        }
                    }
                    if (iface_stats[rti].state == eBusErrorPassive) {
                        // In addition to trying to get frames on the bus, drain the queue to a degree.
                        while (3 * node_get_qsize(rti) > 2 * configNODE_TX_QUEUE_CAPACITY) {
                            txqi = node_tx_peek(rti);
                            LOG_DEBUG("Discard frame %lx", txqi->frame.extended_can_id);
                            node_tx_free(rti, txqi);
                        }
                    }
                }
            }

            // Throttle transport worker loop frequency on low memory. Two ticks ensure there will always be at least
            // one tick of delay, even if the next tick occurs immediately after the delay instruction.
            //
            // From https://www.freertos.org/RTOS-idle-task.html:
            //
            //     The idle task is responsible for freeing memory allocated by the RTOS to tasks that have since been
            //     deleted. It is therefore important in applications that make use of the vTaskDelete() function to
            //     ensure the idle task is not starved of processing time.
            if (xPortGetFreeHeapSize() < 0x2000) vTaskDelay(2);
        } while (!error);

        mode = uavcan_node_Mode_1_0_INITIALIZATION;

        LOG_CRITICAL("Transport Worker loop failed with error 0x%x", error);
        HALT_IF_DEBUGGING();

        // =========================================== DE-INITIALIZATION ===============================================

        for (uint_fast8_t rti = 0u; rti < N; ++rti) {
            // uninstall hooks
            ifaces[rti]->install_hooks(NULL, rti);
        }

        vPortFree(rx_transfer);
        vPortFree(rx_frame);

        // try again
    }

    WTF();
}

/**
 * @brief Start the Transport Worker Task
 *
 * This function is called by the node module during node_init(), upon which it will start the transport worker task.
 * It is not exported publicly, the node module references it as an external function.
 *
 * @return TaskHandle_t handle of the created task, asserted not to be NULL
 */
TaskHandle_t start_transport_task(void) {
    TaskHandle_t task = NULL;

    const BaseType_t rv = xTaskCreate(TransportWorker, "Transport", configTRANSPORT_STACK_SIZE, NULL,
                                      configTRANSPORT_TASK_PRIORITY, &task);
    ASSERT(rv == pdPASS);
    ASSERT(task);

    return task;
}
