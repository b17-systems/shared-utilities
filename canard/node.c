/**
 * @file node.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Experimental Node Interface
 * @version 0.1
 * @date 2022-01-08
 *
 * @copyright Copyright (c) 2022 starcopter GmbH
 *
 */

#include "node.h"
#include <main.h>
#include "transport.h"
#include "fdcan.h"

#include <FreeRTOS.h>
#include <semphr.h>
#include <timers.h>

#include "../sclib/registry.h"
#include "../sclib/timekeeper.h"
#include "../sclib/shared_ram.h"

#include <uavcan/node/Health_1_0.h>
#include <uavcan/node/Mode_1_0.h>
#include <uavcan/node/Heartbeat_1_0.h>
#include <uavcan/node/GetInfo_1_0.h>
#include <uavcan/node/ExecuteCommand_1_1.h>
#include <uavcan/time/Synchronization_1_0.h>
#include <uavcan/_register/List_1_0.h>
#include <uavcan/_register/Access_1_0.h>

// ============================================== BUILD CONFIGURATION ==================================================

#if configSUPPORT_DYNAMIC_ALLOCATION != 1
#    error "This module relies on dynamic memory allocation, configSUPPORT_DYNAMIC_ALLOCATION needs to be enabled"
#endif

#if configUSE_TASK_NOTIFICATIONS != 1 || configTASK_NOTIFICATION_ARRAY_ENTRIES < 1
#    error "This module's subscription implementation requires FreeRTOS Task Notifications to be enabled"
#endif

// don't log to UAVCAN from inside this module
#define configLOG_MASK_UAVCAN
#define LOG_LEVEL configNODE_LOG_LEVEL
#include <sclib/log.h>

// ================================================== LOCAL STATE ======================================================

static const uint32_t SUBJECT_ARRAY_GROW_STEP = 8;

typedef struct SubjectArray {
    size_t       capacity;
    size_t       occupied;
    CanardPortID subjects[];
} SubjectArray;

// for now, node will stay a singleton, local to this module
struct Node {
    SemaphoreHandle_t lock;  // FreeRTOS Mutex guarding the Canard Instance
    CanardInstance    cins;
    CanardTxQueue     queue[configNODE_NUM_INTERFACES];
    TimerHandle_t     heartbeat_timer;
    uint_fast8_t      mode;    // saturated uint3 uavcan.node.Mode.1.0.value
    uint_fast8_t      vssc;    // saturated uint8 vendor_specific_status_code
    uint32_t          uptime;  // uptime in seconds, saturated, overflow-safe
    TaskHandle_t      transport_task;
    SubjectArray*     subjects;  // to be allocated on heap
} node;

static void __init_node_struct(void) __attribute__((constructor));
static void __init_node_struct(void) { node.mode = uavcan_node_Mode_1_0_INITIALIZATION; }

// =============================================== GETTERS & SETTERS ===================================================

inline TaskHandle_t node_get_transport_task(void) { return node.transport_task; }
inline CanardNodeID node_get_id(void) { return node.cins.node_id; }
inline bool         node_is_anonymous(void) { return node_get_id() > CANARD_NODE_ID_MAX; }
inline size_t       node_get_qsize(const uint_fast8_t rti) { return node.queue[rti].size; }
inline bool         node_initialized(void) { return node.mode != uavcan_node_Mode_1_0_INITIALIZATION; }
inline uint_fast8_t node_get_mode(void) { return node.mode; }
inline uint_fast8_t node_get_vssc(void) { return node.vssc; }
inline uint32_t     node_get_uptime(void) { return node.uptime; }
inline uint_fast8_t node_modify_vssc(const uint_fast8_t clearmask, uint_fast8_t setmask) {
    node.vssc = ((node.vssc & ~clearmask) | setmask) & 0xffu;
    return node.vssc;
}

__WEAK uint_fast8_t get_system_health(void) {
    // node has no independent health, return transport worker task health directly
    return transport_get_health();
}
__WEAK uint_fast8_t get_system_mode(void) {
    const uint_fast8_t tmode = transport_get_mode();
    // return worst (=largest) mode state
    if (tmode > node.mode) return tmode;
    return node.mode;
}

// ============================================= RUNTIME CONFIGURATION =================================================

static const uint16_t NODE_ID_MIN     = 0;
static const uint16_t NODE_ID_MAX     = CANARD_NODE_ID_UNSET;
static const uint16_t NODE_ID_DEFAULT = NODE_ID_MAX;
static uint16_t       uavcan_node_id  = NODE_ID_DEFAULT;

static void update_hardware_acceptance_filters(void);

/**
 * @brief Update Node ID
 *
 * Sample the node ID from the uavcan.node.id register and configure the canard instance to use it.
 *
 * This is a TimerCallbackFunction_t, but may also be executed directly with a NULL argument.
 *
 * @param th FreeRTOS timer handle, optional: if not NULL, the corresponding timer will be deleted
 */
static void do_update_node_id(TimerHandle_t th) {
    if (!node_initialized()) return;
    xSemaphoreTake(node.lock, portMAX_DELAY);
    // update node ID from static node_id variable
    node.cins.node_id = uavcan_node_id > UINT8_MAX ? UINT8_MAX : uavcan_node_id;
    xSemaphoreGive(node.lock);
    LOG_INFO("Node ID set to %u", node.cins.node_id);
    update_hardware_acceptance_filters();

    if (th) xTimerDelete(th, portMAX_DELAY);
}

/**
 * @brief Update Node ID after a Delay of 10 Milliseconds
 *
 * Start a FreeRTOS timer to update the canard instance's node id from the uavcan.node.id register value.
 * This allows the register interface to send a response to the register.Access request before changing the node's ID.
 */
static void node_id_register_callback(void) {
    if (!node_initialized()) return;
    // start on a timer to allow the access server to send a (valid) response to the client
    TimerHandle_t t = xTimerCreate("NodeID Update", pdMS_TO_TICKS(10), pdFALSE, NULL, do_update_node_id);
    ASSERT(t);
    xTimerStart(t, portMAX_DELAY);
}
// uavcan.node.id
UAVCANRegisterDeclaration _reg_node_id = {
    .name        = "uavcan.node.id",
    .type        = eUAVCANRegisterTypeNatural16Array,
    .mutable     = 1,
    .persistent  = 1,
    .len         = 1,
    .min         = (const void*) &NODE_ID_MIN,
    .max         = (const void*) &NODE_ID_MAX,
    .dflt        = (const void*) &NODE_ID_DEFAULT,
    .value       = (void*) &uavcan_node_id,
    .update_hook = node_id_register_callback,
    .lock        = NULL,
};

void node_set_id(CanardNodeID id) {
    uavcan_node_id = id;
    do_update_node_id(NULL);
}

const uint32_t  default_bitrate[2] = {configNODE_DEFAULT_ARBITRATION_BITRATE, configNODE_DEFAULT_DATA_BITRATE};
const uint32_t  max_bitrate[2]     = {1000000, 5000000};
const uint32_t  min_bitrate[2]     = {100000, 100000};
static uint32_t bitrate[2]         = {configNODE_DEFAULT_ARBITRATION_BITRATE, configNODE_DEFAULT_DATA_BITRATE};
// uavcan.can.bitrate
UAVCANRegisterDeclaration _reg_can_bitrate = {
    .name        = "uavcan.can.bitrate",
    .type        = eUAVCANRegisterTypeNatural32Array,
    .mutable     = 0,  // TODO: make configurable
    .persistent  = 1,
    .constant    = 0,
    .len         = 2,
    .min         = (void*) min_bitrate,
    .max         = (void*) max_bitrate,
    .dflt        = (void*) default_bitrate,
    .value       = (void*) bitrate,
    .update_hook = NULL,  // TODO
    .lock        = &node.lock,
};

// =============================================== PRIVATE FUNCTIONS ===================================================

static void       send_heartbeat(TimerHandle_t th);
CanardRxTransfer* handle_GetInfo_request(const CanardRxTransfer* transfer, const NodeSubscription* const sub);
static void       send_GetInfo_response(void* param);
CanardRxTransfer* handle_ExecuteCommand_request(const CanardRxTransfer* transfer, const NodeSubscription* const sub);
static void       ExecuteCommand(void* param);
static void       PNPClient(void* param);

/**
 * @brief CanardMemoryAllocate Wrapper around the FreeRTOS-provided pvPortMalloc
 *
 * Libcanard requires a dynamic memory allocation function with this interface, specified by the CanardMemoryAllocate
 * function type. This is a convenience wrapper around the pvPortMalloc function provided by FreeRTOS' memory management
 * implementation.
 *
 * Libcanard requires a constant time complexity memory manager for its verifyable runtime assumptions to be valid.
 * NONE OF THE MEMORY ALLOCATION IMPLEMENTATION INCLUDED IN FreeRTOS FULFIL THIS REQUIREMENT. Nevertheless, the
 * implementation schemes heap_4.c and heap_5.c satisfy all other requirements and have been successfully used.
 *
 * @param ins Pointer to calling CanardInstance; unused in function implementation
 * @param amount number of bytes to be allocated
 *
 * @return void* pointer to block of "amount" bytes allocated, aligned to portBYTE_ALIGNMENT; or NULL in case of failure
 *
 * @see CanardMemoryAllocate, pvPortMalloc
 */
void* canard_malloc(CanardInstance* ins __unused, size_t amount) { return pvPortMalloc(amount); }

/**
 * @brief CanardMemoryFree Wrapper around the FreeRTOS-provided vPortFree
 *
 * Wrapper around the vPortFree function provided by FreeRTOS' memory management implementation to fit the interface
 * specification of the CanardMemoryFree function type.
 *
 * @param ins Pointer to calling CanardInstance; unused in function implementation
 * @param pointer Pointer to memory block to be freed
 *
 * @see CanardMemoryFree, vPortFree
 */
void canard_free(CanardInstance* ins __unused, void* pointer) { vPortFree(pointer); }

/**
 * @brief Free CanardRxTransfer Allocated on the Heap
 *
 * This function is safe to be called with a NULL argument, in which case it will do nothing.
 *
 * @param rxt transfer to clean up
 */
static void clean_up_rx_transfer(CanardRxTransfer* rxt) {
    if (!rxt) return;
    vPortFree(rxt->payload);
    vPortFree(rxt);
}

static void print_subject_id_array(void) {
#if LOG_LEVEL <= LOG_LEVEL_TRACE
    char* string_output = pvPortMalloc(7 * node.subjects->capacity + 1);
    if (node.subjects->capacity && node.subjects->occupied) {
        char* write_location = &string_output[0];
        for (size_t i = 0; i < node.subjects->occupied; ++i) {
            int bytes_written = sprintf(write_location, "%u, ", node.subjects->subjects[i]);
            write_location += bytes_written;
        }
        char* const last_comma = write_location - 2;
        ASSERT(last_comma > string_output);
        *last_comma = '\0';
    } else {
        string_output[0] = '\0';
    }
    LOG_TRACE("Subject ID array: [%s]", string_output);
    vPortFree(string_output);
#endif
}

static void insert_subject_id_into_array(const CanardPortID subject) {
    xSemaphoreTake(node.lock, portMAX_DELAY);
    uint_fast8_t index;
    for (index = 0; index < node.subjects->occupied; ++index) {
        if (node.subjects->subjects[index] == subject) {
            LOG_NOTICE("Subject %u already in array", subject);
            xSemaphoreGive(node.lock);
            return;
        }
    }
    if (node.subjects->occupied + 1 > node.subjects->capacity) {
        SubjectArray* old_sub_arr  = node.subjects;
        const size_t  new_capacity = old_sub_arr->capacity + SUBJECT_ARRAY_GROW_STEP;
        LOG_DEBUG("grow Subject ID array from %u to %u locations", old_sub_arr->capacity, new_capacity);
        SubjectArray* new_sub_arr = pvPortMalloc(sizeof(SubjectArray) + new_capacity * sizeof(CanardPortID));
        ASSERT(new_sub_arr);
        new_sub_arr->capacity = new_capacity;
        for (index = 0; index < old_sub_arr->occupied; ++index) {
            new_sub_arr[index] = old_sub_arr[index];
        }
        new_sub_arr->occupied = old_sub_arr->occupied;
        node.subjects         = new_sub_arr;
        vPortFree(old_sub_arr);
    }
    index                          = node.subjects->occupied++;
    node.subjects->subjects[index] = subject;
    print_subject_id_array();
    xSemaphoreGive(node.lock);
    LOG_DEBUG("Subject %u added to array at index %u", subject, index);
}

static void remove_subject_id_from_array(const CanardPortID subject) {
    xSemaphoreTake(node.lock, portMAX_DELAY);
    uint_fast8_t index;
    for (index = 0; index < node.subjects->occupied; ++index) {
        if (node.subjects->subjects[index] == subject) break;
    }
    if (index == node.subjects->occupied) {
        LOG_WARNING("Subject %u not found in array", subject);
        xSemaphoreGive(node.lock);
        return;
    }
    --node.subjects->occupied;
    LOG_DEBUG("Subject %u removed from array at index %u", subject, index);
    for (; index < node.subjects->occupied; ++index) {
        node.subjects->subjects[index] = node.subjects->subjects[index + 1];
    }

    if (node.subjects->occupied + SUBJECT_ARRAY_GROW_STEP < node.subjects->capacity) {
        SubjectArray* old_sub_arr  = node.subjects;
        const size_t  new_capacity = old_sub_arr->capacity - SUBJECT_ARRAY_GROW_STEP;
        ASSERT(node.subjects->occupied <= new_capacity);
        LOG_DEBUG("shrink Subject ID array from %u to %u locations", old_sub_arr->capacity, new_capacity);
        SubjectArray* new_sub_arr = pvPortMalloc(sizeof(SubjectArray) + new_capacity * sizeof(CanardPortID));
        ASSERT(new_sub_arr);
        new_sub_arr->capacity = new_capacity;
        for (index = 0; index < old_sub_arr->occupied; ++index) {
            new_sub_arr[index] = old_sub_arr[index];
        }
        new_sub_arr->occupied = old_sub_arr->occupied;
        node.subjects         = new_sub_arr;
        vPortFree(old_sub_arr);
    }

    print_subject_id_array();
    xSemaphoreGive(node.lock);
}

// https://graphics.stanford.edu/~seander/bithacks.html#CountBitsSetKernighan
static uint_fast8_t count_bits_set(uint32_t value) {
    uint_fast8_t count;  // count accumulates the total bits set in value
    for (count = 0; value; count++)
        value &= value - 1;  // clear the least significant bit set

    return count;
}

static void update_hardware_acceptance_filters(void) {
    const MediaIOInterface* const io = &FDCAN1MediaIO;

    xSemaphoreTake(node.lock, portMAX_DELAY);

    io->set_filter(canardMakeFilterForServices(node.cins.node_id), 0);

    uint_fast8_t num_filters = node.subjects->occupied;
    if (num_filters) {
        CanardFilter* filters = pvPortMalloc(sizeof(CanardFilter) * num_filters);
        ASSERT(filters);

        for (uint_fast8_t i = 0; i < num_filters; ++i) {
            filters[i] = canardMakeFilterForSubject(node.subjects->subjects[i]);
        }

        // The filter consolidation algorithm is described in the UAVCAN specification v1.0-beta,
        // section 4.2.4.4 Automatic hardware acceptance filter configuration
        while (num_filters > io->num_filters - 1) {
            uint_fast8_t a = 0, b = 0, best_rank = 0;
            for (uint_fast8_t j = 1; j < num_filters; ++j) {
                for (uint_fast8_t i = 0; i < j; ++i) {
                    const uint_fast8_t rank =
                        count_bits_set(canardConsolidateFilters(&filters[i], &filters[j]).extended_mask);
                    if (rank > best_rank) {
                        a = i;
                        b = j;
                    }
                }
            }

            filters[a] = canardConsolidateFilters(&filters[a], &filters[b]);
            for (uint_fast8_t i = b; i < num_filters - 1; ++i)
                filters[i] = filters[i + 1];

            --num_filters;
        }

        ASSERT(num_filters + 1 <= io->num_filters);

        for (uint_fast8_t i = 0; i < num_filters; ++i)
            io->set_filter(filters[i], i + 1);

        vPortFree(filters);
    }

    for (uint_fast8_t i = num_filters + 1; i < io->num_filters; ++i)
        io->set_filter((CanardFilter){0, 0}, i);

    xSemaphoreGive(node.lock);
}

void start_pnp_client_task(void) {
#if configNODE_INCLUDE_AUTOMATIC_NODE_ID_ALLOCATION
    static TaskHandle_t handle = NULL;
    ASSERT(handle == NULL);

    BaseType_t status = xTaskCreate(PNPClient, "PNP Client", 384, NULL, TaskPrioritySlow, &handle);
    ASSERT(status == pdPASS);
#endif
}

__weak_symbol uint8_t node_execute_external_command(const uavcan_node_ExecuteCommand_Request_1_1* const request) {
    // declared in linker file
    extern ExternalCommand_t __cyphal_xcmd_commands_start__;
    extern ExternalCommand_t __cyphal_xcmd_commands_end__;

    for (ExternalCommand_t* cmd = &__cyphal_xcmd_commands_start__; cmd < &__cyphal_xcmd_commands_end__; ++cmd) {
        if (request->command == cmd->command) {
            return cmd->handler(request->parameter.count, request->parameter.elements);
        }
    }

    return uavcan_node_ExecuteCommand_Response_1_1_STATUS_BAD_COMMAND;
}

// ========================================= PUBLIC API & RELATED FUNCTIONS ============================================

void node_init(void) {
    ASSERT(!node_initialized());
    node.lock           = xSemaphoreCreateMutex();
    node.cins           = canardInit(canard_malloc, canard_free);
    const bool   use_fd = bitrate[0] != bitrate[1];
    const size_t mtu    = use_fd ? CANARD_MTU_CAN_FD : CANARD_MTU_CAN_CLASSIC;
    for (uint_fast8_t i = 0; i < configNODE_NUM_INTERFACES; i++) {
        node.queue[i] = canardTxInit(configNODE_TX_QUEUE_CAPACITY, mtu);
    }

    node.subjects = pvPortMalloc(sizeof(SubjectArray));
    ASSERT(node.subjects);
    node.subjects->capacity = 0;
    node.subjects->occupied = 0;

    static_assert(configNODE_NUM_INTERFACES == 1, "Multiple redundant interfaces not yet supported");
    fdcan_configure(0u, bitrate[0], bitrate[1], eFDCAN_MODE_NORMAL, use_fd);

    do_update_node_id(NULL);

    extern TaskHandle_t start_transport_task(void);
    node.transport_task = start_transport_task();

    const TickType_t heartbeat_ticks = pdMS_TO_TICKS(1000ul / uavcan_node_Heartbeat_1_0_MAX_PUBLICATION_PERIOD);
    node.heartbeat_timer             = xTimerCreate("Heartbeat", heartbeat_ticks, pdTRUE, NULL, send_heartbeat);
    ASSERT(node.heartbeat_timer);
    xTimerStart(node.heartbeat_timer, portMAX_DELAY);

#if configNODE_INCLUDE_AUTOMATIC_NODE_ID_ALLOCATION
    start_pnp_client_task();
#endif

    // uavcan.node.GetInfo.1.0
    static NodeSubscription sub_get_info = {.handler = handle_GetInfo_request};
    node_register_service(uavcan_node_GetInfo_1_0_FIXED_PORT_ID_, uavcan_node_GetInfo_Request_1_0_EXTENT_BYTES_,
                          &sub_get_info);

    // uavcan.node.ExecuteCommand.1.1
    static NodeSubscription sub_execute_command = {.handler = handle_ExecuteCommand_request};
    node_register_service(uavcan_node_ExecuteCommand_1_1_FIXED_PORT_ID_,
                          uavcan_node_ExecuteCommand_Request_1_1_EXTENT_BYTES_, &sub_execute_command);

#if configREGISTRY_INCLUDE_SERVICES
    // uavcan.register.List.1.0
    static NodeSubscription sub_reg_list = {.handler = handle_register_list_request};
    node_register_service(uavcan_register_List_1_0_FIXED_PORT_ID_, uavcan_register_List_Request_1_0_EXTENT_BYTES_,
                          &sub_reg_list);

    // uavcan.register.Access.1.0
    static NodeSubscription sub_reg_access = {.handler = handle_register_access_request};
    node_register_service(uavcan_register_Access_1_0_FIXED_PORT_ID_, uavcan_register_Access_Request_1_0_EXTENT_BYTES_,
                          &sub_reg_access);
#endif

#if configTIMEKEEPER_INCLUDE_NETWORK_TIMESYNC
    static NodeSubscription sub_timesync;
    node_subscribe(uavcan_time_Synchronization_1_0_FIXED_PORT_ID_, uavcan_time_Synchronization_1_0_EXTENT_BYTES_,
                   &sub_timesync);
    sub_timesync.handler = handle_time_synchronization_transfer;
#endif

    // initialization complete
    node.mode = uavcan_node_Mode_1_0_OPERATIONAL;

    // send the first heartbeat right away
    send_heartbeat(NULL);
    LOG_DEBUG("Node %i initialized.", node.cins.node_id);
}

void node_try_publish(const uint32_t                      valid_for_us,
                      const CanardTransferMetadata* const metadata,
                      const size_t                        payload_size,
                      const uint8_t* const                payload,
                      const TickType_t                    block_time) {
    if (!node_initialized()) return;
    if (xSemaphoreTake(node.lock, block_time) != pdTRUE) return;
    const CanardMicrosecond tx_deadline = get_monotonic_timestamp_us() + valid_for_us;
    for (uint_fast8_t i = 0; i < configNODE_NUM_INTERFACES; i++) {
        const bool queue_half_full = 2u * node.queue[i].size > node.queue[i].capacity;
        if (metadata->priority == CanardPriorityOptional && queue_half_full) {
            // UAVCAN Specification v1.0-beta, Section 4.1.1.3 "Transfer priority":
            //   Optional – These messages might never be sent (theoretically) for some possible system states.
            //   The system shall tolerate never exchanging optional messages in every possible state. The bus designer
            //   can ignore these messages when calculating bus load. This should be the priority used for diagnostic or
            //   debug messages that are not required on an operational system.
            continue;
        }
        int32_t result = canardTxPush(&node.queue[i], &node.cins, tx_deadline, metadata, payload_size, payload);
        if (result > 0) {
            LOG_TRACE("%lu frames enqueued in TxQueue %u", result, (uint8_t) i);
        } else if (result == -CANARD_ERROR_INVALID_ARGUMENT) {
            if (!node_is_anonymous()) LOG_WARNING("canard push: invalid argument");
        } else if (result == -CANARD_ERROR_OUT_OF_MEMORY) {
            LOG_ERROR("TxQueue %u out of memory", (uint8_t) i);
        } else {
            WTF();
        }
    }
    xSemaphoreGive(node.lock);

    // poke the transport worker to attempt transmitting the new transfer
    xTaskNotifyIndexed(node.transport_task, 0, 0, eNoAction);
}

void node_publish(const uint32_t                      valid_for_us,
                  const CanardTransferMetadata* const metadata,
                  const size_t                        payload_size,
                  const uint8_t* const                payload) {
    node_try_publish(valid_for_us, metadata, payload_size, payload, portMAX_DELAY);
}

/**
 * @brief Place Received Transfer into subscribing Task's Mailbox via FreeRTOS Task Notification
 *
 * This function is an RxTransferHandler, placing received transfers (mostly messages) into the subscribing task's
 * notification inbox. It may be picked up by the task via node_poll(), or overwritten by the next incoming transfer.
 * Any overwritten transfer is handed to the calling transport worker task to be cleaned up.
 *
 * @param new_transfer received transfer to hand to the subscribing task
 * @param sub subscription the received transfer matched
 * @return CanardRxTransfer* previous, now overwritten transfer in the same mailbox, to be free'd by the caller
 *
 * @see RxTransferHandler, node_subscribe, node_poll
 */
static CanardRxTransfer* place_transfer_into_task_mailbox(const CanardRxTransfer*       new_transfer,
                                                          const NodeSubscription* const sub) {
    if (sub->mailbox >= configTASK_NOTIFICATION_ARRAY_ENTRIES) return (CanardRxTransfer*) new_transfer;
    CanardRxTransfer* previous = NULL;
    xTaskNotifyAndQueryIndexed(sub->task, sub->mailbox, (uint32_t) new_transfer, eSetValueWithOverwrite,
                               (uint32_t*) &previous);

    if (previous != NULL) {
        const CanardTransferMetadata* const meta = &previous->metadata;
        LOG_NOTICE("%s %u.%02u from %u dropped", CANARD_TRANSFER_KIND_NAMES[meta->transfer_kind], meta->port_id,
                   meta->transfer_id, meta->remote_node_id);
    }
    return previous;
}

/**
 * @brief Subscribe to Message, Request or Response
 *
 * This function implements the logic behind node_subscribe(), node_register_service() and node_call().
 *
 * @param tk canard transfer kind to subscribe to
 * @param port subject or service ID to subscribe to
 * @param extent message extent as specified in the message data type, see UAVCAN specification section 3.4.5.5
 * @param out_sub reference to existing subscription object
 * @return int_fast8_t number of new subscriptions created (0 or 1), asserted not to be negative
 *
 * @see node_subscribe, node_register_service, node_call
 */
static int_fast8_t
subscribe(const CanardTransferKind tk, const CanardPortID port, const size_t extent, NodeSubscription* const out_sub) {
    ASSERT(out_sub);
    ASSERT(out_sub->handler);
    const CanardMicrosecond timeout = CANARD_DEFAULT_TRANSFER_ID_TIMEOUT_USEC;
    xSemaphoreTake(node.lock, portMAX_DELAY);
    int_fast8_t result = canardRxSubscribe(&node.cins, tk, port, extent, timeout, &out_sub->canard_sub);
    xSemaphoreGive(node.lock);
    ASSERT(result >= 0);
    out_sub->canard_sub.user_reference = (void*) out_sub;
    LOG_DEBUG("Subscribed to %s %u", CANARD_TRANSFER_KIND_NAMES[tk], port);
    return result;
}

/**
 * @brief Unsubscribe from Previously Subscribed to Message, Request or Response
 *
 * This function implements the logic behind node_unsubscribe(), node_unregister_service() and node_call().
 *
 * @param tk canard transfer kind to unsubscribe from
 * @param port subject or service ID to unsubscribe from
 * @return int_fast8_t number of subscriptions removed (0 or 1), asserted not to be negative
 *
 * @see node_unsubscribe, node_unregister_service, node_call
 */
static int_fast8_t unsubscribe(const CanardTransferKind tk, const CanardPortID port) {
    xSemaphoreTake(node.lock, portMAX_DELAY);
    int_fast8_t result = canardRxUnsubscribe(&node.cins, tk, port);
    xSemaphoreGive(node.lock);
    ASSERT(result >= 0);
    if (result == 1) LOG_DEBUG("Unsubscribed from %s %u", CANARD_TRANSFER_KIND_NAMES[tk], port);
    return result;
}

void node_subscribe(const CanardPortID port_id, const size_t extent, NodeSubscription* const out_sub) {
    out_sub->handler         = place_transfer_into_task_mailbox;
    const int_fast8_t result = subscribe(CanardTransferKindMessage, port_id, extent, out_sub);
    if (result) {
        insert_subject_id_into_array(port_id);
        update_hardware_acceptance_filters();
    } else {
        LOG_WARNING("existing sub to msg %u overwritten", port_id);
        // acceptance filter already present from previous subscription
    }
}

void node_unsubscribe(const CanardPortID port_id) {
    const int_fast8_t result = unsubscribe(CanardTransferKindMessage, port_id);
    if (result) {
        remove_subject_id_from_array(port_id);
        update_hardware_acceptance_filters();
    } else {
        LOG_WARNING("msg %u not subscribed to in the first place", port_id);
    }
}

CanardRxTransfer* node_poll(const NodeSubscription* const sub, const TickType_t block_time) {
    CanardRxTransfer* out    = NULL;
    const BaseType_t  result = xTaskNotifyWaitIndexed(sub->mailbox, 0, UINT32_MAX, (uint32_t*) &out, block_time);
    if (!result) LOG_TRACE("Mailbox %u timed out", (uint8_t) sub->mailbox);
    return out;
}

void node_register_service(const CanardPortID port_id, const size_t extent, NodeSubscription* const out_sub) {
    const int_fast8_t result = subscribe(CanardTransferKindRequest, port_id, extent, out_sub);
    ASSERT(result);
}

void node_unregister_service(const CanardPortID port_id) { unsubscribe(CanardTransferKindRequest, port_id); }

CanardRxTransfer* node_call(const CanardTransferMetadata* const metadata,
                            const size_t                        request_size,
                            const size_t                        response_extent,
                            const uint8_t* const                request_buffer,
                            void*                               free_after_tx,
                            const uint32_t                      mailbox,
                            const TickType_t                    block_time_ms) {
    NodeSubscription* sub = pvPortMalloc(sizeof(NodeSubscription));
    ASSERT(sub);
    sub->task                = xTaskGetCurrentTaskHandle();
    sub->mailbox             = mailbox;
    sub->handler             = place_transfer_into_task_mailbox;
    const int_fast8_t result = subscribe(CanardTransferKindResponse, metadata->port_id, response_extent, sub);
    ASSERT(result);

    const uint32_t valid_for_us = block_time_ms > UINT32_MAX / 1000 ? UINT32_MAX : 1000UL * block_time_ms;
    node_publish(valid_for_us, metadata, request_size, request_buffer);
    vPortFree(free_after_tx);

    TickType_t        now      = xTaskGetTickCount();
    TickType_t        deadline = now + block_time_ms;
    CanardRxTransfer* out      = NULL;

    while ((now = xTaskGetTickCount()) < deadline) {
        out = node_poll(sub, deadline - now);
        if (out == NULL) {
            // Timeout
            break;
        }
        ASSERT(out->metadata.transfer_kind == CanardTransferKindResponse);
        ASSERT(out->metadata.port_id == metadata->port_id);
        if (out->metadata.transfer_id == (metadata->transfer_id & CANARD_TRANSFER_ID_MAX)) {
            // this is the response we've been waiting for
            break;
        }
        // we've received a response that does not match our request; clean up and try again
        clean_up_rx_transfer(out);
        out = NULL;
    }

    unsubscribe(CanardTransferKindResponse, metadata->port_id);

    // Guard against an unlikely race condition memory leak:
    // if a response arrives between node_poll() timeout and unsubscribing, the RX transfer still needs cleaning up.
    clean_up_rx_transfer(node_poll(sub, 0));

    vPortFree(sub);
    return out;
}

CanardRxTransfer* dispatch_request_handler_task(const TaskFunction_t         task,
                                                const char* const            short_name,
                                                const char* const            full_name,
                                                const configSTACK_DEPTH_TYPE stack_size,
                                                const UBaseType_t            priority,
                                                const CanardRxTransfer*      transfer) {
    LOG_DEBUG("%s requested, dispatching handler service", full_name);

    char name[configMAX_TASK_NAME_LEN];
    int  len =
        sprintf(name, "%s %3u/%02u", short_name, transfer->metadata.remote_node_id, transfer->metadata.transfer_id);
    ASSERT(len < configMAX_TASK_NAME_LEN);

    BaseType_t rv = xTaskCreate(task, name, stack_size, (void*) transfer, priority, NULL);

    if (rv != pdPASS) {
        LOG_ERROR("%s request dropped, insufficient memory", full_name);
        return (CanardRxTransfer*) transfer;
    }
    return NULL;
}

// ======================================== TRANSPORT WORKER TASK INTERFACE ============================================

/**
 * @brief Ingest a Frame from Interface into Canard Instance
 *
 * This function is used by the transport worker task, forming the link between media IO interface and libcanard.
 *
 * @param timestamp_usec reception timestamp
 * @param frame received frame
 * @param rti redundant transport index of the interface the frame was received via
 * @param out_transfer RxTransfer to populate, if the ingested frame completed a transfer
 * @param out_subscription pointer to write subscription reference to, optional
 * @return int_fast8_t canardRxAccept result
 *
 * @see canardRxAccept
 */
int_fast8_t node_rx_accept(const CanardMicrosecond  timestamp_usec,
                           const CanardFrame* const frame,
                           const uint_fast8_t       rti,
                           CanardRxTransfer* const  out_transfer,
                           NodeSubscription** const out_subscription) {
    CanardRxSubscription* csub;
    xSemaphoreTake(node.lock, portMAX_DELAY);
    const int_fast8_t result = canardRxAccept(&node.cins, timestamp_usec, frame, rti, out_transfer, &csub);
    xSemaphoreGive(node.lock);
    if (out_subscription) *out_subscription = (NodeSubscription*) csub->user_reference;
    return result;
}
/**
 * @brief Peek into Libcanard Interface Tx Queue for Next Frame
 *
 * @param rti redundant transport index to query Tx queue of
 * @return const CanardTxQueueItem* next frame to transmit, or NULL if the Tx queue is exhausted
 */
const CanardTxQueueItem* node_tx_peek(const uint_fast8_t rti) {
    ASSERT(rti < configNODE_NUM_INTERFACES);
    xSemaphoreTake(node.lock, portMAX_DELAY);
    const CanardTxQueueItem* item = canardTxPeek(&node.queue[rti]);
    xSemaphoreGive(node.lock);
    return item;
}

/**
 * @brief Remove Transmitted Frame from Interface Tx Queue
 *
 * @param rti redundant transport index to remove the frame from
 * @param item frame (or rather queue item) to remove
 */
void node_tx_free(const uint_fast8_t rti, const CanardTxQueueItem* const item) {
    ASSERT(rti < configNODE_NUM_INTERFACES);
    xSemaphoreTake(node.lock, portMAX_DELAY);
    CanardTxQueueItem* mut_item = canardTxPop(&node.queue[rti], item);
    xSemaphoreGive(node.lock);
    vPortFree(mut_item);
}

// ============================================ UAVCAN APPLICATION LEVEL ===============================================

static void send_heartbeat(TimerHandle_t th __unused) {
    node.uptime = get_monotonic_timestamp_us() / 1000000ul;

    /* Cyphal Specification v1.0-beta, Section 5.3.2 Node heartbeat
     *
     *   Every non-anonymous Cyphal node shall report its status and presence by periodically publishing messages
     *   of type uavcan.node.Heartbeat (section 6.4.4 on page 108) at a fixed rate specified in the message definition
     *   using the fixed subject-ID. Anonymous nodes shall not publish to this subject.
     */
    if (node_is_anonymous()) return;

    // manual "serialized" representation of a uavcan.node.Heartbeat.1.0
    const struct __packed Heartbeat10 {
        uint32_t uptime;
        uint8_t  health;
        uint8_t  mode;
        uint8_t  vssc;
    } hb = {.uptime = node_get_uptime(), .health = get_system_health(), .mode = get_system_mode(), .vssc = node.vssc};
    static_assert(sizeof(hb) == uavcan_node_Heartbeat_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_, "Something's wrong");

    LOG_TRACE("send heartbeat: uptime=%lu, health=%u, mode=%u, VSSC=0x%02x", hb.uptime, hb.health, hb.mode, hb.vssc);

    static uint_fast8_t          transfer_id = 0;
    const CanardTransferMetadata meta        = {.priority       = CanardPriorityNominal,
                                                .transfer_kind  = CanardTransferKindMessage,
                                                .port_id        = uavcan_node_Heartbeat_1_0_FIXED_PORT_ID_,
                                                .remote_node_id = CANARD_NODE_ID_UNSET,
                                                .transfer_id    = transfer_id++};

    node_publish(1000000ul / uavcan_node_Heartbeat_1_0_MAX_PUBLICATION_PERIOD, &meta,
                 uavcan_node_Heartbeat_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_, (uint8_t*) &hb);
}

static void send_GetInfo_response(void* param) {
    LOG_TRACE("%s task spawned (%p)", pcTaskGetName(NULL), xTaskGetCurrentTaskHandle());

    CanardRxTransfer* rx = (CanardRxTransfer*) param;

    const CanardTransferMetadata meta = {.priority       = rx->metadata.priority,
                                         .transfer_kind  = CanardTransferKindResponse,
                                         .port_id        = rx->metadata.port_id,
                                         .remote_node_id = rx->metadata.remote_node_id,
                                         .transfer_id    = rx->metadata.transfer_id};
    vPortFree(rx->payload);
    vPortFree(rx);

    const bool dcb_valid = dcb_validate();

    uavcan_node_GetInfo_Response_1_0 response = {
        .protocol_version = {CANARD_UAVCAN_SPECIFICATION_VERSION_MAJOR, CANARD_UAVCAN_SPECIFICATION_VERSION_MINOR},
        .hardware_version = {dcb_valid ? device_configuration_block.release.major - 'A' + 1 : 0,
                             dcb_valid ? device_configuration_block.release.minor : 0},
        .software_version            = {image_hdr.version.major, image_hdr.version.minor},
        .software_vcs_revision_id    = __builtin_bswap64(*(uint64_t*) &image_hdr.git_sha[0]),
        .software_image_crc          = {.elements = {__image_crc}, .count = 1},
        .certificate_of_authenticity = {.count = 0}};

    if (dcb_valid) {
        memcpy(response.unique_id, device_configuration_block.uid, sizeof(response.unique_id));
    } else {
        memset(response.unique_id, 0, sizeof(response.unique_id));
    }

    const char* const name = dcb_valid ? device_configuration_block.name : image_hdr.name;
    strcpy((char*) response.name.elements, name);
    response.name.count = strlen(name);

    size_t   payload_size = uavcan_node_GetInfo_Response_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_;
    uint8_t* payload      = pvPortMalloc(payload_size);
    ASSERT(payload);
    const int_fast8_t status = uavcan_node_GetInfo_Response_1_0_serialize_(&response, payload, &payload_size);
    ASSERT(status == NUNAVUT_SUCCESS);

    node_publish(1000000ul, &meta, payload_size, payload);
    vPortFree(payload);
    LOG_INFO("GetInfo request %u from %i handled", meta.transfer_id, meta.remote_node_id);
    vTaskDelete(NULL);
    WTF();
}

// this is an RxTransferHandler
CanardRxTransfer* handle_GetInfo_request(const CanardRxTransfer* transfer, const NodeSubscription* const sub) {
    return dispatch_request_handler_task(send_GetInfo_response, "GetInfo", "node.GetInfo", 512,
                                         configNODE_GET_INFO_TASK_PRIORITY, transfer);
}

static void ExecuteCommand(void* param) {
    LOG_TRACE("%s task spawned (%p)", pcTaskGetName(NULL), xTaskGetCurrentTaskHandle());

    CanardRxTransfer* rx = (CanardRxTransfer*) param;

    uavcan_node_ExecuteCommand_Request_1_1 request;
    int_fast8_t status = uavcan_node_ExecuteCommand_Request_1_1_deserialize_(&request, rx->payload, &rx->payload_size);
    ASSERT(status == NUNAVUT_SUCCESS);
    const CanardTransferMetadata meta = {.priority       = rx->metadata.priority,
                                         .transfer_kind  = CanardTransferKindResponse,
                                         .port_id        = rx->metadata.port_id,
                                         .remote_node_id = rx->metadata.remote_node_id,
                                         .transfer_id    = rx->metadata.transfer_id};
    vPortFree(rx->payload);
    vPortFree(rx);

    uavcan_node_ExecuteCommand_Response_1_1 response;

    bool restart = false;

    switch (request.command) {
        case uavcan_node_ExecuteCommand_Request_1_1_COMMAND_RESTART:
            // TODO: check system state
            response.status = uavcan_node_ExecuteCommand_Response_1_1_STATUS_SUCCESS;
            restart         = true;  // delay restart and send a response first
            break;
#if configREGISTRY_INCLUDE_SAVE_PERSISTENT_REGISTERS
        case uavcan_node_ExecuteCommand_Request_1_1_COMMAND_STORE_PERSISTENT_STATES:
            response.status = uavcan_node_ExecuteCommand_Response_1_1_STATUS_SUCCESS;
            save_persistent_registers();
            break;
#endif
        case uavcan_node_ExecuteCommand_Request_1_1_COMMAND_BEGIN_SOFTWARE_UPDATE:
            if (!request.parameter.count) {
                response.status = uavcan_node_ExecuteCommand_Response_1_1_STATUS_BAD_PARAMETER;
            } else if (node.mode == uavcan_node_Mode_1_0_SOFTWARE_UPDATE) {
                // TODO: check system state
                response.status = uavcan_node_ExecuteCommand_Response_1_1_STATUS_BAD_STATE;
            } else {
                shared_register_dfu_request((const char*) request.parameter.elements, request.parameter.count,
                                            meta.remote_node_id);
                response.status = uavcan_node_ExecuteCommand_Response_1_1_STATUS_SUCCESS;
                restart         = true;  // delay restart and send a response first
            }
            break;
        default:
            response.status = node_execute_external_command((const uavcan_node_ExecuteCommand_Request_1_1*) &request);
            break;
    }

    size_t   payload_size = uavcan_node_ExecuteCommand_Response_1_1_SERIALIZATION_BUFFER_SIZE_BYTES_;
    uint8_t* payload      = pvPortMalloc(payload_size);
    ASSERT(payload);
    status = uavcan_node_ExecuteCommand_Response_1_1_serialize_(&response, payload, &payload_size);
    ASSERT(status == NUNAVUT_SUCCESS);

    node_publish(1000000ul, &meta, payload_size, payload);
    vPortFree(payload);
    LOG_INFO("Command %u (request %u from %i) handled with status %u", request.command, meta.transfer_id,
             meta.remote_node_id, response.status);

    if (restart) {
        vTaskDelay(pdMS_TO_TICKS(10));
        save_persistent_registers();
        NVIC_SystemReset();  // never returns
    }

    vTaskDelete(NULL);
    WTF();
}

// this is an RxTransferHandler
CanardRxTransfer* handle_ExecuteCommand_request(const CanardRxTransfer* transfer, const NodeSubscription* const sub) {
    return dispatch_request_handler_task(ExecuteCommand, "xCmd", "node.ExecuteCommand", 512,
                                         configNODE_EXECUTE_COMMAND_TASK_PRIORITY, transfer);
}

#if configNODE_INCLUDE_AUTOMATIC_NODE_ID_ALLOCATION
#    include <uavcan/pnp/NodeIDAllocationData_2_0.h>

/**
 * @brief Send (broadcast) a `uavcan.pnp.NodeIDAllocationData.2.0` message.
 *
 * This function only covers _broadcasting_ the allocation request, see `run_node_id_allocation_algorithm()` for the
 * actual implementation.
 *
 * @see run_node_id_allocation_algorithm
 */
static void broadcast_allocation_request_2_0(void) {
    static uint_fast8_t transfer_id = 0;

    uavcan_pnp_NodeIDAllocationData_2_0 obj = {.node_id = {CANARD_NODE_ID_MAX - 2}};
    memcpy(obj.unique_id, device_configuration_block.uid, sizeof(obj.unique_id));

    size_t  size = uavcan_pnp_NodeIDAllocationData_2_0_SERIALIZATION_BUFFER_SIZE_BYTES_;
    uint8_t buf[uavcan_pnp_NodeIDAllocationData_2_0_SERIALIZATION_BUFFER_SIZE_BYTES_];

    const int_fast8_t result = uavcan_pnp_NodeIDAllocationData_2_0_serialize_(&obj, buf, &size);
    ASSERT(result == NUNAVUT_SUCCESS);

    const CanardTransferMetadata meta = {.priority       = CanardPrioritySlow,
                                         .transfer_kind  = CanardTransferKindMessage,
                                         .port_id        = uavcan_pnp_NodeIDAllocationData_2_0_FIXED_PORT_ID_,
                                         .remote_node_id = CANARD_NODE_ID_UNSET,
                                         .transfer_id    = transfer_id++};

    node_publish(100000ul, &meta, size, buf);
}

/**
 * @brief Request a Node ID from an allocator, if possible.
 *
 * This function implements the node ID allocation algorithm outlined in the `uavcan.pnp.NodeIDAllocationData.2.0`
 * message. In broad strokes this function does the following:
 *
 *   - create a subscription to the NodeIDAllocationData message
 *   - periodically emit an allocation request (same message type)
 *   - monitor responses until a valid response is found
 *   - cancel subscription, update node ID and return
 *
 * ATTENTION: this function blocks until a valid node ID is received!
 */
static void run_node_id_allocation_algorithm(void) {
    NodeSubscription sub_allocation_data = {.task = xTaskGetCurrentTaskHandle(), .mailbox = 1};
    node_subscribe(uavcan_pnp_NodeIDAllocationData_2_0_FIXED_PORT_ID_,
                   uavcan_pnp_NodeIDAllocationData_2_0_EXTENT_BYTES_, &sub_allocation_data);

    TickType_t        next_broadcast = xTaskGetTickCount();
    CanardRxTransfer* transfer       = NULL;

    LOG_INFO("PNP Client initialized");

    while (uavcan_node_id > CANARD_NODE_ID_MAX) {
        while (!READ_BIT(RNG->SR, RNG_SR_DRDY)) {
            // wait for random data to be ready
        }
        // random delay between 50 and 561 ms
        next_broadcast += pdMS_TO_TICKS((RNG->DR & 0x1fful) + 50);

        while (uavcan_node_id > CANARD_NODE_ID_MAX &&
               (transfer = node_poll(&sub_allocation_data, next_broadcast - xTaskGetTickCount())))
        {
            LOG_DEBUG("Node ID Allocation Data received");

            if (transfer->metadata.remote_node_id <= CANARD_NODE_ID_MAX) {
                // non-anonymous transfer
                uavcan_pnp_NodeIDAllocationData_2_0 allocation_data;
                size_t                              size = transfer->payload_size;
                const int_fast8_t                   ds_status =
                    uavcan_pnp_NodeIDAllocationData_2_0_deserialize_(&allocation_data, transfer->payload, &size);
                ASSERT(ds_status == 0);

                if (memcmp(device_configuration_block.uid, allocation_data.unique_id,
                           sizeof(allocation_data.unique_id)) == 0 &&
                    allocation_data.node_id.value <= CANARD_NODE_ID_MAX)
                {
                    // this is a response to our allocation request, we got a node ID!
                    uavcan_node_id = allocation_data.node_id.value;
                    LOG_NOTICE("Node ID %u received from %u", allocation_data.node_id.value,
                               transfer->metadata.remote_node_id);
                }
            }

            vPortFree((void*) transfer->payload);
            vPortFree((void*) transfer);
        }

        if (uavcan_node_id <= CANARD_NODE_ID_MAX) break;

        broadcast_allocation_request_2_0();
    }

    node_unsubscribe(uavcan_pnp_NodeIDAllocationData_2_0_FIXED_PORT_ID_);
    do_update_node_id(NULL);
}

static void PNPClient(void* param __unused) {
    ASSERT(dcb_validate());                    // make sure we have a valid UID
    ASSERT(RCC->AHB2ENR & RCC_AHB2ENR_RNGEN);  // ensure we have randomness

    for (;;) {
        if (node_is_anonymous()) run_node_id_allocation_algorithm();

        vTaskDelay(pdMS_TO_TICKS(1000));
    }
}
#endif
