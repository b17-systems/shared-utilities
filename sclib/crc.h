/**
 * @file crc.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief CRC Utilities
 * @version 0.1
 * @date 2021-07-13
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#pragma once

#include <stdint.h>
#include <stdbool.h>

/**
 * @brief Calculate and Verify CRC-32 Checksum
 *
 * @details This function calculates the CRC-32 checksum of an arbitrary block of data, compares it with an
 *          expected value and returns both the new checksum (calculate) and the verification result (verify).
 *          The expected checksum is passed to the function by reference, and to that location the newly calculated
 *          checksum will be written back to.
 *
 * @attention The length has to be word (32-bit) aligned.
 *
 * @param data pointer to start of data block to calculate checksum of
 * @param len length of data block in bytes; should be a multiple of 4
 * @param in_out_crc pointer to variable to check against, and location to write calculated CRC back to
 * @return CRC of data block matches referenced expected checksum
 *
 * @see crc_init, crc_deinit
 */
bool crc32(const uint32_t* data, uint32_t len, uint32_t* in_out_crc);

/**
 * @brief Initialize CRC Peripheral
 */
void crc_init(void);

/**
 * @brief De-Initialize CRC Peripheral
 */
void crc_deinit(void);
