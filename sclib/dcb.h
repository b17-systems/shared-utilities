/**
 * @file dcb.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Device configuration Block
 * @version 0.2
 * @date 2021-07-13
 *
 * @copyright Copyright (c) 2022 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 * @details The Device Configuration Block (DCB) contains device-specific, immutable configuration data.
 *          It will never change during the lifetime of a device and cannot be updated at runtime.
 */

#pragma once

#include <stdint.h>
#include <stdbool.h>
#include <stdlib.h>

static const uint32_t DEVICE_CONFIG_MAGIC = 0xa5 << 0 | 'c' << 8 | 'f' << 16 | 'g' << 24;  // 0x676663a5

enum DeviceConfigVersion {
    eDEV_CONFIG_VERSION_0       = 0,
    eDEV_CONFIG_VERSION_1       = 1,
    eDEV_CONFIG_VERSION_CURRENT = eDEV_CONFIG_VERSION_1
};

/* Hardware Version Specification in Little-Endian Order, sortable as u16 */
typedef union HardwareVersion {
    struct {
        const uint8_t minor;  // 1 through 255 (Altium style)
        const char    major;  // 'A' through 'Z' (Altium style)
    };
    uint16_t u16;  // sort accessor, A.1 (16641, 0x4101) through Z.255 (23295, 0x5aff)
} HWVersion_t;

typedef struct __aligned(8) DeviceConfigurationV1 DeviceConfig_t;

// Device Configuration Block, defined by Linker
extern const DeviceConfig_t device_configuration_block;

/**
 * @brief Validate the Device Configuration Block
 *
 * This function checks if the DCB
 * - starts with DEVICE_CONFIG_MAGIC
 * - has the correct (current) version
 * - has a valid checksum at the end
 *
 * @return Device Configuration Block is valid
 */
bool dcb_validate(void);

struct DeviceConfigurationV1 {
    const uint32_t magic;    // should be DEVICE_CONFIG_MAGIC (0x676663a5)
    const uint32_t version;  // eDEV_CONFIG_VERSION_1
    const uint32_t size;     // config block length in bytes, e.g. sizeof(struct DeviceConfigurationV1)

    const uint32_t    __pad0;    // padding to align next member to double-word boundary
    const char        name[52];  // null-terminated device/platform name, as in uavcan.node.GetInfo.1.0.name
    const HWVersion_t release;   // device/platform hardware release
    const uint16_t    __pad1;    // padding to align to word boundary, should be zero
    const uint64_t    date;      // manufacturing date in microseconds since 1970-01-01T00:00:00Z
    const uint8_t     uid[16];   // 128-bit unique hardware identifier
    const uint32_t    __pad2;    // make crc end align with double-word boundary

    const uint32_t crc;  // CRC-32 of the configuration block up to here
};

struct DeviceConfigurationV0 {
    const uint32_t magic;    // should be DEVICE_CONFIG_MAGIC (0x676663a5)
    const uint32_t version;  // eDEV_CONFIG_VERSION_0
    const uint32_t size;     // config block length in bytes, e.g. sizeof(struct DeviceConfigurationV0) == 72

    const char        name[52];  // null-terminated device/platform name, as in uavcan.node.GetInfo.1.0.name
    const HWVersion_t release;   // device/platform hardware release
    const uint16_t    __pad0;    // padding to align to word boundary, should be zero

    const uint32_t crc;  // CRC-32 of the configuration block up to here
};
