/**
 * @file shared_ram.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Shared RAM Interface
 * @version 0.1
 * @date 2021-06-23
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 * Shared RAM is the means of communication between different applications (e.g. bootloader, app loader, main app)
 * running on the same processor. This module implements the reboot-loop mitigation interface and device firmware update
 * request passing from the main app to the app loader.
 */

#pragma once

#include <inttypes.h>
#include "image.h"

/**
 * @brief Get Current Application's Reboot Counter
 *
 * If this is higher than 1, this application must have crashed recently before reaching its checkpoint.
 *
 * @return reboot counter value
 * @see shared_reset_boot_count
 */
uint32_t shared_get_boot_count(void);

/**
 * @brief Reset Current Application's Reboot Counter
 *
 * By calling this function an application can "check in" and record that it has started and taken up operations.
 * Any reboot (crash) before calling this function will cause the reboot counter to increase and eventually cause the
 * caller to stop booting it. Any reset after calling this function will lead to a normal reboot into this app.
 */
void shared_reset_boot_count(void);

/**
 * @brief Get Current Application's Context
 *
 * @return pointer to currently running application's context, or NULL if unknown
 */
ApplicationContext_t* shared_get_current_context(void);

/**
 * @brief Get Current Application's Caller
 *
 * @return pointer to image header of the application which booted the currently running app
 */
ImageHeader_t* shared_get_caller(void);

/**
 * @brief Register a DFU Request for the App Loader to Pick Up
 *
 * @param file_path
 * @param path_length
 * @param server_node_id
 */
void shared_register_dfu_request(const char* file_path, size_t path_length, uint32_t server_node_id);

bool shared_dfu_requested(void);
int  shared_get_dfu_context(char*                 out_file_path,
                            uint32_t*             out_server_node_id,
                            const ImageHeader_t** out_current_application);

void shared_reset_dfu_request(void);
