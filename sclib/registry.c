/**
 * @file registry.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief UAVCAN Named Register Implementation
 * @version 0.1
 * @date 2021-12-24
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 * @attention This module requires a section for UAVCANRegister structs to be set aside and labelled by the linker:
 * .uavcan_registers : {
 *     . = ALIGN(4);
 *     __uavcan_registers_start__ = .;
 *     KEEP(*(.uavcan_registers))
 *     __uavcan_registers_end__ = .;
 * } > ROM
 *
 */

#include "registry.h"
#include "flash.h"
#include "memory_map.h"
#include "assert.h"
#include "crc.h"
#include "timekeeper.h"

#include <string.h>

#include <uavcan/_register/List_1_0.h>
#include <uavcan/_register/Access_1_0.h>
#include <uavcan/_register/Name_1_0.h>
#include <uavcan/_register/Value_1_0.h>

#define LOG_LEVEL configREGISTRY_LOG_LEVEL
#include <sclib/log.h>

const uint32_t NVM_SECTION_MAGIC = 0xa5 << 0 | 'n' << 8 | 'v' << 16 | 'm' << 24;  // 0x6d766ea5
enum NVMSectionVersion { eNVM_SECTION_VERSION_0 = 0, eNVM_SECTION_VERSION_CURRENT = eNVM_SECTION_VERSION_0 };

// total size 2k
typedef struct NVMSectionV0 {
    uint32_t magic;          // should be NVM_SECTION_MAGIC (0x6d766ea5)
    uint32_t version;        // should be eNVM_SECTION_VERSION_CURRENT
    uint8_t  content[2032];  // content buffer, unused sections shall be filled with 0xff
    uint32_t size;           // number of bytes utilized in content buffer
    uint32_t crc;            // CRC-32 over entire NVM bock (up to here)
} NVMSection_t;

static_assert(sizeof(NVMSection_t) == PAGE_SIZE, "The NVM Section should fit into one entire Flash Page");

extern const UAVCANRegister_t __uavcan_registers_start__;
extern const UAVCANRegister_t __uavcan_registers_end__;

const size_t ValueTypeSize[uavcan_register_Value_1_0_UNION_OPTION_COUNT_] = {
    0,  // Empty
    1,  // String
    1,  // Unstructured
    0,  // Bit; NOT IMPLEMENTED
    8,  // Integer64
    4,  // Integer32
    2,  // Integer16
    1,  // Integer8
    8,  // Natural64
    4,  // Natural32
    2,  // Natural16
    1,  // Natural8
    8,  // Real64
    4,  // Real32
    0   // Real16; NOT IMPLEMENTED
};
typedef enum { eRegisterValue, eRegisterMin, eRegisterDefault, eRegisterMax } SpecialRegisterType;

/**
 * @brief Get a named UAVCAN (base) register struct by name
 *
 * @details
 * This function searches the .uavcan_registers ROM section for the (base) register matching the requested name.
 * If a special register (marked by suffix) is requested AND a valid pointer to an output SpecialRegisterType variable
 * is provided, the corresponding base register (without suffix) is searched for.
 *
 * If the base register cannot be found, or if a non-existent special register (e.g. '>'-suffix for register without
 * configured maximum) is requested, NULL is returned.
 *
 * @param name name of the register in question
 * @param out_type[optional] type of the named register, determined by name suffix ('<', '=', '>', or nothing)
 * @return a pointer to the base register structure in ROM (.uavcan_registers section), or NULL if not found
 */
const UAVCANRegister_t* get_base_register_by_name(const char* const name, SpecialRegisterType* const out_type) {
    int len = strlen(name);
    if (len == 0) return NULL;

    if (out_type) {
        // decode special registers, if necessary
        switch (name[len - 1]) {
            case '<':
                *out_type = eRegisterMin;
                break;
            case '=':
                *out_type = eRegisterDefault;
                break;
            case '>':
                *out_type = eRegisterMax;
                break;
            default:
                *out_type = eRegisterValue;
                break;
        }
        if (*out_type != eRegisterValue) {
            --len;
        }
    }

    for (const UAVCANRegister_t* reg = &__uavcan_registers_start__; reg < &__uavcan_registers_end__; reg++)
        if (strncmp(reg->name, name, len) == 0) {
            if (out_type == NULL || *out_type == eRegisterValue || (*out_type == eRegisterMin && reg->min) ||
                (*out_type == eRegisterDefault && reg->dflt) || (*out_type == eRegisterMax && reg->max))
            {
                return reg;
            } else {
                // the base register has no configured min/default/max value,
                // therefore the special register does not exist
                return NULL;
            }
        }

    return NULL;
}

const UAVCANRegister_t* get_register_by_name(const char* const name) { return get_base_register_by_name(name, NULL); }

#if configREGISTRY_INCLUDE_SERVICES
/**
 * @brief Get a named UAVCAN (base) register struct by index
 *
 * This routine is required for the uavcan.register.List.1.0 request, in which register names are discovered by index.
 * Register specification structs are discovered in the .uavcan_registers ROM section, with the order set in the linking
 * process. The standard only requires the order not to change while the node is running, which this implementation
 * complies to.
 *
 * Every discovered base register is directly followed by its minimum, default and maximum value register (suffixed '<',
 * '=', and '>', respectively), given these are defined. A special register is indicated by the char in *out_suffix.
 * It is up to this function's caller to assemble a register's base name and the optional suffix into a complete name.
 *
 * @param index register position to find
 * @param out_suffix pointer to writable char to have a special register's suffix written to
 * @return a pointer to the base register structure in ROM (.uavcan_registers section),
 *         or NULL if the index is out of bounds
 */
const UAVCANRegister_t* get_register_by_index(const uint16_t index, char* const out_suffix) {
    uint16_t i = 0;
    ASSERT(out_suffix);
    *out_suffix = '\0';

    for (const UAVCANRegister_t* reg = &__uavcan_registers_start__; reg < &__uavcan_registers_end__; reg++) {
        if (reg->hidden) continue;
        if (i++ == index) {
            return reg;
        }
        if (reg->min && i++ == index) {
            *out_suffix = '<';
            return reg;
        }
        if (reg->dflt && i++ == index) {
            *out_suffix = '=';
            return reg;
        }
        if (reg->max && i++ == index) {
            *out_suffix = '>';
            return reg;
        }
    }

    return NULL;
}
#endif

// proxy function around generated `static inline int8_t uavcan_register_Access_Request_1_0_deserialize_()`
static int8_t _proxy_access_request_deserialize_(uavcan_register_Access_Request_1_0* const out_obj,
                                                 const uint8_t* const                      buffer,
                                                 size_t* const                             inout_buffer_size_bytes) {
    return uavcan_register_Access_Request_1_0_deserialize_(out_obj, buffer, inout_buffer_size_bytes);
}

bool load_persistent_registers(void) {
#if configREGISTRY_INCLUDE_LOAD_PERSISTENT_REGISTERS
    const NVMSection_t* const block                     = (const NVMSection_t*) &__nvm_start__;
    const char* const         literal_nvm_block_invalid = "NVM block invalid: ";
    if (block->magic != NVM_SECTION_MAGIC || block->version != eNVM_SECTION_VERSION_CURRENT) {
        // version mismatch
        LOG_ERROR("%smagic=0x%08lx version=%lu", literal_nvm_block_invalid, block->magic, block->version);
        return false;
    }
    uint32_t block_crc = block->crc;
    if (!crc32((const uint32_t*) block, sizeof(*block) - 4, &block_crc)) {
        // CRC mismatch
        LOG_ERROR("%sCRC mismatch", literal_nvm_block_invalid);
        return false;
    }

    const uint8_t*       read_position = &block->content[0];
    const uint8_t* const end_position  = &block->content[0] + block->size;

    uavcan_register_Access_Request_1_0* item = pvPortMalloc(sizeof(uavcan_register_Access_Request_1_0));
    ASSERT(item);

    while (read_position < end_position) {
        size_t       inout_buffer_size_bytes = end_position - read_position;
        const int8_t result = _proxy_access_request_deserialize_(item, read_position, &inout_buffer_size_bytes);
        ASSERT(result == NUNAVUT_SUCCESS);
        read_position += inout_buffer_size_bytes;

        // By inserting a NULL character at the end of the uint8_t array, item->name can be interpreted as a
        // null-terminated string. The maximum length of uavcan.register.Name.1.0 is 255, and since the
        // uavcan_register_Name_1_0 struct is not packed, there will be a padding byte before the .count member.
        // Therefore it is safe to overwrite .elements[.count], even with a full-length name.
        ASSERT(&item->name.name.elements[item->name.name.count] < (uint8_t*) &item->name.name.count);
        item->name.name.elements[item->name.name.count] = '\0';

        const UAVCANRegister_t* const reg = get_register_by_name((const char*) &item->name);
        if (!reg) {
            LOG_INFO("register %s from NVM not found in application", (const char*) &item->name);
            continue;  // likely belongs to a different application running on the same platform
        }
        ASSERT(reg);
        if (reg->constant) {
            LOG_WARNING("constant register %s found in NVM, ignoring...", (const char*) &item->name);
            continue;
        }
        const bool use_lock = (xTaskGetSchedulerState() == taskSCHEDULER_RUNNING) && reg->lock && *reg->lock;
        ASSERT(item->value._tag_ == reg->type);
        ASSERT(reg->type != eUAVCANRegisterTypeEmpty && reg->type != eUAVCANRegisterTypeBitArray &&
               reg->type != eUAVCANRegisterTypeReal16Array);
        ASSERT(item->value.unstructured.value.count == reg->len);
        ASSERT(reg->value);
        if (use_lock) xSemaphoreTake(*reg->lock, portMAX_DELAY);
        memcpy(reg->value, &item->value, reg->len * ValueTypeSize[reg->type]);
        if (reg->update_hook) reg->update_hook();
        if (use_lock) xSemaphoreGive(*reg->lock);
    }

    vPortFree(item);
    return true;
#else
    return false;
#endif
}

void save_persistent_registers(void) {
#if configREGISTRY_INCLUDE_SAVE_PERSISTENT_REGISTERS
    const NVMSection_t* const NVM = (const NVMSection_t*) &__nvm_start__;

    NVMSection_t* block = pvPortMalloc(sizeof(NVMSection_t));
    block->magic        = NVM_SECTION_MAGIC;
    block->version      = eNVM_SECTION_VERSION_CURRENT;

    uint8_t*             write_position = &block->content[0];
    const uint8_t* const end_position   = &block->content[0] + sizeof(block->content);

    uavcan_register_Access_Request_1_0* item = pvPortMalloc(sizeof(uavcan_register_Access_Request_1_0));
    uint8_t* sbuf = pvPortMalloc(uavcan_register_Access_Request_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_);
    ASSERT(item);
    ASSERT(sbuf);

    for (const UAVCANRegister_t* reg = &__uavcan_registers_start__; reg < &__uavcan_registers_end__; reg++) {
        if (!reg->persistent || reg->constant) continue;
        ASSERT(reg->type != eUAVCANRegisterTypeEmpty && reg->type != eUAVCANRegisterTypeBitArray &&
               reg->type != eUAVCANRegisterTypeReal16Array);

        ASSERT(reg->name);
        item->name.name.count = strlen(reg->name);
        ASSERT(item->name.name.count <= uavcan_register_Name_1_0_name_ARRAY_CAPACITY_);
        memcpy(&item->name, reg->name, item->name.name.count);

        item->value._tag_ = reg->type;
        ASSERT(reg->value);
        ASSERT(reg->type <= uavcan_register_Value_1_0_UNION_OPTION_COUNT_);
        const bool use_lock = (xTaskGetSchedulerState() == taskSCHEDULER_RUNNING) && reg->lock && *reg->lock;
        if (use_lock) xSemaphoreTake(*reg->lock, portMAX_DELAY);
        memcpy(&item->value, reg->value, reg->len * ValueTypeSize[reg->type]);
        if (use_lock) xSemaphoreGive(*reg->lock);
        // regardless of the underlying type, the .count property is always at (&item->value + 256B).
        item->value.unstructured.value.count = reg->len;

        size_t       inout_buffer_size_bytes = uavcan_register_Access_Request_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_;
        const int8_t result = uavcan_register_Access_Request_1_0_serialize_(item, sbuf, &inout_buffer_size_bytes);
        ASSERT(result == NUNAVUT_SUCCESS);
        ASSERT((uint32_t) write_position + inout_buffer_size_bytes <= (uint32_t) end_position);
        memcpy(write_position, sbuf, inout_buffer_size_bytes);
        write_position += inout_buffer_size_bytes;
    }

    vPortFree(item);
    vPortFree(sbuf);

    block->size = (uint32_t) write_position - (uint32_t) &block->content[0];
    while (write_position < end_position)
        *write_position++ = 0xff;

    crc32((const uint32_t*) block, sizeof(*block) - 4, &block->crc);

    ASSERT(sizeof(*block) <= (uint32_t) &__nvm_end__ - (uint32_t) &__nvm_start__);
    ASSERT((sizeof(*block) & 7u) == 0);

    if (block->crc != NVM->crc) {
        // something has changed, we'll need to refresh
        const uint32_t nvm_page = flash_get_page_number(&__nvm_start__);
        flash_unlock();
        flash_erase_page(nvm_page);
        flash_write(&__nvm_start__, (uint32_t*) block, sizeof(*block));
        flash_lock();
        LOG_INFO("persistent registers committed to NVM, utilization %u %%",
                 (unsigned int) ((100 * block->size + 50) / sizeof(block->content)));
    }

    vPortFree(block);
#endif
}

#if configREGISTRY_INCLUDE_SERVICES
// -------------------------------- uavcan.register.List Service Handler and Task -------------------------------------

static void send_register_list_response(void* param);

// this is an RxTransferHandler
CanardRxTransfer* handle_register_list_request(const CanardRxTransfer* transfer, const NodeSubscription* const sub) {
    return dispatch_request_handler_task(send_register_list_response, "List", "register.List", 384, 1, transfer);
}

static void send_register_list_response(void* param) {
    LOG_TRACE("%s task spawned (%p)", pcTaskGetName(NULL), xTaskGetCurrentTaskHandle());

    CanardRxTransfer* rx = (CanardRxTransfer*) param;
    int_fast8_t       result;

    uavcan_register_List_Request_1_0 request;
    result = uavcan_register_List_Request_1_0_deserialize_(&request, rx->payload, &rx->payload_size);
    ASSERT(result == NUNAVUT_SUCCESS);
    const CanardTransferMetadata meta = {.priority       = rx->metadata.priority,
                                         .transfer_kind  = CanardTransferKindResponse,
                                         .port_id        = rx->metadata.port_id,
                                         .remote_node_id = rx->metadata.remote_node_id,
                                         .transfer_id    = rx->metadata.transfer_id};
    vPortFree(rx->payload);
    vPortFree(rx);

    char suffix;
    // reg will be NULL if the list is exhausted
    const UAVCANRegister_t* reg = get_register_by_index(request.index, &suffix);

    uavcan_register_List_Response_1_0 response;
    if (reg) {
        response.name.name.count = strlen(reg->name);
        ASSERT(response.name.name.count <= uavcan_register_Name_1_0_name_ARRAY_CAPACITY_ - 1);
        memcpy((char*) &response.name, reg->name, response.name.name.count);
        if (suffix) {
            response.name.name.elements[response.name.name.count++] = (uint8_t) suffix;
        }
    } else {
        // empty
        response.name.name.count = 0;
    }

    size_t   size = uavcan_register_List_Response_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_;
    uint8_t* buf  = pvPortMalloc(size);
    ASSERT(buf);

    result = uavcan_register_List_Response_1_0_serialize_(&response, buf, &size);
    ASSERT(result == NUNAVUT_SUCCESS);

    node_publish(1000000ul, &meta, size, buf);
    LOG_INFO("register.List request %u from %i handled", meta.transfer_id, meta.remote_node_id);
    vPortFree(buf);

    LOG_DEBUG("hwm=%lu", uxTaskGetStackHighWaterMark(NULL));
    vTaskDelete(NULL);
    WTF();
}

// -------------------------------- uavcan.register.Access Service Handler and Task -----------------------------------

static void send_register_access_response(void* param);

// this is an RxTransferHandler
CanardRxTransfer* handle_register_access_request(const CanardRxTransfer* transfer, const NodeSubscription* const sub) {
    return dispatch_request_handler_task(send_register_access_response, "Access", "register.Access", 640, 1, transfer);
}

static void send_register_access_response(void* param) {
    LOG_TRACE("%s task spawned (%p)", pcTaskGetName(NULL), xTaskGetCurrentTaskHandle());

    CanardRxTransfer* rx = (CanardRxTransfer*) param;
    int_fast8_t       result;

    const CanardTransferMetadata meta = {.priority       = rx->metadata.priority,
                                         .transfer_kind  = CanardTransferKindResponse,
                                         .port_id        = rx->metadata.port_id,
                                         .remote_node_id = rx->metadata.remote_node_id,
                                         .transfer_id    = rx->metadata.transfer_id};

    const UAVCANRegister_t* reg;
    SpecialRegisterType     regtype;
    bool                    use_lock;
    bool                    execute_hook_and_release_lock = false;

    {
        uavcan_register_Access_Request_1_0 request;
        result = _proxy_access_request_deserialize_(&request, rx->payload, &rx->payload_size);
        ASSERT(result == NUNAVUT_SUCCESS);
        vPortFree(rx->payload);
        vPortFree(rx);

        // By inserting a NULL character at the end of the uint8_t array, request.name can be interpreted as a
        // null-terminated string. The maximum length of uavcan.register.Name.1.0 is 255, and since the
        // uavcan_register_Name_1_0 struct is not packed, there will be a padding byte before the .count member.
        // Therefore it is safe to overwrite .elements[.count], even with a full-length name.
        ASSERT(&request.name.name.elements[request.name.name.count] < (uint8_t*) &request.name.name.count);
        request.name.name.elements[request.name.name.count] = '\0';

        reg = get_base_register_by_name((const char*) &request.name, &regtype);
        // reg will be NULL if the requested register does not exist

        use_lock = reg && reg->lock && *reg->lock;

        if (reg && regtype == eRegisterValue && reg->mutable && request.value._tag_ != eUAVCANRegisterTypeEmpty &&
            request.value._tag_ == reg->type && request.value.unstructured.value.count == reg->len)
        {
            // TODO: check min/max constraints
            // write value
            ASSERT(reg->value);
            if (use_lock) xSemaphoreTake(*reg->lock, portMAX_DELAY);
            memcpy(reg->value, &request.value, reg->len * ValueTypeSize[reg->type]);
            // Defer executing hook until the stack-heavy request object is removed from the stack.
            // That way, the application-specific update_hook function has more stack to work with.
            execute_hook_and_release_lock = true;
        }
    }

    if (execute_hook_and_release_lock) {
        // Here an arbitrary function can be called, which will run on this task's stack.
        // If the hook function should be too stack-heavy, it might cause this task's stack to overflow.
        // It is up to the application developer to ensure the function fits into the available stack.
        // TODO: determine the available stack space at this point
        if (reg->update_hook) reg->update_hook();
        if (use_lock) xSemaphoreGive(*reg->lock);
    }

    size_t   size = uavcan_register_Access_Response_1_0_SERIALIZATION_BUFFER_SIZE_BYTES_;
    uint8_t* buf  = pvPortMalloc(size);
    ASSERT(buf);

    {
        uavcan_register_Access_Response_1_0 response;
        response.timestamp.microsecond = get_synchronized_timestamp_us();
        if (reg) {
            if (regtype == eRegisterValue) {
                response._mutable   = reg->mutable;
                response.persistent = reg->persistent;
            } else {
                response._mutable   = false;
                response.persistent = true;
            }
            const size_t nbytes = reg->len * ValueTypeSize[reg->type];
            switch (regtype) {
                case eRegisterValue:
                    ASSERT(reg->value);
                    if (use_lock) xSemaphoreTake(*reg->lock, portMAX_DELAY);
                    memcpy(&response.value, reg->value, nbytes);
                    if (use_lock) xSemaphoreGive(*reg->lock);
                    break;
                case eRegisterMin:
                    ASSERT(reg->min);
                    memcpy(&response.value, reg->min, nbytes);
                    break;
                case eRegisterDefault:
                    ASSERT(reg->dflt);
                    memcpy(&response.value, reg->dflt, nbytes);
                    break;
                case eRegisterMax:
                    ASSERT(reg->max);
                    memcpy(&response.value, reg->max, nbytes);
                    break;
                default:
                    WTF();
            }
            // regardless of the underlying type, the .count property is always at (&item->value + 256B).
            response.value.unstructured.value.count = reg->len;
            response.value._tag_                    = reg->type;
        } else {
            // requested register does not exist
            response.value._tag_ = eUAVCANRegisterTypeEmpty;
            response._mutable    = false;
            response.persistent  = false;
        }

        result = uavcan_register_Access_Response_1_0_serialize_(&response, buf, &size);
        ASSERT(result == NUNAVUT_SUCCESS);
    }

    node_publish(1000000ul, &meta, size, buf);
    LOG_INFO("register.Access request %u from %i handled", meta.transfer_id, meta.remote_node_id);
    vPortFree(buf);

    LOG_DEBUG("hwm=%lu", uxTaskGetStackHighWaterMark(NULL));
    vTaskDelete(NULL);
    WTF();
}
#endif
