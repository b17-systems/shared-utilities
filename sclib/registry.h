/**
 * @file registry.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief UAVCAN Named Register Interface
 * @version 0.1
 * @date 2021-12-24
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 * @details
 * The registry module is configurable by the following macros in <main.h>:
 *
 * | Macro Name                                       | type     | default         |
 * | ------------------------------------------------ | -------- | --------------- |
 * | configREGISTRY_LOG_LEVEL                         | severity | LOG_LEVEL_TRACE |
 * | configREGISTRY_INCLUDE_SERVICES                  | flag     | 1               |
 * | configREGISTRY_INCLUDE_LOAD_PERSISTENT_REGISTERS | flag     | 1               |
 * | configREGISTRY_INCLUDE_SAVE_PERSISTENT_REGISTERS | flag     | 1               |
 *
 */

#pragma once

#include <main.h>
#include <semphr.h>  // for lock (Mutex)
#include <uavcan/_register/Value_1_0.h>

#include "../canard/canard.h"
#include "../canard/node.h"

#ifndef configREGISTRY_LOG_LEVEL
#    define configREGISTRY_LOG_LEVEL LOG_LEVEL_TRACE
#endif

#ifndef configREGISTRY_INCLUDE_SERVICES
#    define configREGISTRY_INCLUDE_SERVICES 1
#endif

#ifndef configREGISTRY_INCLUDE_LOAD_PERSISTENT_REGISTERS
#    define configREGISTRY_INCLUDE_LOAD_PERSISTENT_REGISTERS 1
#endif

#ifndef configREGISTRY_INCLUDE_SAVE_PERSISTENT_REGISTERS
#    define configREGISTRY_INCLUDE_SAVE_PERSISTENT_REGISTERS 1
#endif

// ================================  Public API  ================================

static const uint16_t SUBJECT_ID_DEFAULT = UINT16_MAX;

/**
 * @brief Register Update Hook Function
 *
 * A named UAVCAN register may be assigned an update hook function, which (when defined) will be executed whenever the
 * value is written. This is the case both for initializing values from persistent storage (load_persistent_registers())
 * and for updating values via uavcan.register.Access.1.0.
 *
 * When updating via the UAVCAN register API, the update hook is executed AFTER the new value has been written and
 * BEFORE the response is sent (for which the value is read back). This enables the possibility to adjust the
 * synchronized value from inside the hook function before it is reported back to the client or otherwise used.
 *
 * If a lock is specified for the register, it will be held by the calling task throughout the hook function's lifetime.
 * This way, the lock can be used to guard against incompatible values, with the guarantee that the hook function is
 * executed after every external modification before a client task is allowed access to the value.
 *
 * This function is executed on the Access Server Task stack.
 *
 * @see load_persistent_registers, handle_register_access_request, UAVCANRegister_t
 */
typedef void (*HookFunction)(void);

/**
 * @brief UAVCAN Named Register Definition
 *
 * For automatic register discovery and handling, see UAVCANRegisterDeclaration.
 *
 * @see UAVCANRegisterDeclaration
 */
typedef struct UAVCANRegister {
    /**
     * Register name should contain only:
     *   - Lowercase ASCII alphanumeric characters (a-z, 0-9)
     *   - Full stop (.)
     *   - Low line (underscore) (_)
     * With the following limitations/recommendations:
     *   - The name shall not begin with a decimal digit (0-9).
     *   - The name shall neither begin nor end with a full stop.
     *   - A low line shall not be followed by a non-alphanumeric character.
     *   - The name should contain at least one full stop character.
     */
    const char* const name;

    /**
     * Register Data Type
     *
     * The data type shall be one specified in uavcan.register.Value.1.0, represented in UAVCANRegisterValueType_t.
     *
     * As of now, the following types are not implemented and thus are NOT TO BE USED:
     *   - uavcan.primitive.array.Bit.1.0
     *   - uavcan.primitive.array.Real16.1.0
     */
    uint8_t const type;

    /**
     * Register Mutability Flag
     *
     * Mutable means that the register can be written using the register.Access service.
     * Immutable registers cannot be written, but that doesn't imply that their values are constant (unchanging).
     */
    uint8_t const mutable : 1;

    /**
     * Register Persistency Flag
     *
     * Persistence means that the register retains its value permanently across power cycles or any other changes
     * in the state of the server, until it is explicitly overwritten (either via UAVCAN, or by the device itself).
     */
    uint8_t const persistent : 1;

    /**
     * Hidden Register Flag
     *
     * A hidden register will not be advertised by the uavcan.register.List service, but may still be accessed.
     * The client will need to know the register's name in advance to be able to read it, it cannot be dynamically
     * discovered.
     *
     * THIS IS AN EXTENSION TO THE OFFICIAL UAVCAN SPECIFICATION.
     */
    uint8_t const hidden : 1;

    /**
     * Constant Register Flag
     *
     * A constant register is expected to never change, and its value may reside in read-only memory. A constant
     * register's value therefore will not be saved to non-volatile memory, nor will it ever be written to.
     *
     * THIS IS AN EXTENSION TO THE OFFICIAL UAVCAN SPECIFICATION.
     */
    uint8_t const constant : 1;

    uint8_t const _pad : 4;

    /**
     * Register Dimensionality
     *
     * Number of elements of type {.type} in value array, NOT number of bytes.
     */
    uint16_t const len;

    /**
     * Minimum Value (optional)
     *
     * Pointer to configured minimum value(s) in ROM. Set NULL if not required.
     * The minimum values shall have the same data type and dimensionality as the base register.
     *
     * If configured, this value is accessible in the persistent, immutable register "base.register.name<".
     */
    const void* const min;

    /**
     * Maximum Value (optional)
     *
     * Pointer to configured maximum value(s) in ROM. Set NULL if not required.
     * The maximum values shall have the same data type and dimensionality as the base register.
     *
     * If configured, this value is accessible in the persistent, immutable register "base.register.name>".
     */
    const void* const max;

    /**
     * Default Value (optional)
     *
     * Pointer to configured default value(s) in ROM. Set NULL if not required.
     * The default values shall have the same data type and dimensionality as the base register.
     * Upon a factory reset command, all registers will be reset to their default value.
     *
     * If configured, this value is accessible in the persistent, immutable register "base.register.name=".
     */
    const void* const dflt;

    /**
     * Register Value
     *
     * Pointer to static variable in RAM holding the actual value.
     * It might be prudent to declare the variable as volatile.
     */
    void* const value;

    /**
     * Update Hook Function (optional)
     *
     * Pointer to a function which shall be executed on every value update. Set NULL if not required.
     * See HookFunction type declaration for details.
     */
    HookFunction const update_hook;

    /**
     * Value Access Mutex (optional)
     *
     * Pointer to a FreeRTOS Mutex, which will be acquired during value updates. Set NULL if not required.
     * The implementation guards against a non-initialized (NULL) Mutex handle, in which case the lock will be ignored.
     */
    SemaphoreHandle_t* const lock;
} UAVCANRegister_t;

/**
 * @brief UAVCANRegister Declaration
 *
 * This macro ensures the struct lands in the correct ROM section, which is essential for register discovery and
 * handling.
 */
#define UAVCANRegisterDeclaration const UAVCANRegister_t __attribute__((section(".uavcan_registers")))

/**
 * UAVCAN Register Data Type Options.
 *
 * This enum's members resemble the options in the uavcan.register.Value.1.0 union.
 */
typedef enum eUAVCANRegisterValueType {
    eUAVCANRegisterTypeEmpty          = 0,
    eUAVCANRegisterTypeString         = 1,
    eUAVCANRegisterTypeUnstructured   = 2,
    eUAVCANRegisterTypeBitArray       = 3,  // NOT IMPLEMENTED, do not use
    eUAVCANRegisterTypeInteger64Array = 4,
    eUAVCANRegisterTypeInteger32Array = 5,
    eUAVCANRegisterTypeInteger16Array = 6,
    eUAVCANRegisterTypeInteger8Array  = 7,
    eUAVCANRegisterTypeNatural64Array = 8,
    eUAVCANRegisterTypeNatural32Array = 9,
    eUAVCANRegisterTypeNatural16Array = 10,
    eUAVCANRegisterTypeNatural8Array  = 11,
    eUAVCANRegisterTypeReal64Array    = 12,
    eUAVCANRegisterTypeReal32Array    = 13,
    eUAVCANRegisterTypeReal16Array    = 14,  // NOT IMPLEMENTED, do not use
} UAVCANRegisterValueType_t;

/**
 * @brief Get a named UAVCAN register struct by name
 *
 * @param name (base) name of the register in question
 * @return a pointer to the register structure in ROM (.uavcan_registers section), or NULL if not found
 */
const UAVCANRegister_t* get_register_by_name(const char* const name);

/**
 * @brief Initialize Persistent Registers' Value Variables with Stored Values
 *
 * @return true if a valid and readable NVM configuration block was found
 */
bool load_persistent_registers(void);

/**
 * @brief Commit Persistent Registers' Values to Non-Volatile Memory
 */
void save_persistent_registers(void);

#if configREGISTRY_INCLUDE_SERVICES
/**
 * @brief RxTransferHandler to serve uavcan.register.List.1.0 Requests
 *
 * @see RxTransferHandler
 */
CanardRxTransfer* handle_register_list_request(const CanardRxTransfer* transfer, const NodeSubscription* const sub);

/**
 * @brief RxTransferHandler to serve uavcan.register.Access.1.0 Requests
 *
 * @see RxTransferHandler
 */
CanardRxTransfer* handle_register_access_request(const CanardRxTransfer* transfer, const NodeSubscription* const sub);
#endif
