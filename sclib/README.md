# starcopter shared library `sclib`

This directory contains C headers and sources for library functions shared between different projects.

## Integration

_Note: this section assumes the repository being included as a submodule under `lib/util/`_

To include the source files in the build, they have to be added to the corresponding list in the Makefile.
Also, the repository base directory should be added to the list of C include locations:

```make
# C sources
C_SOURCES =  \
# [...]
$(wildcard lib/util/sclib/*.c) \
# [...]

# C includes
C_INCLUDES =  \
# [...]
-Ilib/util \
# [...]
```

To be able to use VSCode's syntax checker and code completion, the repository should be added to `c_cpp_properties.json`'s include path as well:

```json
"includePath": [
    # [...]
    "${workspaceFolder}/lib/util",
],
```

## Configuration

Certain platform-specific configuration has to be provided by the including project in `sclib-config.h` anywhere on the include path.
[`sclib-config.EXAMPLE.h`](sclib-config.EXAMPLE.h) may serve as a starting point.

### Serial Console

[`console.c`](console.c) implements a `_write()` function, which is required for the standard library's `printf()` to work.
Currently ony a UART-based console is supported; the correct HAL UART handle has to be specified with the `SCLIB__STDOUT_UART_HANDLE` configuration parameter.

By default the blocking `HAL_UART_Transmit()` transport will be used.
To utilize an asynchronous interrupt-based transport, the corresponding USART Interrupt has to be enabled (including HAL code generation),
and `SCLIB__UART_HAL_IT` has to be defined in `sclib-config.h`.
