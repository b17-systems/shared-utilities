/**
 * @file console.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Pure CMSIS Console (StdOut) Implementation for STM32
 * @version 0.1
 * @date 2021-06-17
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#pragma once

#include <stdint.h>

/**
 * @brief Initialize the UART Console
 *
 * This function configures the UART interface specified in the configSTDOUT_USART macro.
 *
 * It DOES NOT
 *    - enable the peripheral in the RCC->APBxENRy register
 *    - configure the peripheral's clock; this has to be done manually before and specified in the
 *      configSTDOUT_USART_CLOCK_RATE macro
 *    - configure the peripheral's output pins (AF mapping, output type, speed, etc.)
 *
 * @param baudrate baud rate to communicate with, optional. Set to zero to keep current configuration.
 *
 * @see console_irq_handler
 */
void console_init(uint32_t baudrate);

/**
 * @brief Flush the UART Console
 *
 * When this function is called from inside an ISR, the UART console interrupt (configSTDOUT_USART_IRQn) gets raised in
 * priority to one level above the running interrupt's. This is necessary to avoid a deadlock situation.
 *
 * This function blocks until the TX ring buffer is empty. It will block indefinitely if the UART interface is not
 * transmitting for any reason (not initialized, interrupt not enabled, etc.).
 */
void console_flush(void);

/**
 * @brief Directly print to UART Console
 *
 * This is basically like printf(), only without formatting.
 */
void stdout_puts(char* buf, int count);

/**
 * @brief Console Interrupt Handler Implementation
 *
 * This function implements the interrupt-triggered transmit logic to be executed in the USARTx_IRQHandler.
 *
 * @example
 *
 *      void SystemInit(void) {
 *          // [...]
 *          console_init(115200);
 *      }
 *
 *      // configSTDOUT_USART is defined as USART2
 *      void USART2_IRQHandler(void) {
 *          console_irq_handler();
 *      }
 *
 * @see console_init
 */
void console_irq_handler(void);
