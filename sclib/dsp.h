/**
 * @file dsp.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Digital Signal Processing Tools
 * @version 0.1
 * @date 2022-10-03
 *
 * @copyright Copyright (c) 2022 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 */

#pragma once

#include <main.h>
#include <math.h>

/**
 * @brief Biquad (or Second Order Sections) Filter Stage Coefficients
 *
 * The coefficients in this struct form a second-order IIR filter, implemented by the following equation:
 *     y[n] = (b0 * x[n] + b1 * x[n-1] + b2 * x[n-2] - a1 * y[n-1] - a2 * y[n-2]) / a0
 *
 * This struct (and the SOS filter implementation) is intended to be used alongside the `scipy.signal` module,
 * which provides the necessary filter design routines.
 *
 * @example third order low pass filter with cutoff frequency at 20 Hz, sampled at 100 Hz
 *
 * The filter coefficients are calculated using the scipy.signal function `iirfilter()`:
 *
 *     >>> from scipy import signal
 *     >>> signal.iirfilter(3, 20, btype="lowpass", output="sos", fs=100)
 *         array([[ 0.09853116,  0.19706232,  0.09853116,  1.        , -0.15838444,  0.        ],
 *                [ 1.        ,  1.        ,  0.        ,  1.        , -0.41885608,  0.35544676]])
 *
 * These values correlate directly to BiquadStage coefficients:
 *
 *     const BiquadStage coeffs[] = {
 *         { 0.09853116,  0.19706232,  0.09853116,  1.        , -0.15838444,  0.        },
 *         { 1.        ,  1.        ,  0.        ,  1.        , -0.41885608,  0.35544676},
 *     };
 *
 * @note the parameter a0 is usually normalized to 1, but this does not have to be the case
 */
typedef struct BiquadStage {
    float b0, b1, b2;
    float a0, a1, a2;
} BiquadStage;

/**
 * @brief Biquad Cascade Filter Instance
 *
 * @see BiquadFilterHandle_t, init_sos_filter
 */
struct BiquadFilterCascade {
    size_t             n_sections;  // number of sections (filter order divided by two, rounded up)
    const BiquadStage* coeffs;      // pointer to array of `n_sections` biquad stages
    float              _state[];    // `2 * (n_sections + 1)` elements, see struct BiquadSectionIOState
};

/**
 * @brief Biquad Cascade Filter.
 *
 * A filter instance has to be initialized by `init_sos_filter()` before it can be used.
 *
 * @see init_sos_filter
 */
typedef struct BiquadFilterCascade* BiquadFilterHandle_t;

/**
 * @brief IIR Filter Instance (Numerator/Denominator Form).
 *
 * @see FilterHandle_t, init_ba_filter
 */
struct BAFilter {
    size_t       n;         // number of numerator polynomial coefficients
    size_t       m;         // number of denominator polynomial coefficients
    const float* coeffs;    // external array holding the coefficients ({b[0], ..., b[n-1], a[0], ..., a[m-1]})
    float        _state[];  // internal state array ({x[0], x[1], ..., x[n-1], y[0], y[1], ..., y[m-1]})
};

/**
 * @brief IIR Filter in Numerator/Denominator ('ba') Form.
 *
 * An instance of this filter should not be created directly, but rather through the function `init_ba_filter()`.
 *
 * @see init_ba_filter
 */
typedef struct BAFilter* FilterHandle_t;

/**
 * @brief Initialize a Biquad (or Second Order Sections) Filter Cascade.
 *
 * This function allocates (!) and initializes a biquad filter instance. A filter instance is defined by its individual
 * biquad filter stages (sets of coefficients), and optionally an initial internal state.
 *
 * The array of coefficient structs is not copied into the filter instance, it is only referenced by the filter.
 * On each filter update, the coefficients are read from the memory location referenced by `coeffs`.
 * This has two important implications:
 *
 * 1. The filter may be updated in-place, enabling adaptive filtering. In that case, however, care must be taken that
 *    the filter's internal state does not mess with the new coefficients.
 * 2. The coefficients must stay in scope. They may reside on the stack, on the heap or in constant memory, as long as
 *    they stay valid and accessible throughout the filter's lifetime.
 *
 * If the initial conditions are known, the filter can be initialized with a specific internal state `zi`. Refer to
 * `scipy.signal.sosfilt_zi()` for details on the initial state. If omitted, all initial states are initialized to zero.
 *
 * @param coeffs array of BiquadStage coefficient structs; referenced to, HAS TO STAY IN SCOPE
 * @param zi initial internal state, optional; set to NULL if unused
 * @return BiquadFilterHandle_t a handle for the created filter instance
 * @see BiquadStage, sosfilt, sosfilt_update
 */
#define init_sos_filter(coeffs, zi) _init_sos_filter(coeffs, sizeof(coeffs) / sizeof(BiquadStage), zi)
BiquadFilterHandle_t _init_sos_filter(const BiquadStage* const coeffs, size_t const n_sections, const float* const zi);

/**
 * @brief Process an input sample with an existing filter instance.
 *
 * The input sample `x` will propagate through the filter, with the result being available in the filter's output stage.
 * Use get_sos_filter_output() to retrieve the filter's result, or use the sosfilt() shorthand function.
 *
 * When `x` is non-finite (NaN or ±Inf), it will be dropped in favor of the previous input sample. This is known as the
 * "Last Observation Carried Forward" (LOCF) method to handle missing data.
 *
 * This function is safe to be called with an uninitialized filter instance. In that case, it will do exactly nothing.
 *
 * @param filter handle of the filter instance to process this sample
 * @param x input sample into the filter
 */
void sosfilt_update(BiquadFilterHandle_t filter, float const x);

/**
 * @brief Process an input sample with an existing filter instance, returning the result.
 *
 * This function calls sosfilt_update() internally.
 *
 * @param filter handle of the filter instance to process this sample
 * @param x input sample into the filter
 * @return float filter operation's result
 * @see sosfilt_update
 */
float sosfilt(BiquadFilterHandle_t filter, float const x);

/**
 * @brief Retrieve filter's last input sample
 * @param filter handle of the filter instance to get the last input sample from
 * @return last input sample
 */
static inline float get_sos_filter_input(const BiquadFilterHandle_t filter) { return filter->_state[0]; }

/**
 * @brief Retrieve filter's last result
 * @param filter handle of the filter instance to get the last result from
 * @note The filter has to be initialized, otherwise calling this function will result in a memory access violation!
 * @return last filter operation's output
 */
static inline float get_sos_filter_output_unsafe(const BiquadFilterHandle_t filter) {
    return filter->_state[2 * filter->n_sections];
}

/**
 * @brief Retrieve filter's last result
 * @param maybe_filter handle of the filter instance to get the last result from, may be uninitialized (NULL)
 * @return last filter operation's output, or NaN if not initialized
 */
static inline float get_sos_filter_output(const BiquadFilterHandle_t maybe_filter) {
    return maybe_filter ? get_sos_filter_output_unsafe(maybe_filter) : NAN;
}

/**
 * @brief Initialize a Digital Filter (FIR/IIR) in Numerator/Denominator ('ba') Form.
 *
 * This function allocates (!) and initializes a digital filter, implemented in direct form I. It is defined by its set
 * of numerator coefficients b[0]..b[n-1] and denominator coefficients a[0]..a[m-1], which have to be provided by
 * reference through `coeffs`.
 *
 * The coefficient array `coeffs` is expected to contain the `b` coefficients, directly followed by the `a` values, both
 * in ascending order: `{b[0], b[1], ..., b[n-1], a[0], a[1], ..., a[m-1]}`. These coefficients are not copied into the
 * filter instance, but always read from the location referenced by `coeffs`. This has two major implications:
 *
 * 1. The filter may be updated in-place, enabling adaptive filtering. Since the filter is implemented in direct form I,
 *    it contains no internal state other than the history of input and output values, which makes it more suitable for
 *    online coefficient updates. The number of numerator and denominator coefficients has to stay constant, but unused
 *    parameters may well be set zero.
 * 2. The coefficients must stay in scope. They may reside on the stack, on the heap or in constant memory, as long as
 *    they stay valid and accessible throughout the filter's lifetime.
 *
 * The filter has to have at least one numerator coefficient and one denominator coefficient, e.g. at least `b0` and
 * `a0` must exist. Additionally, the `a0` parameter must not be zero, since the output will be scaled by `1/a0`.
 *
 * To implement an FIR filter, simply only specify numerator coefficients and set `a0` to 1.
 *
 * @param coeffs array of filter coefficients, e.g. `{b[0], b[1], ..., b[n-1], a[0], a[1], ..., a[m-1]}`
 * @param n_b number of numerator coefficients
 * @param m_a number of denominator coefficients
 * @return FilterHandle_t a handle for the created filter instance
 * @see lfilter, lfilter_update
 */
FilterHandle_t init_ba_filter(const float* const coeffs, size_t const n_b, size_t const m_a);

/**
 * @brief Process an input sample with an existing filter instance.
 *
 * The input sample `x` will propagate through the filter, with the result being available in the filter's output stage.
 * Use get_ba_filter_output() to retrieve the filter's result, or use the lfilter() shorthand function.
 *
 * When `x` is non-finite (NaN or ±Inf), it will be dropped in favor of the previous input sample. This is known as the
 * "Last Observation Carried Forward" (LOCF) method to handle missing data.
 *
 * This function is safe to be called with an uninitialized filter instance. In that case, it will do exactly nothing.
 *
 * @param filter handle of the filter instance to process this sample
 * @param x input sample into the filter
 */
void lfilter_update(FilterHandle_t filter, float const x);

/**
 * @brief Process an input sample with an existing filter instance, returning the result.
 *
 * This function calls lfilter_update() internally.
 *
 * @param filter handle of the filter instance to process this sample
 * @param x input sample into the filter
 * @return float filter operation's result
 * @see lfilter_update
 */
float lfilter(FilterHandle_t filter, float const x);

/**
 * @brief Retrieve filter's last input sample
 * @param filter handle of the filter instance to get the last input sample from
 * @return last input sample
 */
static inline float get_ba_filter_input(const FilterHandle_t filter) { return filter->_state[0]; }

/**
 * @brief Retrieve filter's last result
 * @param filter handle of the filter instance to get the last result from
 * @note The filter has to be initialized, otherwise calling this function will result in a memory access violation!
 * @return last filter operation's output
 */
static inline float get_ba_filter_output_unsafe(const FilterHandle_t filter) { return filter->_state[filter->n]; }

/**
 * @brief Retrieve filter's last result
 * @param maybe_filter handle of the filter instance to get the last result from, may be uninitialized (NULL)
 * @return last filter operation's output, or NaN if not initialized
 */
static inline float get_ba_filter_output(const FilterHandle_t maybe_filter) {
    return maybe_filter ? get_ba_filter_output_unsafe(maybe_filter) : NAN;
}
