/**
 * @file image_desc_registers.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief
 * @version 0.1
 * @date 2022-01-06
 *
 * @copyright Copyright (c) 2022 starcopter GmbH
 *
 */

#include <main.h>
#include "image.h"
#include "registry.h"

extern const ELFNote_t g_note_build_id;

UAVCANRegisterDeclaration _reg_build_id = {  // com.starcopter.gnu_build_id
    .name        = "com.starcopter.gnu_build_id",
    .type        = eUAVCANRegisterTypeNatural8Array,
    .mutable     = 0,
    .persistent  = 1,
    .constant    = 1,
    .len         = 16,
    .min         = NULL,
    .max         = NULL,
    .dflt        = NULL,
    .value       = (void*) &g_note_build_id.data[4],
    .update_hook = NULL,
    .lock        = NULL};

UAVCANRegisterDeclaration _reg_git_sha = {  // com.starcopter.git_sha
    .name        = "com.starcopter.git_sha",
    .type        = eUAVCANRegisterTypeNatural8Array,
    .mutable     = 0,
    .persistent  = 1,
    .constant    = 1,
    .len         = 16,
    .min         = NULL,
    .max         = NULL,
    .dflt        = NULL,
    .value       = (void*) &image_hdr.git_sha,
    .update_hook = NULL,
    .lock        = NULL};

UAVCANRegisterDeclaration _reg_stm_uid = {  // com.starcopter.stm32_uid
    .name        = "com.starcopter.stm32_uid",
    .type        = eUAVCANRegisterTypeNatural32Array,
    .mutable     = 0,
    .persistent  = 1,
    .constant    = 1,
    .len         = 3,
    .min         = NULL,
    .max         = NULL,
    .dflt        = NULL,
    .value       = (void*) UID_BASE,
    .update_hook = NULL,
    .lock        = NULL};
