/**
 * @file log.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief UAVCAN- and FreeRTOS-Aware Logging Library
 * @version 0.2
 *
 * @copyright Copyright (c) 2022 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 * This library provides logging macros for each of the severity levels defined in uavcan.diagnostic.Severity.1.0.
 *
 * By defining the macro `LOG_LEVEL` **before** importing this header file, all log macro invocations for levels
 * below LOG_LEVEL will be excluded from the compilation. A typical use case would look like this:
 *
 *     #define LOG_LEVEL LOG_LEVEL_NOTICE
 *     #include <sclib/log.h>
 *
 *     // longAndComplicatedFunction() will never be called, since the DEBUG level is disabled for this file
 *     LOG_DEBUG("detailled information: %ul", longAndComplicatedFunction());
 *
 *     // This call will be forwarded to the log handling function, since NOTICE and above is enabled for this file
 *     LOG_NOTICE("Something noteworthy happened.")
 *
 * The file-local LOG_LEVEL can be globally overridden by `configLOG_LEVEL_OVERRIDE`, e.g. from the command line by
 * passing a flag `-DconfigLOG_LEVEL_OVERRIDE=LOG_LEVEL_ERROR` to the compiler invocation. Likewise, a global minimum
 * log level (maximum verbosity) can be specified by the `configLOG_LEVEL_MIN` macro.
 *
 * If neither LOG_LEVEL nor configLOG_LEVEL_OVERRIDE are defined, LOG_LEVEL defaults to LOG_LEVEL_WARNING as recommended
 * in the UAVCAN standard.
 *
 * Each output channel (UART console, UAVCAN) can be masked out independently at compile time on a per-file-basis by
 * defining the macros configLOG_MASK_UART and configLOG_MASK_UAVCAN at the top of the file, respectively. This is
 * useful to remove the potential of recursive logging loops, e.g. logging about transmitting a log record on the bus.
 * Each output channel may further be filtered at runtime by the "log_level.console" and "log_level.uavcan" registers.
 *
 */

#pragma once

// These definitions mirror the convention in uavcan.diagnostic.Severity.1.0
#define LOG_LEVEL_TRACE    0u
#define LOG_LEVEL_DEBUG    1u
#define LOG_LEVEL_INFO     2u
#define LOG_LEVEL_NOTICE   3u
#define LOG_LEVEL_WARNING  4u
#define LOG_LEVEL_ERROR    5u
#define LOG_LEVEL_CRITICAL 6u
#define LOG_LEVEL_ALERT    7u

#if !defined configLOG_LEVEL_DEFAULT
#    define configLOG_LEVEL_DEFAULT LOG_LEVEL_WARNING
#endif
#if defined configLOG_LEVEL_OVERRIDE
#    undef LOG_LEVEL
#    define LOG_LEVEL configLOG_LEVEL_OVERRIDE
#elif !defined LOG_LEVEL
#    define LOG_LEVEL configLOG_LEVEL_DEFAULT
#endif

#if !defined configLOG_LEVEL_MIN
#    define configLOG_LEVEL_MIN LOG_LEVEL_TRACE
#endif

#define _LOG_ENABLE_UART   1u
#define _LOG_ENABLE_UAVCAN 2u

#if !defined configLOG_MASK_UART
#    define _LOG_UART _LOG_ENABLE_UART
#else
#    define _LOG_UART 0u
#endif

#if !defined configLOG_MASK_UAVCAN
#    define _LOG_UAVCAN _LOG_ENABLE_UAVCAN
#else
#    define _LOG_UAVCAN 0u
#endif

#define _LOG_ENABLED_HANDLERS (_LOG_UART | _LOG_UAVCAN)

/**
 * @brief Internal Log Message Handler Function
 *
 * @param log_level severity of the log message
 * @param enabled_handlers bit mask of enabled logging handlers
 * @param format_string printf-compatible format string; trailing line break NOT REQUIRED
 * @param ... arguments for printf(), if any
 */
void _emit_log(const uint_fast8_t log_level, const uint_fast8_t enabled_handlers, const char* format_string, ...)
    __attribute__((format(printf, 3, 4)));

#if (LOG_LEVEL <= LOG_LEVEL_TRACE) && (configLOG_LEVEL_MIN <= LOG_LEVEL_TRACE)
/**
 * @brief Log a Message with TRACE severity.
 *
 * Messages of this severity can be used only during development.
 * They shall not be used in a fielded operational system.
 */
#    define LOG_TRACE(...) _emit_log(LOG_LEVEL_TRACE, _LOG_ENABLED_HANDLERS, __VA_ARGS__)
#else
/**
 * @brief Log a Message with TRACE severity.
 *
 * Messages of this severity can be used only during development.
 * They shall not be used in a fielded operational system.
 *
 * This log level is suppressed for the current file.
 */
#    define LOG_TRACE(...) ((void) 0U)
#endif

#if (LOG_LEVEL <= LOG_LEVEL_DEBUG) && (configLOG_LEVEL_MIN <= LOG_LEVEL_DEBUG)
/**
 * @brief Log a Message with DEBUG severity.
 *
 * Messages that can aid in troubleshooting.
 * Messages of this severity and lower should be disabled by default.
 */
#    define LOG_DEBUG(...) _emit_log(LOG_LEVEL_DEBUG, _LOG_ENABLED_HANDLERS, __VA_ARGS__)
#else
/**
 * @brief Log a Message with DEBUG severity.
 *
 * Messages that can aid in troubleshooting.
 * Messages of this severity and lower should be disabled by default.
 *
 * This log level is suppressed for the current file.
 */
#    define LOG_DEBUG(...) ((void) 0U)
#endif

#if (LOG_LEVEL <= LOG_LEVEL_INFO) && (configLOG_LEVEL_MIN <= LOG_LEVEL_INFO)
/**
 * @brief Log a Message with INFO severity.
 *
 * General informational messages of low importance.
 * Messages of this severity and lower should be disabled by default.
 */
#    define LOG_INFO(...) _emit_log(LOG_LEVEL_INFO, _LOG_ENABLED_HANDLERS, __VA_ARGS__)
#else
/**
 * @brief Log a Message with INFO severity.
 *
 * General informational messages of low importance.
 * Messages of this severity and lower should be disabled by default.
 *
 * This log level is suppressed for the current file.
 */
#    define LOG_INFO(...) ((void) 0U)
#endif

#if (LOG_LEVEL <= LOG_LEVEL_NOTICE) && (configLOG_LEVEL_MIN <= LOG_LEVEL_NOTICE)
/**
 * @brief Log a Message with NOTICE severity.
 *
 * General informational messages of high importance.
 * Messages of this severity and lower should be disabled by default.
 */
#    define LOG_NOTICE(...) _emit_log(LOG_LEVEL_NOTICE, _LOG_ENABLED_HANDLERS, __VA_ARGS__)
#else
/**
 * @brief Log a Message with NOTICE severity.
 *
 * General informational messages of high importance.
 * Messages of this severity and lower should be disabled by default.
 *
 * This log level is suppressed for the current file.
 */
#    define LOG_NOTICE(...) ((void) 0U)
#endif

#if (LOG_LEVEL <= LOG_LEVEL_WARNING) && (configLOG_LEVEL_MIN <= LOG_LEVEL_WARNING)
/**
 * @brief Log a Message with WARNING severity.
 *
 * Messages reporting abnormalities and warning conditions.
 * Messages of this severity and higher should be enabled by default.
 */
#    define LOG_WARNING(...) _emit_log(LOG_LEVEL_WARNING, _LOG_ENABLED_HANDLERS, __VA_ARGS__)
#else
/**
 * @brief Log a Message with WARNING severity.
 *
 * Messages reporting abnormalities and warning conditions.
 * Messages of this severity and higher should be enabled by default.
 *
 * This log level is suppressed for the current file.
 */
#    define LOG_WARNING(...) ((void) 0U)
#endif

#if (LOG_LEVEL <= LOG_LEVEL_ERROR) && (configLOG_LEVEL_MIN <= LOG_LEVEL_ERROR)
/**
 * @brief Log a Message with ERROR severity.
 *
 * Messages reporting problems and error conditions.
 * Messages of this severity and higher should be enabled by default.
 */
#    define LOG_ERROR(...) _emit_log(LOG_LEVEL_ERROR, _LOG_ENABLED_HANDLERS, __VA_ARGS__)
#else
/**
 * @brief Log a Message with ERROR severity.
 *
 * Messages reporting problems and error conditions.
 * Messages of this severity and higher should be enabled by default.
 *
 * This log level is suppressed for the current file.
 */
#    define LOG_ERROR(...) ((void) 0U)
#endif

/**
 * @brief Log a Message with CRITICAL severity.
 *
 * Messages reporting serious problems and critical conditions.
 * Messages of this severity and higher should be always enabled.
 */
#define LOG_CRITICAL(...) _emit_log(LOG_LEVEL_CRITICAL, _LOG_ENABLED_HANDLERS, __VA_ARGS__)

/**
 * @brief Log a Message with ALERT severity.
 *
 * Notifications of dangerous circumstances that demand immediate attention.
 * Messages of this severity should be always enabled.
 */
#define LOG_ALERT(...) _emit_log(LOG_LEVEL_ALERT, _LOG_ENABLED_HANDLERS, __VA_ARGS__)
