/**
 * @file dsp.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Digital Signal Processing Tools
 * @version 0.1
 * @date 2022-10-03
 *
 * @copyright Copyright (c) 2022 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include "dsp.h"
#include <string.h>

/**
 * @brief View to a single biquad section's input-output state.
 *
 * This structure resembles a view on the BiquadFilterCascade::_state[] array: since one section's output is the
 * next section's input, this view is shifted along the state array like depicted below.
 *
 *     state index |  0  1  2  3  4  5  6  7
 *     section 0   | x0 x1 y0 y1
 *     section 1   |       x0 x1 y0 y1
 *     section 2   |             x0 x1 y0 y1
 *
 * The values x2 and y2 are only needed momentarily during the calculation, and are not stored in the filter's state.
 */
struct BiquadSectionIOState {
    const float x0, x1;
    float       y0, y1;
};

BiquadFilterHandle_t _init_sos_filter(const BiquadStage* const coeffs, size_t const n_sections, const float* const zi) {
    ASSERT(coeffs);
    ASSERT(n_sections);
    const size_t         state_length = 2 * (n_sections + 1);
    BiquadFilterHandle_t filter       = pvPortMalloc(sizeof(struct BiquadFilterCascade) + state_length * sizeof(float));
    ASSERT(filter);
    filter->n_sections = n_sections;
    filter->coeffs     = coeffs;
    if (zi) {
        memcpy(filter->_state, zi, state_length * sizeof(float));
    } else {
        for (size_t i = 0; i < state_length; ++i)
            filter->_state[i] = 0.0f;
    }
    return filter;
}

void __attribute__((optimize("-O3"))) sosfilt_update(BiquadFilterHandle_t filter, float const x) {
    float x2, y2;

    if (!filter) return;
    ASSERT(filter);
    ASSERT(filter->n_sections > 0);
    ASSERT(filter->coeffs);

    // shift input into state array
    x2                = filter->_state[1];
    filter->_state[1] = filter->_state[0];
    // LOCF: don't allow non-finite values into state, carry the last observation forward instead
    if (isfinite(x)) filter->_state[0] = x;

    for (size_t i = 0; i < filter->n_sections; ++i) {
        const BiquadStage* const           c = &filter->coeffs[i];
        struct BiquadSectionIOState* const z = (struct BiquadSectionIOState*) &filter->_state[2 * i];

        y2    = z->y1;
        z->y1 = z->y0;
        z->y0 = (c->b0 * z->x0 + c->b1 * z->x1 + c->b2 * x2 - c->a1 * z->y1 - c->a2 * y2) / c->a0;

        x2 = y2;
    }
}

float sosfilt(BiquadFilterHandle_t filter, float const x) {
    sosfilt_update(filter, x);
    return get_sos_filter_output(filter);
}

FilterHandle_t init_ba_filter(const float* const coeffs, size_t const n, size_t const m) {
    ASSERT(coeffs);
    ASSERT(n);
    ASSERT(m);

    const size_t   state_length = n + m;
    FilterHandle_t filter       = pvPortMalloc(sizeof(struct BAFilter) + state_length * sizeof(float));
    ASSERT(filter);
    filter->n      = n;
    filter->m      = m;
    filter->coeffs = coeffs;
    for (size_t i = 0; i < state_length; ++i)
        filter->_state[i] = 0.0f;
    return filter;
}

void __attribute__((optimize("-O3"))) lfilter_update(FilterHandle_t filter, float const x) {
    if (!filter) return;
    ASSERT(filter);
    ASSERT(filter->n > 0);
    ASSERT(filter->m > 0);
    ASSERT(filter->coeffs);

    const float* const b0  = &filter->coeffs[0];                          // b[0]
    const float* const bn1 = &filter->coeffs[filter->n - 1];              // b[n-1]
    const float* const a0  = &filter->coeffs[filter->n];                  // a[0]
    const float* const am1 = &filter->coeffs[filter->n + filter->m - 1];  // a[m-1]
    float* const       x0  = &filter->_state[0];                          // x[0]
    float* const       y0  = &filter->_state[filter->n];                  // y[0]

    // shift state array
    for (size_t i = filter->n + filter->m - 1; i > 0; --i) {
        filter->_state[i] = filter->_state[i - 1];
    }
    // LOCF: don't allow non-finite values into state, carry the last observation forward instead
    if (isfinite(x)) *x0 = x;

    float acc = 0;

    // for i in 0:n-1
    const float* bi = b0;
    float*       xi = x0;
    while (bi <= bn1)
        acc += *xi++ * *bi++;

    ASSERT(bi == a0);
    ASSERT(xi == y0);

    // for i in 1:m-1
    const float* ai = a0 + 1;
    float*       yi = y0 + 1;
    while (ai <= am1)
        acc -= *yi++ * *ai++;

    ASSERT(*a0 != 0);
    *y0 = acc / *a0;
}

float lfilter(FilterHandle_t filter, float const x) {
    lfilter_update(filter, x);
    return get_ba_filter_output(filter);
}
