/**
 * @file image.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief
 * @version 0.1
 * @date 2021-06-11
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include <stddef.h>
#include <stdio.h>
#include "device.h"

#include "image.h"
#include "crc.h"
#include "shared_ram.h"
#include "memory_map.h"

extern void _switch_app_context(ApplicationContext_t* new_context);

bool image_check(const ImageHeader_t* const image, bool check_header, bool check_body) {
    if (image == NULL || image->image_magic != IMAGE_MAGIC) return false;

    if (check_header) {
        uint32_t header_crc = *(uint32_t*) ((uint32_t) image + image->header_size - sizeof(uint32_t));
        if (!crc32((uint32_t*) image, image->header_size - sizeof(uint32_t), &header_crc)) return false;
    }

    if (check_body) {
        uint32_t       image_crc  = *(uint32_t*) ((uint32_t) image->image_end - sizeof(uint32_t));
        uint32_t*      data_start = (uint32_t*) ((uint32_t) image + image->header_size);  // deprecated
        uint32_t       data_crc   = image_crc;                                            // deprecated
        const uint32_t image_size = (uint32_t) image->image_end - (uint32_t) image;
        if (!crc32((uint32_t*) image, image_size - sizeof(uint32_t), &image_crc) &&
            !crc32(data_start, image_size - image->header_size - sizeof(uint32_t), &data_crc) /* legacy support */)
        {
            return false;
        }
    }

    return true;
}

int image_render_version_string(char* out_version_string, const SWVersion_t* const version, const bool clean_build) {
    if (version->commits == 0 && clean_build)
        return sprintf(out_version_string, "v%u.%u.%u", version->major, version->minor, version->patch);
    return sprintf(out_version_string, "v%u.%u.%u-%u%s", version->major, version->minor, version->patch,
                   version->commits, clean_build ? "" : "*");
}

__attribute__((naked, noreturn)) void launch_image(const ImageHeader_t* const image) {
    (void) image;
    __asm("\
        ldr     r1, [r0, #8]   /* load vector table address into r1 */ \n\
        ldr     r2, [r1, #0]   /* load stack pointer from vector table into r2 */ \n\
        ldr     r3, [r1, #4]   /* load entry address from vector table into r3 */ \n\
        orr.w   r3, r3, #1     /* set LSB in r3 (PC) */ \n\
        msr     MSP, r2        /* load r2 into MSP */ \n\
        mov     r0, #0         /* r0 = 0; ensure we're using the Main Stack Pointer (MSP) */ \n\
        msr     CONTROL, r0    /* load r0 into CONTROL */ \n\
        bx      r3             /* branch to address at r3 */ \n\
    ");
}

void __NO_RETURN image_boot(ApplicationContext_t* const ctx) {
    ctx->boot_count++;
    _switch_app_context(ctx);
    launch_image(ctx->application);
}
