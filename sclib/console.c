/**
 * @file console.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief GCC Low-Level I/O for stdio's printf()
 * @version 0.1
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include <main.h>
#include <device.h>

#include "console.h"
#include "registry.h"

#ifndef configSTDOUT_USART
#    error _write() and thus printf() will not work without a `USART_TypeDef* configSTDOUT_USART` defined
#endif

#ifndef configSTDOUT_USART_IRQn
#    error _write() and thus printf() will not work without a `IRQn_Type configSTDOUT_USART_IRQn` defined
#endif

#ifndef configSTDOUT_USART_ISR_PRIORITY
// lowest priority by default
#    define configSTDOUT_USART_ISR_PRIORITY (0xff)
#endif

#ifndef configSTDOUT_USART_CLOCK_RATE
#    define configSTDOUT_USART_CLOCK_RATE SystemCoreClock
#endif

#ifndef configSTDOUT_BUFFERSIZE
#    define configSTDOUT_BUFFERSIZE 82UL
#endif

static char        stdout_buf[configSTDOUT_BUFFERSIZE] = {0};
static char* const stdout_buf_start                    = &stdout_buf[0];
static char* const stdout_buf_end                      = &stdout_buf[configSTDOUT_BUFFERSIZE];
static char* volatile stdout_buf_write;
static char* volatile stdout_buf_read;

static bool           _initialized      = false;
static const uint32_t BAUD_RATE_MIN     = 9600;
static const uint32_t BAUD_RATE_MAX     = 5000000;
static const uint32_t BAUD_RATE_DEFAULT = 1000000;
static uint32_t       console_baud_rate = BAUD_RATE_DEFAULT;

void set_baud_rate(void) {
    if (!_initialized) return;
    CLEAR_BIT(configSTDOUT_USART->CR1, USART_CR1_UE);
    configSTDOUT_USART->BRR = (configSTDOUT_USART_CLOCK_RATE / console_baud_rate) & USART_BRR_BRR_Msk;
    SET_BIT(configSTDOUT_USART->CR1, USART_CR1_UE);
}

// console.baud_rate
UAVCANRegisterDeclaration _reg_console_baud_rate = {
    .name        = "console.baud_rate",
    .type        = eUAVCANRegisterTypeNatural32Array,
    .mutable     = 1,
    .persistent  = 1,
    .len         = 1,
    .min         = (const void*) &BAUD_RATE_MIN,
    .max         = (const void*) &BAUD_RATE_MAX,
    .dflt        = (const void*) &BAUD_RATE_DEFAULT,
    .value       = (void*) &console_baud_rate,
    .update_hook = set_baud_rate,
    .lock        = NULL,
};

/**
 * @brief Copy Message Content into the STDOUT ring buffer
 *
 * If the message length exceeds the space currently available in the buffer, it will be partially written.
 *
 * @param msg       message to be written
 * @param count     message length
 * @return uint32_t number of bytes (chars) written
 */
static uint32_t stdout_buf_puts(const char* msg, uint32_t count);

void console_init(uint32_t baudrate) {
    NVIC_DisableIRQ(configSTDOUT_USART_IRQn);
    stdout_buf_write = stdout_buf_start;
    stdout_buf_read  = stdout_buf_start;

    configSTDOUT_USART->PRESC = 0;
    configSTDOUT_USART->CR3   = 0;
    configSTDOUT_USART->CR2   = 0;
    configSTDOUT_USART->CR1   = USART_CR1_FIFOEN | USART_CR1_TE;  // enable FIFO and transmitter

    NVIC_SetPriority(configSTDOUT_USART_IRQn, configSTDOUT_USART_ISR_PRIORITY);
    NVIC_EnableIRQ(configSTDOUT_USART_IRQn);

    if (baudrate) console_baud_rate = baudrate;
    _initialized = true;
    set_baud_rate();

    stdout_buf_puts("\r\n\r\n", 4);
}

void console_flush(void) {
    if (!_initialized) return;
    const bool isr = RUNNING_INSIDE_ISR;
    if (isr) {
        const uint_fast8_t current_priority = NVIC_GetPriority(CURRENTLY_RUNNING_IRQn);
        // raise UART interrupt priority to avoid deadlock situation
        NVIC_SetPriority(configSTDOUT_USART_IRQn, current_priority > 0 ? current_priority - 1 : 0);
    }
    while (stdout_buf_read != stdout_buf_write) {
        // wait until no characters are left to write
    }
    while (!PERIPH_BIT(configSTDOUT_USART->ISR, USART_ISR_TXFE_Pos)) {
        // wait until TXFIFO is empty
    }
    if (isr) {
        // reset priority
        NVIC_SetPriority(configSTDOUT_USART_IRQn, configSTDOUT_USART_ISR_PRIORITY);
    }
}

void console_irq_handler(void) {
    // while TXFIFO not full and characters left to write
    while (PERIPH_BIT(configSTDOUT_USART->ISR, USART_ISR_TXE_TXFNF_Pos) && (stdout_buf_read != stdout_buf_write)) {
        configSTDOUT_USART->TDR = *stdout_buf_read++;
        if (stdout_buf_read == stdout_buf_end) stdout_buf_read = stdout_buf_start;
    }
    if (stdout_buf_read == stdout_buf_write) {
        // we're done for now, disable the TX FIFO empty interrupt
        PERIPH_BIT(configSTDOUT_USART->CR1, USART_CR1_TXFEIE_Pos) = 0;
    }
}

/**
 * @brief GCC Low-Level I/O for use with `printf()`
 *
 * Newlib's implementation of `printf()` calls this function to perform actual I/O.
 * See https://interrupt.memfault.com/blog/boostrapping-libc-with-newlib#write
 *
 * @param fd    file descriptor; currently only 1 for StdOut is implemented
 * @param buf   pointer to character buffer holding data to transmit
 * @param count number of characters to transmit
 * @return int  number of characters transmitted
 */
int _write(int fd, char* buf, int count) {
    if (!_initialized) return count;
    uint32_t sent = 0;
    if (fd == 1) {
        do {
            sent += stdout_buf_puts((char*) ((uint32_t) buf + sent), count - sent);
        } while (count - sent > 0);
    }
    return sent;
}

static uint32_t stdout_buf_puts(const char* msg, uint32_t count) {
    const char* const msg_start = msg;
    const char* const msg_end   = (const char* const) ((uint32_t) msg + count);

    while (msg < msg_end) {
        // break when only one byte left in buffer
        int capacity = (uint32_t) stdout_buf_read - (uint32_t) stdout_buf_write;
        if (capacity <= 0) capacity += configSTDOUT_BUFFERSIZE;
        if (capacity == 1) break;

        *stdout_buf_write++ = *msg++;

        if (stdout_buf_write == stdout_buf_end) stdout_buf_write = stdout_buf_start;
    }

    // enable the TX FIFO empty interrupt
    PERIPH_BIT(configSTDOUT_USART->CR1, USART_CR1_TXFEIE_Pos) = 1;
    return msg - msg_start;
}

void stdout_puts(char* buf, int count) { _write(1, buf, count); }
