/**
 * @file log.c
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief UAVCAN- and FreeRTOS-Aware Logging Library
 * @version 0.2
 *
 * @copyright Copyright (c) 2022 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#include <stdarg.h>
#include <stdio.h>
#include <main.h>

#ifdef INC_FREERTOS_H
#    include <task.h>
#endif

#include "log.h"
#include "assert.h"
#include "console.h"
#include "timekeeper.h"
#include "registry.h"
#include <canard/node.h>
#include <uavcan/diagnostic/Record_1_1.h>

static const uint8_t _LOG_LEVEL_MIN             = configLOG_LEVEL_MIN;
static const uint8_t _LOG_LEVEL_MAX             = LOG_LEVEL_ALERT;
static const uint8_t CONSOLE_LOG_LEVEL_DEFAULT  = LOG_LEVEL_TRACE;
static const uint8_t BUS_LOG_LEVEL_DEFAULT      = LOG_LEVEL_WARNING;
static uint8_t       log_level_console          = CONSOLE_LOG_LEVEL_DEFAULT;
static uint8_t       log_level_uavcan           = BUS_LOG_LEVEL_DEFAULT;
static uint8_t       previous_log_level_console = CONSOLE_LOG_LEVEL_DEFAULT;
static uint8_t       previous_log_level_uavcan  = BUS_LOG_LEVEL_DEFAULT;

void notify_console_log_level(void);
void notify_uavcan_log_level(void);

UAVCANRegisterDeclaration _reg_log_level_console = {  // log_level.console
    .name        = "log_level.console",
    .type        = eUAVCANRegisterTypeNatural8Array,
    .mutable     = 1,
    .persistent  = 1,
    .len         = 1,
    .min         = (const void*) &_LOG_LEVEL_MIN,
    .max         = (const void*) &_LOG_LEVEL_MAX,
    .dflt        = (const void*) &CONSOLE_LOG_LEVEL_DEFAULT,
    .value       = (void*) &log_level_console,
    .update_hook = notify_console_log_level,
    .lock        = NULL};

UAVCANRegisterDeclaration _reg_log_level_uavcan = {  // log_level.uavcan
    .name        = "log_level.uavcan",
    .type        = eUAVCANRegisterTypeNatural8Array,
    .mutable     = 1,
    .persistent  = 1,
    .len         = 1,
    .min         = (const void*) &_LOG_LEVEL_MIN,
    .max         = (const void*) &_LOG_LEVEL_MAX,
    .dflt        = (const void*) &BUS_LOG_LEVEL_DEFAULT,
    .value       = (void*) &log_level_uavcan,
    .update_hook = notify_uavcan_log_level,
    .lock        = NULL};

static const char* getLogLevelName(uint8_t log_level);

static void notify_log_level(const char* channel_name, uint8_t* previous, const uint8_t current) {
    if (*previous == current) return;

    _emit_log(current, _LOG_ENABLED_HANDLERS, "%s Log Level set to %s", channel_name, getLogLevelName(current));
    *previous = current;
}

void notify_console_log_level(void) { notify_log_level("Console", &previous_log_level_console, log_level_console); }

void notify_uavcan_log_level(void) { notify_log_level("Bus", &previous_log_level_uavcan, log_level_uavcan); }

static const char* getLogLevelName(uint8_t log_level) {
    /// as per uavcan.diagnostic.Severity.1.0
    const char*   LOG_LEVEL_NAMES[] = {"TRACE", "DEBUG", "INFO", "NOTICE", "WARNING", "ERROR", "CRITICAL", "ALERT"};
    const uint8_t MAX_LOG_LEVEL     = sizeof(LOG_LEVEL_NAMES) / sizeof(LOG_LEVEL_NAMES[0]);

    if (log_level >= MAX_LOG_LEVEL) log_level = MAX_LOG_LEVEL - 1;

    return LOG_LEVEL_NAMES[log_level];
}

void _emit_log(const uint_fast8_t log_level, const uint_fast8_t enabled_handlers, const char* format_string, ...) {
    if (RUNNING_INSIDE_ISR) return;
    if (log_level < log_level_console && log_level < log_level_uavcan) return;
    if (!enabled_handlers) return;

    // manual "serialized" representation of a uavcan.diagnostic.Record.1.1
    static struct __packed Record11 {
        union {
            uint64_t timestamp;
            uint8_t  header_bytes[8];
        };
        uint8_t len;
        char    message[256];
    } buffer;
    static_assert(sizeof(buffer.message) > uavcan_diagnostic_Record_1_1_text_ARRAY_CAPACITY_, "Something's wrong");
    static_assert(sizeof(buffer) > uavcan_diagnostic_Record_1_1_SERIALIZATION_BUFFER_SIZE_BYTES_, "Something's wrong");

    const uint64_t    monotonic_timestamp    = get_monotonic_timestamp_us();
    const uint64_t    synchronized_timestamp = get_synchronized_timestamp_us();
    const bool        rtos                   = xTaskGetSchedulerState() == taskSCHEDULER_RUNNING;
    const char* const task_name              = rtos ? pcTaskGetName(NULL) : "Core";

    const bool log_to_console = (enabled_handlers & _LOG_ENABLE_UART) && log_level >= log_level_console;
    const bool log_to_bus     = (enabled_handlers & _LOG_ENABLE_UAVCAN) && rtos && (log_level >= log_level_uavcan) &&
                            (node_get_transport_task() != NULL) && node_get_id() < CANARD_NODE_ID_MAX;

    va_list args;
    size_t  pos;

    if (rtos) vTaskSuspendAll();

    if (log_to_console) {
        const uint32_t ts_9_digits = monotonic_timestamp - 1000000000ul * (monotonic_timestamp / 1000000000ul);
        pos = sprintf(&buffer.message[0], "%9lu %5u [%-15s] %8s: ", ts_9_digits, xPortGetFreeHeapSize(), task_name,
                      getLogLevelName(log_level));
        ASSERT(pos < 256);
        stdout_puts(buffer.message, pos);
    }

    pos                    = sprintf(&buffer.message[0], "%s: ", task_name);
    const size_t msg_start = pos;
    va_start(args, format_string);
    pos += vsprintf(&buffer.message[pos], format_string, args);
    va_end(args);
    ASSERT(pos < 256);

    if (log_to_console) {
        stdout_puts(&buffer.message[msg_start], pos - msg_start);
        stdout_puts("\r\n", 2);
    }

    if (log_to_bus) {
        buffer.len             = (uint8_t) pos;
        buffer.timestamp       = synchronized_timestamp;
        buffer.header_bytes[7] = (uint8_t) log_level;

        static uint_fast8_t          transfer_id = 0;
        const CanardTransferMetadata meta        = {.priority       = log_level < LOG_LEVEL_WARNING ? CanardPriorityOptional
                                                                                                    : CanardPrioritySlow,
                                                    .transfer_kind  = CanardTransferKindMessage,
                                                    .port_id        = uavcan_diagnostic_Record_1_1_FIXED_PORT_ID_,
                                                    .remote_node_id = CANARD_NODE_ID_UNSET,
                                                    .transfer_id    = transfer_id++};
        node_try_publish(1000000UL, &meta, pos + 9, (uint8_t*) &buffer, 0);
    }

    if (rtos) xTaskResumeAll();
}
