/**
 * @file memory_map.h
 * @author Lasse Fröhner <lasse@starcopter.com>
 * @brief Shared Memory Map, Referencing Linker Variables
 * @date 2021-06-10
 *
 * @copyright Copyright (c) 2021 starcopter GmbH
 *
 * This software is distributed under the terms of the MIT License.
 *
 */

#pragma once

#include <inttypes.h>

extern uint32_t __shared_ram_start__;
extern uint32_t __shared_ram_end__;

extern uint32_t __app_ram_start__;
extern uint32_t __app_ram_end__;

extern uint32_t __bootloader_start__;
extern uint32_t __bootloader_end__;

extern uint32_t __dcb_start__;
extern uint32_t __dcb_end__;

extern uint32_t __app_loader_start__;
extern uint32_t __app_loader_end__;

extern uint32_t __app_start__;
extern uint32_t __app_end__;

extern uint32_t __nvm_start__;
extern uint32_t __nvm_end__;

/* First word in RAM. Declared in Linker Script. */
extern uint32_t __ram_start__;
/* First word AFTER RAM; DEFINITELY NOT ACCESSIBLE. Declared in Linker Script. */
extern uint32_t __ram_end__;

/* First word in flash memory. Declared in Linker Script. */
extern uint32_t __flash_start__;
/* First word AFTER the flash memory; DEFINITELY NOT ACCESSIBLE. Declared in Linker Script. */
extern uint32_t __flash_end__;
